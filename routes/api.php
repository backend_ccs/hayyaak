<?php

use Illuminate\Http\Request;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');

header('Content-Type: application/json; charset=UTF-8', true);

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|order_received
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy buiةشlding your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//users


Route::get('show_allroles','contactController@show_allroles');

Route::get('delete_admin','contactController@delete_admin');

Route::get('show_alladmin','contactController@show_alladmin');
Route::get('show_adminbyid','contactController@show_adminbyid');
Route::post('add_admindata','contactController@add_admindata');
Route::post('update_admin','contactController@update_admin');


Route::post('logout','UserController@logout');

Route::get('accept_cancelRequest','OrdersController@accept_cancelRequest');




Route::get('show_contactDetails','contactController@show_contactDetails');
Route::get('show_onecontact','contactController@show_onecontact');
Route::post('add_contact','contactController@add_contact');
Route::post('update_contact','contactController@update_contact');

Route::post('edit_message','dashcontroller@edit_message');

Route::get('show_notificationstate','dashcontroller@show_notificationstate');


Route::post('save_notification','dashcontroller@save_notification');

Route::get('show_defaultmessages','dashcontroller@show_defaultmessages');

Route::get('show_allmessages','dashcontroller@show_allmessages');

Route::get('show_messagebycode','dashcontroller@show_messagebycode');

Route::get('delete_message','dashcontroller@delete_message');


Route::get('resend_notification','dashcontroller@resend_notification');


Route::get('data_count','dashcontroller@data_count');

Route::get('delete_driver','dashcontroller@delete_driver');

Route::get('close_driveraccount','AdminController@close_driveraccount');


Route::post('add_user','AdminController@add_user');
Route::post('update_user','AdminController@update_user');
Route::get('show_userbyid','AdminController@show_userbyid');


Route::post('add_driver','AdminController@add_driver');
Route::post('update_driver','AdminController@update_driver');


Route::get('show_allDrivercompliance','AdminController@show_allDrivercompliance');
Route::get('show_oldDrivercompliance','AdminController@show_oldDrivercompliance');

Route::get('regist_again','dashcontroller@regist_again');

Route::get('show_users','dashcontroller@show_users');


Route::post('search_users','dashcontroller@search_users');

Route::post('send_copon_activeusers','dashcontroller@send_copon_activeusers');


Route::post('send_copon_specialusers','dashcontroller@send_copon_specialusers');
Route::post('send_copon_allusers','dashcontroller@send_copon_allusers');
Route::post('send_copon_allagents','dashcontroller@send_copon_allagents');





Route::post('send_notification_allusers','dashcontroller@send_notification_allusers');
Route::post('send_notification_allagent','dashcontroller@send_notification_allagent');
Route::post('send_notification_specialusers','dashcontroller@send_notification_specialusers');


Route::get('show_lesscredit','dashcontroller@show_lesscredit');
Route::get('show_morecredit','dashcontroller@show_morecredit');


Route::post('send_credit_notify','dashcontroller@send_credit_notify');

Route::post('edit_credit','dashcontroller@edit_credit');


Route::get('show_chat','dashcontroller@show_chat');



Route::get('show_chat','dashcontroller@show_chat');

Route::get('show_activedrivers','dashcontroller@show_activedrivers');
Route::get('show_allusers','dashcontroller@show_allusers');



Route::post('search_usersRate','dashcontroller@search_usersRate');

Route::get('show_usersRate','dashcontroller@show_usersRate');
Route::get('show_driversRate','dashcontroller@show_driversRate');


Route::get('show_blockedusers','dashcontroller@show_blockedusers');
Route::get('show_activeusers','dashcontroller@show_activeusers');



Route::get('time','dashcontroller@time');

Route::get('not_now','UserController@not_now');

Route::get('check_email','UserController@check_email');

Route::post('verification_code','TrainController@verification_code');


Route::get('current_location','UserController@current_location');

Route::post('add_account','UserController@add_account');

Route::post('edit_account','UserController@edit_account');
Route::get('my_accounts','UserController@my_accounts');
Route::get('make_defualt','UserController@make_defualt');


Route::get('show_banks','UserController@show_banks');

Route::get('show_gender','UserController@show_gender');
Route::get('show_user','UserController@show_user');
Route::post('regesteration','UserController@regesteration');
Route::post('driver_regesteration','UserController@driver_regesteration');
Route::post('update_location','UserController@update_location');
Route::post('edit_userProfile','UserController@edit_userProfile');
Route::post('update_location','UserController@update_location');

Route::post('phone_verify','UserController@phone_verify');


Route::post('code_verify','UserController@code_verify');

//all country

Route::get('show_allcountry','CountryController@show_allcountry');

//offers

Route::get('show_offers','NewController@show_offers');
Route::get('near_orders','NewController@near_orders');


Route::get('cancel_reasons','NewController@cancel_reasons');

Route::get('show_usertax','NewController@show_usertax');

Route::get('show_tax','NewController@show_tax');

Route::post('update_tax','NewController@update_tax');

Route::get('delivery_total','NewController@delivery_total');

Route::get('show_types','NewController@show_types');
Route::get('show_typebyid','NewController@show_typebyid');
Route::get('delete_type','NewController@delete_type');


Route::post('add_product_type','NewController@add_product_type');
Route::post('update_product_type','NewController@update_product_type');




Route::get('show_menu','NewController@show_menu');
Route::get('show_productbyid','NewController@show_productbyid');
Route::get('delete_product','NewController@delete_product');

Route::post('update_product','NewController@update_product');
Route::post('add_product','NewController@add_product');


//notifications

Route::post('remove_email','MessageController@remove_email');

Route::get('click_order','MessageController@click_order');

Route::get('delete_Notification','MessageController@delete_Notification');

Route::get('show_Notifications','MessageController@show_Notifications');

Route::get('turn_ONNotifications','MessageController@turn_ONNotifications');

Route::get('Notification_count','MessageController@Notification_count');



Route::get('show_product_type','driverController@show_product_type');

//fhow driver by user

Route::get('show_driverprofile','driverController@show_driverprofile');

Route::get('finished_requestes','MessageController@finished_requestes');

Route::get('show_bill','MessageController@show_bill');

Route::get('late_requestes','MessageController@late_requestes');

Route::get('wait_requestes','MessageController@wait_requestes');
Route::get('canceled_requestes','MessageController@canceled_requestes');
Route::get('new_requestes','MessageController@new_requestes');

Route::get('hang_requestes','MessageController@hang_requestes');

//messages

Route::get('showmessage','MessageController@showmessage');
Route::post('sentmessage','MessageController@sentmessage');
Route::get('show_UsersHasMessage','MessageController@show_UsersHasMessage');

//complaince


Route::get('show_complain_questions','NotificationController@show_complain_questions');

Route::post('make_compliance','NotificationController@make_compliance');
Route::get('show_languages','NotificationController@show_languages');

Route::get('choose_language','NotificationController@choose_language');

Route::get('show_compliance','NotificationController@show_compliance');


Route::get('show_compliancebyid','NotificationController@show_compliancebyid');

//
Route::get('add_me_shopAgent','ShopController@add_me_shopAgent');


//payments

Route::get('show_payment','PaymentController@show_payment');

//user_copons
Route::get('show_myCopons','PaymentController@show_myCopons');
Route::post('add_mycopon','PaymentController@add_mycopon');
Route::get('delete_mycopon','PaymentController@delete_mycopon');


//data_count

Route::get('user_Datacounts','OrdersController@user_Datacounts');
Route::get('user_cancel_order','OrdersController@user_cancel_order');
Route::get('client_say_payment','OrdersController@client_say_payment');

Route::post('make_order','OrdersController@make_order');
Route::post('make_order1','OrdersController@make_order1');
Route::post('make_order2','OrdersController@make_order2');
Route::post('make_order3','OrdersController@make_order3');
Route::post('make_order4','OrdersController@make_order4');
Route::get('user_accept_offer','OrdersController@user_accept_offer');


Route::get('show_userOf_order','OrdersController@show_userOf_order');



//shop_orders

Route::get('show_allnearshopes','driverController@show_allnearshopes');

Route::get('show_shopbyid','driverController@show_shopbyid');
Route::get('show_nearshopes','driverController@show_nearshopes');

Route::get('shop_orders','OrdersController@shop_orders');


//orders

Route::get('order_byid','OrdersController@order_byid');

//offers

Route::get('driver_active_Orders','OfferController@driver_active_Orders');

Route::get('driver_finished_Orders','OfferController@driver_finished_Orders');


Route::get('change_agent','OfferController@change_agent');


Route::post('make_offer','OrdersController@make_offer');
Route::get('Withdraw_Fromorder','OfferController@Withdraw_Fromorder');
    
Route::get('arrived_site','OfferController@arrived_site');

Route::get('driver_Datacounts','OfferController@driver_Datacounts');

Route::get('order_deliverd','OfferController@order_deliverd');
Route::get('order_received','OfferController@order_received');
Route::get('user_received_order','OfferController@user_received_order');



//rating
Route::get('show_rating','AboutController@show_rating');
Route::get('show_ratingby_userid','AboutController@show_ratingby_userid');

Route::post('driver_rateUser','AboutController@driver_rateUser');

Route::get('show_oneconditions','AboutController@show_oneconditions');



//


Route::get('show_complain_reasons','AboutController@show_complain_reasons');

Route::get('show_policy','AboutController@show_policy');
Route::get('show_use_conditions','AboutController@show_use_conditions');


Route::get('about_us','AboutController@about_us');


Route::get('show_categorybyid','ShopController@show_categorybyid');
Route::get('delete_category','ShopController@delete_category');
Route::post('add_category','ShopController@add_category');
Route::post('update_category','ShopController@update_category');


Route::get('user_active_requestes','ShopController@user_active_requestes');
Route::get('show_oneshope','ShopController@show_oneshope');
Route::get('show_category','ShopController@show_category');


Route::post('add_branch','ShopController@add_branch');
Route::post('update_branch','ShopController@update_branch');
Route::get('show_branches','ShopController@show_branches');
Route::get('show_branchbyid','ShopController@show_branchbyid');
Route::get('delete_branche','ShopController@delete_branche');




Route::get('user_finished_requestes','ShopController@user_finished_requestes');

//////////////////////////////////////////////

//dashboard

//admin

Route::post('admin_login','AdminController@admin_login');
Route::post('edit_adminprofile','AdminController@edit_adminprofile');
Route::get('show_adminById','AdminController@show_adminById');

//shopes

Route::post('make_bill','ShopController@make_bill');

Route::get('show_shopes','ShopController@show_shopes');
Route::get('show_shopebyid','ShopController@show_shopebyid');
Route::get('delete_shop','ShopController@delete_shop');
Route::post('add_shope','ShopController@add_shope');
Route::post('update_shope','ShopController@update_shope');

Route::get('show_bill_byRequestID','OrdersController@show_bill_byRequestID');





//drivers
Route::get('unblock_agent','AdminController@unblock_agent');


Route::get('block_agent','AdminController@block_agent');

Route::get('show_driverbyid','AdminController@show_driverbyid');
Route::get('show_alldrivers','AdminController@show_alldrivers');
Route::get('show_pendingdrivers','AdminController@show_pendingdrivers');
Route::get('show_blockeddrivers','AdminController@show_blockeddrivers');
Route::get('accept_driver','AdminController@accept_driver');
Route::get('block_driver','AdminController@block_driver');


//copons
Route::post('add_copon','CoponController@add_copon');
Route::post('updated_copon','CoponController@updated_copon');
Route::get('delete_copon','CoponController@delete_copon');
Route::get('show_allCopons','CoponController@show_allCopons');

Route::get('show_notactiveCopons','CoponController@show_notactiveCopons');
Route::get('show_copon_ByID','CoponController@show_copon_ByID');



//country

Route::get('show_country','CountryController@show_country');
Route::get('show_countryByid','CountryController@show_countryByid');
Route::get('delete_country','CountryController@delete_country');
Route::post('insert_country','CountryController@insert_country');
Route::post('update_country','CountryController@update_country');



//payments

Route::get('show_payments','VehicelController@show_payments');
Route::get('show_paymentByid','VehicelController@show_paymentByid');

Route::get('show_paymentByid_dash','VehicelController@show_paymentByid_dash');

Route::get('delete_payment','VehicelController@delete_payment');
Route::post('insert_payment','VehicelController@insert_payment');
Route::post('update_payment','VehicelController@update_payment');



//policy
Route::get('show_policy_dash','AboutController@show_policy_dash');

Route::post('update_policy','AboutController@update_policy');


//use conditions
Route::post('update_use_conditions','AboutController@update_use_conditions');

//about
Route::get('about_us_dash','AboutController@about_us_dash');

Route::post('update_about_us','AboutController@update_about_us');


//compliance

Route::post('refuse_complaince','AdminController@refuse_complaince');
Route::get('show_onecompliance','AdminController@show_onecompliance');

Route::post('accept_complaince','AdminController@accept_complaince');

Route::get('show_allcompliance','AdminController@show_allcompliance');

Route::get('show_oldcompliance','AdminController@show_oldcompliance');



//payment online ::


Route::get('payment_request','PaymentController@payment_request');

Route::get('payment_status','PaymentController@payment_status');
