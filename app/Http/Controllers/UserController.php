<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Gender;
use App\City;
use App\Vehicels;
use App\Vehicel_type;
use App\Work_way;
use App\how;
use App\setting;
use App\user_accounts;
use App\banks;
use Illuminate\Support\Str;
use App\deriver_details;
use App\Government;
use carbon\carbon;
use App\Wallet;

class UserController extends Controller
{
    public $message=array();
    
    
     public function not_now(Request $request)
     {
       try{
            $state=$request->input('state');
                 
	                $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

		           
                     try{
                        $str='trainig';
		                  
		                  $user_token=hash('sha256', Str::random(60)).time().rand(100000,999999).sha1($str);// give a unique name to file to be saved
		                  $fname='trainig';
		                  $last='training';
		                  
		                    
		            
		           }catch(Exception $ex){
		                $message['error']=4;
                   $message['message']='error in generate key';    
		          }
		          
		          
		          
		          $insert=new User;
		          $insert->first_name=$fname;
		          $insert->last_name=$last;
		          $insert->state=$state;
		          $insert->user_token=$user_token;
		          $insert->type='training';
		          $insert->created_at=$dateTime;
		          $insert->save();

               $select=User::select('users.id','users.first_name','users.last_name','roles.description as state','users.user_token')
                     ->leftJoin('roles','users.state','=','roles.id')
                     ->where('users.id',$insert->id)
                      ->first();
           
           
           
                 if($insert ==true){
               
            
                      $message['data']=$select;
                      $message['error']=0;
                      $message['message']='insert data success';
                  }else{
                      
                       $message['data']=$select;
                      $message['error']=1;
                      $message['message']='no data'; 
                      
                  }
           
            
           
    	
          
            }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
            return response()->json($message);
     }
     
     
     
     
     
    public function check_email(Request $request)
     {
       try{
                  $email=$request->input('email');  
                  $firebase_token=$request->input('firebase_token');  
                
                 
	          
		          
		  if($request->has('email')){        
		  
               $select=User::select('users.id','users.first_name','users.last_name','users.image','users.email','users.password as passwords','roles.role as state','users.phone','users.user_token','users.gender','gender.name as gender')
                     ->leftJoin('roles','users.state','=','roles.id')
                      ->leftJoin('gender','users.gender','=','gender.id')
                     ->where('users.email',$email)
                      ->first();
           
           
           
               $updated_at = carbon::now()->toDateTimeString();
               $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
                 
              
                 
                     try{
                    
		                  
		                    $user_token=hash('sha256', Str::random(60)).time().rand(100000,999999);// give a unique name to file to be saved
		                    
		            
		           }catch(Exception $ex){
		                $message['error']=4;
                   $message['message']='error in generate key';    
		          }
		          
           
           
                 if($select !=null){
                     
                     
                     $update=User::where('email',$email)->update([
                            'firebase_token'=>$firebase_token,
                            'user_token'=>$user_token,
                            'updated_at'=>$dateTime
                         ]);
                         
                         
                 $select=User::select('users.id','users.first_name','users.last_name','users.image','users.email','users.password as passwords','roles.role as state','users.phone','users.user_token','users.gender','gender.name as gender_name','users.firebase_token')
                     ->leftJoin('roles','users.state','=','roles.id')
                      ->leftJoin('gender','users.gender','=','gender.id')
                     ->where('users.email',$email)
                      ->first();
               
            
                      $message['data']=$select;
                      $message['error']=0;
                      $message['message']='show data success';
                  }else{
                      
                       $message['data']=$select;
                      $message['error']=1;
                      $message['message']='no data'; 
                      
                  }
           
            
           
    		  }else{
    		        $message['error']=5;
                     $message['message']='please enter email'; 
                          
    		      
    		  }
          
            }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
            return response()->json($message);
     }
    
    
    
    
    
    public function regesteration(Request $request)
    {
       try{
           
              $id=$request->input('user_id');
              
              
              $fname=$request->input('first_name');
              $lname=$request->input('last_name');
         
	          $email=$request->input('email');
	          $phone=$request->input('phone');
	          $image=$request->file('image');
	          $pass=$request->input('passwords');
              $gender=$request->input('gender_id');
              $longitude=$request->input('longitude');
              $latitude=$request->input('latitude');
              $firebase_token=$request->input('firebase_token');
          
        
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));


           if(isset($image)) {
                        $new_name = $image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/users';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
               }else{
                  $images = NULL;       
              }
     
              
                //verification
     if($request->has('user_id')){
             
              if($request->has('email')){
                  $emailexist=User::select('id','email')->where([['email',$email],['id','!=',$id]])->get();
              }else{
                  
                 $emailexist=array(); 
              }
              
              
               if($request->has('phone') && $phone !='' && !empty($phone)){
                   
                  $phoneexist=User::select('id','phone')->where([['wats_phone',$phone],['id','!=',$id] , ['registy_state' , '!=' , 0]])->get();
               
                  $deletephone = User::select('id','phone')->where([['wats_phone',$phone], ['registy_state'  , 0]])->delete();

               }else{
                  
                 $phoneexist=array(); 
              }
           if(count($phoneexist)> 0){
                  $message['data']=$phoneexist;
                  $message['error']=4;
                  $message['message']='phone  exist';
           }elseif(count($emailexist) >0){
                  $message['error']=5;
                  $message['message']='email  exist';
           }else{
                 
               
            

          $update=User::where('id',$id)->update([
              'first_name'=>$fname,
              'last_name'=>$lname,
              'email'=>$email,
              'wats_phone'=>$phone,
              'password'=>$pass,
              'image'=>$images,
              'gender'=>$gender,
              'longitude'=>$longitude,
              'state'=>3,
              'registy_state'=>1,
              'is_driver'=>0,
              'latitude'=>$latitude,
              'firebase_token'=>$firebase_token,
              'updated_at'=>$dateTime]);
          
                  
            $select=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwods','users.phone','users.wats_phone as whats_phone','roles.role as state','users.image','users.longitude','users.latitude','users.user_token','users.firebase_token')
            ->join('roles','users.state','=','roles.id')
           
               ->where('users.id',$id)->first();

                
      
                if($update ==true){

                    $set=new setting;
                    $set->user_id=$id;
                    $set->notification="ON";
                    $set->language=1;
                    $set->created_at=$dateTime;
                    $set->save();

                    $wal=new Wallet;
                    $wal->user_id=$id;
                    $wal->credit=0;
                    $wal->created_at=$dateTime;
                    $wal->save();
                  
                    $addRate = new \App\Rating;
                    $addRate->user_id = $id;
                    $addRate->driver_id = 1;
                    $addRate->rate = 5;
                    $addRate->comment = "رجل ممتاز";
                    $addRate->who_rate = "driver";
                    $addRate->created_at = $dateTime;
                    $addRate->save();
                    
                    $message['data']=$select;
                    $message['error']=0;
                    $message['message']='regesteration success';
                }else{
                      $message['data']=$select;
                      $message['error']=1;
                       $message['message']='error in save';
                }
              

            }
    }else{
        
          
                     try{
                         $str='user';
		                  
		                    $user_token=hash('sha256', Str::random(90)).time().rand(100000,999999).sha1($str);// give a unique name to file to be saved
		                    
		            
		           }catch(Exception $ex){
		                $message['error']=4;
                        $message['message']='error in generate key';    
		           }
		          
		          
          if($request->has('email')){
              $emailexist=User::select('id','email')->where('email',$email)->first();
          }else{
              
              $emailexist=array();
          }
          
           if($request->has('phone')){
             $phoneexist=User::select('id','phone')->where([['phone',$phone] , ['registy_state' , '!=' , 0]])->first();

            $deletephone = User::select('id','phone')->where([['wats_phone',$phone], ['registy_state'  , 0]])->delete();

           
           }else{
               
               $phoneexist=array();
           }           
           if( $phoneexist != NULL){
                  $message['data']=$phoneexist;
                  $message['error']=4;
                  $message['message']='phone  exist';
           }elseif( $emailexist != NULL){
                  $message['error']=5;
                  $message['message']='email  exist';
           }elseif(empty($phone) && empty($email)){
                   $message['error']=5;
                   $message['message']='please enter phone or email ';
           }else{
                 
               
            

          $insert=User::insertGetId([
              'first_name'=>$fname,
              'last_name'=>$lname,
              'email'=>$email,
              'phone'=>$phone,
              'password'=>$pass,
              'image'=>$images,
              'gender'=>$gender,
              'longitude'=>$longitude,
              'user_token'=>$user_token,
              'state'=>3,
              'registy_state'=>1,
              'is_driver'=>0,
              'latitude'=>$latitude,
              'firebase_token'=>$firebase_token,
              'created_at'=>$dateTime]);
          
                  
            $select=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwods','users.phone','users.wats_phone as whats_phone','roles.role as state','users.image','users.longitude','users.latitude','users.user_token','users.firebase_token')
            ->join('roles','users.state','=','roles.id')
           
               ->where('users.id',$insert)->first();

                
      
                if($insert >0){

                	$set=new setting;
                	$set->user_id=$insert;
                	$set->notification="ON";
                    $set->language=1;
                	$set->created_at=$dateTime;
                	$set->save();

                    $wal=new Wallet;
                    $wal->user_id=$insert;
                    $wal->credit=0;
                    $wal->created_at=$dateTime;
                    $wal->save();
                  
                    $addRate = new \App\Rating;
                    $addRate->user_id = $insert;
                    $addRate->driver_id = 1;
                    $addRate->rate = 5;
                    $addRate->comment = "رجل ممتاز";
                    $addRate->who_rate = "driver";
                    $addRate->created_at = $dateTime;
                    $addRate->save();
                    
                    $message['data']=$select;
                    $message['error']=0;
                    $message['message']='regesteration success';
                }else{
                      $message['data']=$select;
                      $message['error']=1;
                       $message['message']='error in save';
                }
              

            }
        
        
        
        
        
        
        
        
        
    }
            }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
            return response()->json($message);
    }


      public function phone_verify(Request $request)
     {
       try{
                 $phone=$request->input('phone');  
                 $country=$request->input('country');
                 $firebase_token=$request->input('firebase_token');
                 
                 
                    
                $updated_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
              
                 
                     try{
                    
		                    $user_token=hash('sha256', Str::random(90)).time().rand(1000000,9999999).sha1($phone);// give a unique name to file to be saved
		                   //  $code=rand(1000,9999);
                           $code='8888';
		            
		           }catch(Exception $ex){
		                $message['error']=4;
                   $message['message']='error in generate key';    
		          }
		          
		          
		          
		  if($request->has('phone')){        
		      
           $exist=User::select('users.id','users.phone','users.user_token')
                      ->where([['users.phone',$phone],['registy_state',1]])
                      ->first();
           
           
           
           if($exist !=null){
               
               if($request->has('firebase_token')){
                   $firebase_token=$request->firebase_token;
               }else{
                   $firebase_token=User::where('phone',$phone)->limit(1)->value('firebase_token');
               }
               
                $update=User::where('phone',$phone)->update([
                                                            'user_token'=>$user_token,
                                                           // 'verify_code'=>$code,
                                                            'firebase_token'=>$firebase_token
                                                            ]);
                    
                      

                $phoneexist=User::select('users.id','users.first_name','users.image','users.email','users.password as passwords','roles.role as state','users.phone','users.user_token','users.gender','gender.name as gender_name')
                      ->leftJoin('roles','users.state','=','roles.id')
                      ->leftJoin('gender','users.gender','=','gender.id')
                      ->where('users.phone',$phone)
                      ->first();
                      
                      
                  
           
                  $message['data']=$phoneexist;
                  $message['error']=4;
                  $message['message']='phone exist and verification code sent';
           }else{
               
            $exist=User::select('users.id','users.phone','users.user_token')
             ->where('users.phone',$phone)
              ->first();
           
           
           
           if($exist !=null){
               
                $update=User::where('phone',$phone)->update([
                                                        'verify_code'=>$code,
                                                        'firebase_token'=>$firebase_token
                                                        ]);
               
                  $select=User::select('users.id','users.phone','roles.role as state','users.created_at')
                   ->leftJoin('roles','users.state','=','roles.id')
                  ->where('users.id',$exist['id'])->first();
                  
                  
                  
                   $phone= str_replace('+', '', $phone);
                      $headers = array
                                      (
                                        'Authorization: ("username":966534066660,"password":"f123456kh")',
                                        'Content-Type: application/json',
                                        'Accept: application/json',
                                         );
                            $ch = curl_init();
                          curl_setopt($ch, CURLOPT_URL,"https://www.hisms.ws/api.php?send_sms&username=966534066660&password=f123456kh&lang=3&numbers=".$phone."&sender=Hayyak&message=verification__code__is__".$code."&applicationType=68");
                    
                               // curl_setopt($ch, CURLOPT_POST,true);
                                curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
                                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, true );
                                 curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    
                              //  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));
                    
                                $response = curl_exec($ch);
                                curl_close($ch);
                                
                                  $select['isverify']='true';
                      $message['data']=$select;
                      $message['error']=0;
                      $message['message']='regesteration success';
           }else{
           

                  
         $user=User::insertGetId(['phone'=>$phone,'country'=>$country,'user_token'=>$user_token,'registy_state'=>0,'created_at'=>$dateTime,'verify_code'=>$code]);

                  $select=User::select('users.id','users.phone','roles.role as state','users.created_at')
                   ->leftJoin('roles','users.state','=','roles.id')
                   ->where('users.id',$user)->first();
                  
                  
                  if($user > 0){
                       $phone= str_replace('+', '', $phone);
                      $headers = array
                                      (
                                         'Authorization: ("username":966534066660,"password":"f123456kh")',
                                       'Content-Type: application/json',
                                       'Accept: application/json',
                                         );
                            $ch = curl_init();
                          curl_setopt($ch, CURLOPT_URL,"https://www.hisms.ws/api.php?send_sms&username=966534066660&password=f123456kh&lang=3&numbers=".$phone."&sender=Hayyak&message=verification__code__is__".$code."&applicationType=68");
                    
                               // curl_setopt($ch, CURLOPT_POST,true);
                                curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
                                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, true );
                                 curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    
                              //  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));
                    
                                $response = curl_exec($ch);
                                curl_close($ch);
                                
                                  $select['isverify']='true';
                      
                      $message['data']=$select;
                      $message['error']=0;
                      $message['message']='regesteration success';
                  }else{
                      
                       $message['data']=$select;
                      $message['error']=1;
                      $message['message']='regesteration failed'; 
                      
                  }
           }
            
           }
		  }else{
		        $message['error']=5;
                 $message['message']='please enter data'; 
                      
		      
		  }
          
            }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
            return response()->json($message);
     }

    public function code_verify(Request $request)
     {
       try{
                 $phone=$request->input('phone');  
                 $code=$request->input('code');
                 $action=$request->input('action'); //r =>regesteration    u =>update
                 
                 
                    
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
                 
               
                   
                        	$user_data=User::select('users.id','users.first_name','users.image','users.email','users.password as passwords','roles.role as state','users.phone','users.user_token','users.gender','gender.name as gender_name')
                              ->leftJoin('roles','users.state','=','roles.id')
                              ->leftJoin('gender','users.gender','=','gender.id')
     	                 	   ->where([['users.phone',$phone],['verify_code',$code]])->first();
     	                 	
     	                 	
                   
                      if($user_data !=null){
                    $update=User::where('phone',$phone)->update(['verify_code'=>NULL]);

                      $message['data']=$user_data;
                      $message['error']=0;
                      $message['message']='verification success';
                  }else{
                      
                       $message['data']=$user_data;
                      $message['error']=1;
                      $message['message']='verification failed'; 
                      
                  }
                   
                   
                   
               
       
               
   
            }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
            return response()->json($message);
     }

     //show gender

      public function show_gender(Request $request)
     {
       try{
            $lang=$request->input('lang');


       	  if($lang =='EN' ||$lang =='en'){

            $gender=Gender::select('id','E_name as name')->get();
           }else{
            $gender=Gender::select('id','name')->get();

           } 

              if(count($gender)>0 ){
         
              $message['data']=$gender;
              $message['error']=0;
              $message['message']='show_success';

            }else{

              $message['data']=$gender;
              $message['error']=1;
              $message['message']='error in show data';
            }


             
		    }catch(Exception $ex){
		               $message['error']=2;
		               $message['message']='error'.$ex->getMessage();
		            }
		      
		             return response()->json($message);
     }

     
     
       public function logout(Request $request)
     {
       try{
           $token=$request->input('user_token');

             $check_token=User::where('user_token',$token)->first();
          
          
        if($request->has('user_token') && $check_token !=NULL){
       
          
        
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
          
          
          $update=User::where('id',$check_token['id'])->update([
              'firebase_token'=>NULL,
              'user_token'=>NULL,
              'updated_at'=>$dateTime
              
              
              
              
              ]);
       	 

               if($update ==true){
                  
                      $message['error']=0;
                       $message['message']='logout  success';
                }else{
                      $message['data']=$select;
                      $message['error']=1;
                       $message['message']='error in logout';
                }
              

            
        }else{
          $message['error']=3;
            $message['message']='this token is not exist';
        }
      }catch(Exception $ex){
          $message['error']=2;
          $message['message']='error'.$ex->getMessage();
      }
            return response()->json($message);
     }
       public function show_banks(Request $request)
     {
       try{
            $lang=$request->input('lang');


          if($lang =='EN' ||$lang =='en'){

            $select=banks::select('id','E_name as name')->get();
           }else{
            $select=banks::select('id','name')->get();

           } 

              if(count($select)>0 ){
         
              $message['data']=$select;
              $message['error']=0;
              $message['message']='show_success';

            }else{

              $message['data']=$select;
              $message['error']=1;
              $message['message']='error in show data';
            }


             
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
                }
          
                 return response()->json($message);
     }
     //driver regesteration
      public function driver_regesteration(Request $request)
     {
       try{
              $id=$request->input('user_id');
              
              
              $fname=$request->input('first_name');
              $lname=$request->input('last_name');
              $email=NULL;
              $email=$request->input('email');
              $phone=$request->input('phone');
              $image=$request->file('image');
              $pass=$request->input('passwords');
              $gender=$request->input('gender_id');

              $nationality=$request->input('nationality');
              $card_number=$request->input('card_number');
  
              $card_image=$request->file('card_image');
              $driving_license_image=$request->file('driving_license_image');
              $car_license_image=$request->file('car_license_image');
              $front_vehical_image=$request->file('front_vehical_image');
              $longitude=$request->input('longitude');
              $latitude=$request->input('latitude');
              $bank_account=$request->input('bank_account');//1,0
              $bank=$request->input('bank');
              $apple_account=$request->input('apple_bay');//1,0
              $number=$request->input('account_number');


              	
             $firebase_token=$request->input('firebase_token');
          
        
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));


           if(isset($image)) {
                        $new_name = $image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/users';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
               }else{
                  $images = NULL;       
              }

               if(isset($card_image)) {
                        $new_name = $card_image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/cards';
                        $card_image->move($destinationPath_id, $savedFileName);
            
                        $card_images = $savedFileName;
                      
               }else{
                  $card_images = NULL;       
              }
     
          if(isset($driving_license_image)) {
                        $new_name = $driving_license_image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/license';
                        $driving_license_image->move($destinationPath_id, $savedFileName);
            
                        $driving_license_images = $savedFileName;
                      
               }else{
                  $driving_license_images = NULL;       
              }
     
          if(isset($car_license_image)) {
                $new_name = $car_license_image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                $destinationPath_id = 'uploads/license';
                $car_license_image->move($destinationPath_id, $savedFileName);
    
                $car_license_images = $savedFileName;
                  
           }else{
              $car_license_images = NULL;       
          }
         
            if(isset($front_vehical_image)) {
                $new_name = $front_vehical_image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                $destinationPath_id = 'uploads/vehicels';
                $front_vehical_image->move($destinationPath_id, $savedFileName);
    
                $front_vehical_images = $savedFileName;
                  
            }else{
              $front_vehical_images = NULL;       
            }
     
        
     

     if($request->has('user_id')){

                //verification
         $check_state=User::where('id',$id)->first();
           
        /*   if($check_state['state'] !=NULL && $check_state['state'] !=''&& !empty($check_state['state'])){
               
               
           }*/
              if($request->has('phone')){
                $phoneexist=User::select('id','phone','first_name','image','password as passwords')->where([['phone',$phone],['id','!=',$id]])->orwhere([['wats_phone',$phone],['id','!=',$id]])->first();
              }else{
                $phoneexist=(object)array();
              }
           if($phoneexist != NULL){
                  $message['data']=$phoneexist;
                  $message['error']=4;
                   $message['message']='phone  exists';
           }else{
                 
                 //is_driver=1  /0
                 
               if($check_state['state'] !=NULL && $check_state['state'] !=''&& !empty($check_state['state'])){
                   
                $user=User::where('id',$id)->update(['first_name'=>$fname,'last_name'=>$lname,'email'=>$email,'wats_phone'=>$phone,'password'=>$pass,'state'=>5,'image'=>$images,'gender'=>$gender,'longitude'=>$longitude,'latitude'=>$latitude,
              	'firebase_token'=>$firebase_token,'created_at'=>$dateTime,'registy_state'=>1,'is_driver'=>0]);
              	
              	
              	
              	
              	
               }else{
                   
                     
                $user=User::where('id',$id)->update(['first_name'=>$fname,'last_name'=>$lname,'email'=>$email,'wats_phone'=>$phone,'password'=>$pass,'state'=>5,'image'=>$images,'gender'=>$gender,'longitude'=>$longitude,'latitude'=>$latitude,
              	'firebase_token'=>$firebase_token,'created_at'=>$dateTime,'registy_state'=>1,'is_driver'=>1]); 
              	
              	   if($user ==true){
                  		$set=new setting;
                    	$set->user_id=$id;
                    	$set->notification="ON";
                        $set->language=1;
                    	$set->created_at=$dateTime;
                    	$set->save();

                        $wal=new Wallet;
                        $wal->user_id=$id;
                        $wal->credit=50;
                        $wal->created_at=$dateTime;
                        $wal->save();
                        
                        $addRate = new \App\Rating;
                        $addRate->user_id = 1;
                        $addRate->driver_id = $id;
                        $addRate->rate = 5;
                        $addRate->comment = "رجل ممتاز";
                        $addRate->who_rate = "user";
                        $addRate->created_at = $dateTime;
                        $addRate->save();  
                          
              	   }
              	
              	
               }
               
            

         
          
                  
            $select=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwods','users.phone','roles.role as state','users.image','users.gender','users.longitude','users.latitude','users.firebase_token','users.user_token')
              ->join('roles','users.state','=','roles.id')
           //  ->join('driver_vehicles_details','users.id','=','driver_vehicles_details.driver_id')
               ->where('users.id',$id)->first();

                
      
                if($user ==true){

                 $insert=new deriver_details;
                 $insert->driver_id=$id;
                 $insert->card_number=$card_number;
                 $insert->card_image=$card_images;
                 $insert->driving_license_image=$driving_license_images;
                 $insert->car_license_image=$car_license_images;
                 $insert->front_vehical_image=$front_vehical_images;
                 $insert->created_at=$dateTime; 
                 $insert->save();



                

                if($bank_account ==1 && !empty($bank)){
                  $account=new user_accounts;
                  $account->user_id=$id;
                  $account->bank=$bank;
                  $account->type='bank';
                  $account->payment='credit card';
                  $account->created_at=$dateTime;
                  $account->save();
                }

                if($apple_account ==1 && !empty($number)){
                  $account2=new user_accounts;
                  $account2->user_id=$id;
                  $account2->account_number=$number;
                  $account2->type='apple_bay';
                  $account2->payment='STCPAy account';
                  $account2->created_at=$dateTime;
                  $account2->save();
                }
                  
                      $message['data']=$select;
                      $message['error']=0;
                       $message['message']='regesteration success';
                }else{
                      $message['data']=$select;
                      $message['error']=1;
                       $message['message']='error in save';
                }
              

            }
     }else{
         
         

       
             if($request->has('phone')){
             $phoneexist=User::select('id','phone','first_name','image','password as passwords')->where('phone',$phone)->first();
             }else{
                 $message['error']=5;
                 $message['message']='please enter phone';
                 
                   return response()->json($message);
             }
           if($phoneexist != NULL){
                  $message['data']=$phoneexist;
                  $message['error']=4;
                  $message['message']='phone  exist';
                   
           }else{
                 
                  try{
                    $str='driver';
		                  
		                    $user_token=hash('sha256', Str::random(60)).time().rand(100000,999999).sha1($str);// give a unique name to file to be saved
		                    
		            
		           }catch(Exception $ex){
		                $message['error']=4;
                        $message['message']='error in generate key';    
		          }
        
        
                   $user=User::insertGetId([
                       'first_name'=>$fname,
                       'last_name'=>$lname,
                       'email'=>$email,
                       'phone'=>$phone,
                       'password'=>$pass,
                       'state'=>5,
                       'registy_state'=>1,
                       'image'=>$images,
                       'user_token'=>$user_token,
                       'gender'=>$gender,
                       'longitude'=>$longitude,
                       'latitude'=>$latitude,
                       'is_driver'=>1,
                       'firebase_token'=>$firebase_token,
                       'created_at'=>$dateTime]);
              

         
          
                  
            $select=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwods','users.phone','roles.role as state','users.gender','users.image','users.longitude','users.latitude','users.firebase_token','users.user_token')
              ->join('roles','users.state','=','roles.id')
           //  ->join('driver_vehicles_details','users.id','=','driver_vehicles_details.driver_id')
               ->where('users.id',$user)->first();

                
      
                if($user >0){

                 $insert=new deriver_details;
                 $insert->driver_id=$user;
                 $insert->card_number=$card_number;
                 $insert->card_image=$card_images;
                 $insert->driving_license_image=$driving_license_images;
                 $insert->car_license_image=$car_license_images;
                 $insert->front_vehical_image=$front_vehical_images;
                 $insert->created_at=$dateTime; 
                 $insert->save();



                	$set=new setting;
                	$set->user_id=$user;
                	$set->notification="ON";
                    $set->language=1;
                	$set->created_at=$dateTime;
                	$set->save();

                  $wal=new Wallet;
                  $wal->user_id=$user;
                  $wal->credit=0;
                  $wal->created_at=$dateTime;
                  $wal->save();
                 
                    $addRate = new \App\Rating;
                    $addRate->user_id = 1;
                    $addRate->driver_id = $user;
                    $addRate->rate = 5;
                    $addRate->comment = "رجل ممتاز";
                    $addRate->who_rate = "user";
                    $addRate->created_at = $dateTime;
                    $addRate->save();

                if($bank_account ==1 && !empty($bank)){
                  $account=new user_accounts;
                  $account->user_id=$user;
                  $account->bank=$bank;
                  $account->type='bank';
                  $account->payment='credit card';
                  $account->created_at=$dateTime;
                  $account->save();
                }

                if($apple_account ==1 && !empty($number)){
                  $account2=new user_accounts;
                  $account2->user_id=$user;
                  $account2->account_number=$number;
                  $account2->type='apple_bay';
                  $account2->payment='STCPAy account';
                  $account2->created_at=$dateTime;
                  $account2->save();
                }
                  
                      $message['data']=$select;
                      $message['error']=0;
                       $message['message']='regesteration success';
                }else{
                      $message['data']=$select;
                      $message['error']=1;
                       $message['message']='error in save';
                }
              

            }
         
         
     }
            }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
            return response()->json($message);
     }



  public function edit_account(Request $request)
  {
   try{

          $token=$request->input('user_token');
          $account=$request->input('account_id');
          $name=$request->input('name');
          $address=$request->input('address');

          $number=$request->input('account_number');
          $country=$request->input('country');
          $phone=$request->input('phone');

          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
        if($request->has('user_token') && $check_token !=NULL){
       
          
        
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

               
          $update=user_accounts::where('id',$account)
          ->update([
            'account_name'=>$name,
             'address'=>$address, 
            'account_number'=>$number,
            'country'=>$country,
            'phone'=>$phone,
            'updated_at'=>$dateTime]);
                   
            $select=user_accounts::where('id',$account)->first();

                
      
                if($update ==true){
                  
                      $message['data']=$select;
                      $message['error']=0;
                       $message['message']='update  success';
                }else{
                      $message['data']=$select;
                      $message['error']=1;
                       $message['message']='error in update';
                }
              

            
        }else{
          $message['error']=3;
            $message['message']='this token is not exist';
        }
      }catch(Exception $ex){
          $message['error']=2;
          $message['message']='error'.$ex->getMessage();
      }
            return response()->json($message);
  }
  
  
  
  public function add_account(Request $request)
  {
   try{

          $token=$request->input('user_token');
          
          $name=$request->input('name');
          $number=$request->input('account_number');
           $date=$request->input('expire_date');
             $code=$request->input('code');
           
            $address=$request->input('address');
          $country=$request->input('country');
          $phone=$request->input('phone');

          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
        if($request->has('user_token') && $check_token !=NULL){
       
          
        
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

               
          $update=user_accounts::insertGetId([
              'user_id'=>$check_token['id'],
            'account_name'=>$name,
            'expire_date'=>$date,
            'code'=>$code,
             'address'=>$address, 
            'account_number'=>$number,
            'country'=>$country,
            'phone'=>$phone,
            'type'=>'credit_card',
            'payment'=>'credit card',
            'updated_at'=>$dateTime]);
                   

                
      
                if($update ==true){
                  
                      $message['error']=0;
                       $message['message']='add  success';
                }else{
                      $message['error']=1;
                       $message['message']='error in add';
                }
              

            
        }else{
          $message['error']=3;
            $message['message']='this token is not exist';
        }
      }catch(Exception $ex){
          $message['error']=2;
          $message['message']='error'.$ex->getMessage();
      }
            return response()->json($message);
  }

  public function my_accounts(Request $request)
  {
   try{

          $token=$request->input('user_token');
          

          $check_token=User::where('user_token',$token)->first();
          
          
        if($request->has('user_token') && $check_token !=NULL){
       
          
                   
            $select=user_accounts::select('user_accounts.id','banks.name as bank_name','user_accounts.account_name','user_accounts.address','user_accounts.account_number')
             ->leftJoin('banks','user_accounts.bank','=','banks.id')
            ->where('user_accounts.user_id',$check_token['id'])
            ->get();

                
      
                if(count($select)>0){
                  
                      $message['data']=$select;
                      $message['error']=0;
                       $message['message']='show  success';
                }else{
                      $message['data']=$select;
                      $message['error']=1;
                       $message['message']='error in show';
                }
              

            
        }else{
          $message['error']=3;
            $message['message']='this token is not exist';
        }
      }catch(Exception $ex){
          $message['error']=2;
          $message['message']='error'.$ex->getMessage();
      }
            return response()->json($message);
  }

   public function make_defualt(Request $request)
  {
   try{

          $token=$request->input('user_token');
          $account=$request->input('account_id');
        

          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
        if($request->has('user_token') && $check_token !=NULL){
       

          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

         $check=user_accounts::where([['user_id',$check_token['id']],['main','=',1]])->first();

         if($check !=null){
           $update_p=user_accounts::where([['user_id',$check_token['id']],['id','!=',$account]])
          ->update([
            'main'=>0,
            'updated_at'=>$dateTime]);

              
       
         }
                   
              $update=user_accounts::where([['user_id',$check_token['id']],['id',$account]])
          ->update([
            'main'=>1,
            'updated_at'=>$dateTime]);
                
      
                if($update ==true){
                  
                 
                      $message['error']=0;
                       $message['message']='update  success';
                }else{
                     
                      $message['error']=1;
                       $message['message']='error in update';
                }
              

            
        }else{
          $message['error']=3;
            $message['message']='this token is not exist';
        }
      }catch(Exception $ex){
          $message['error']=2;
          $message['message']='error'.$ex->getMessage();
      }
            return response()->json($message);
  }



 //update_location of users
  public function update_location(Request $request)
  {
   try{

       	  $token=$request->input('user_token');
       	 // $token=$request->input('user_id');
          $longitude=$request->input('longitude');
          $latitude=$request->input('latitude');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
        if($request->has('user_token') && $check_token !=NULL){
       
          
        
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

               
          $user=User::where('id',$check_token['id'])
          ->update(['longitude'=>$longitude ,'latitude'=>$latitude,'updated_at'=>$dateTime]);
                   
            $select=User::select('longitude','latitude')
            
             ->where('users.id',$check_token['id'])->first();

                
      
                if($user ==true){
                  
                      $message['data']=$select;
                      $message['error']=0;
                       $message['message']='update location success';
                }else{
                      $message['data']=$select;
                      $message['error']=1;
                       $message['message']='error in save';
                }
              

            
        }else{
        	$message['error']=3;
            $message['message']='this token is not exist';
        }
      }catch(Exception $ex){
          $message['error']=2;
          $message['message']='error'.$ex->getMessage();
      }
            return response()->json($message);
  }
  
  //current location
    public function current_location(Request $request)
  {
   try{

       	  $token=$request->input('user_token');
       	
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
        if($request->has('user_token') && $check_token !=NULL){
       
          
        
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

               
           
            $select=User::select('id','first_name','longitude','latitude')
            
             ->where('users.id',$check_token['id'])->first();

                
      
                if($select !=null){
                  
                      $message['data']=$select;
                      $message['error']=0;
                       $message['message']='show location success';
                }else{
                      $message['data']=$select;
                      $message['error']=1;
                       $message['message']='error in show data';
                }
              

            
        }else{
        	$message['error']=3;
            $message['message']='this token is not exist';
        }
      }catch(Exception $ex){
          $message['error']=2;
          $message['message']='error'.$ex->getMessage();
      }
            return response()->json($message);
  }

     public function user_login(Request $request)
     {
      try{
         $email=$request->input('phone');
         $pass=$request->input('passwords');
           $longitude=$request->input('longitude');
           $latitude=$request->input('latitude');
         $firebase_token=$request->input('firebase_token');

        
        
           $userexist=User::select('users.id','users.email','users.state')
            
             ->Where([['users.email',$email],['users.password',$pass]])
             ->orWhere([['users.phone',$email],['users.password',$pass]])

             ->first();
             
             
                try{
                    
		                  
		                    $user_token=sha1($userexist['id']);
		                    //rand(1000000,9999999).time();// give a unique name to file to be saved
		                    
		             $insert=User::Where([['users.email',$email],['users.password',$pass]])
             ->orWhere([['users.phone',$email],['users.password',$pass]])
             ->update([
              'longitude'=>$longitude,
              'latitude'=>$latitude,
		          'user_token'=>$user_token,
		          'firebase_token'=>$firebase_token

		                    ]);
		                    
		           }catch(Exception $ex){
		                $message['error']=4;
                   $message['message']='error in generate key';    
		          }
              //$state=$userexist->state;

              if($userexist !=null){

                 
             $select=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwods','users.phone','users.state','users.image','users.user_token','users.firebase_token')
                        
                         ->where('users.id',$userexist->id)->first();

              
               
                   $message['data']=$select;
                   $message['error']=0;
                   $message['message']='login success';

             }else{
                    $message['data']=$userexist;
                    $message['error']=1;
                     $message['message']='data is wrong';
             }
            
            }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
            }
      
            return response()->json($message);
     }

     

    public function edit_userProfile(Request $request)
    {
       try{
            $token=$request->input('user_token');
          
               
          $fname=$request->input('first_name');
          $lname=$request->input('last_name');
           
          $email=$request->input('email');
          $phone=$request->input('phone');
          $image=$request->file('image');
          $pass=$request->input('passwords');
          $gender=$request->input('gender_id');
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
     
          
          
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));


          

           if(isset($image)) {
                        $new_name = $image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/users';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
               }else{
                  $images =User::where('id',$check_token['id'])->value('image');      
                 }
     

         

             $emailexist=User::where([['email',$email]])->first();
             $phoneexist=User::where([['phone',$phone]])->first();
             
           if($phoneexist != NULL && $phoneexist->id != $check_token->id){
                  $message['error']=5;
                  $message['message']='phone  exist';     
                  return response()->json($message);

           }
           if($emailexist != NULL && $emailexist->id != $check_token->id){
                  $message['error']=6;
                  $message['message']='email  exist';
                              return response()->json($message);

           }
           
        
              if( $request->has('email')){
                $emails = $email;
              }else{
                $emails = User::where('id','!=',$check_token['id'])->value('email');
              }

              
              if( $request->has('phone')){
                $phones = $phone;
              }else{
                $phones = User::where('id','!=',$check_token['id'])->value('phone');
              }

          $update=User::where('user_token',$token)->update(['first_name'=>$fname,'last_name'=>$lname,'email'=>$emails,'phone'=>$phones,'password'=>$pass,'image'=>$images,'gender'=>$gender,'updated_at'=>$dateTime]);

              
              $select=User::select('users.id','users.first_name','users.last_name','users.phone','users.email','users.image','users.state','users.password as passwords','users.user_token')
                        
                     
                  ->where('users.user_token',$token)->first();
               
                if($update == true){
                  
                      $message['data']=$select;
                      $message['error']=0;
                       $message['message']='update success';
                }else{
                      $message['data']=$select;
                      $message['error']=1;
                       $message['message']='error in save';
                }
              

            
          }else{
              $message['error']=3;
              $message['message']='this token is not exist';
          }
            }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
            return response()->json($message);
     }

    public function show_user(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
               
            $select=User::select('users.id','users.first_name','users.last_name','users.phone','users.email','users.image','roles.role as state','users.password as passwords','users.user_token','gender.name as gender')
            ->join('roles','users.state','=','roles.id') 
            ->leftjoin('gender','users.gender','=','gender.id')         
             ->where('user_token',$token)->first();
        
        
                
          if($select !=null){

            $message['data']=$select;
           
         
            $message['error']=0;
            $message['message']='user data';

          }else{
            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data for user ';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

    //add drivers
    public function add_driver(Request $request)
     {
       try{

         $token=$request->input('user_token');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

          $fname=$request->input('first_name');
          $lname=$request->input('last_name');
         
            $email=$request->input('email');
            $phone=$request->input('phone');
            $image=$request->file('image');
            $pass=$request->input('passwords');
             $government=$request->input('government_id');
             $city=$request->input('city_id');
              $date=$request->input('birth_date');
              $gender=$request->input('gender_id');

              $vehicle_id=$request->input('vehicle_id');
              $card_number=$request->input('card_number');
              $vehicle_type=$request->input('vehicle_type');
              $production_year=$request->input('production_year');
              $work_way=$request->input('work_way');
              $how_you_hear_arabawy=$request->input('how_you_hear_arabawy');
              $card_image=$request->file('card_image');
              $driving_license_image=$request->file('driving_license_image');
              $car_license_image=$request->file('car_license_image');
              $front_vehical_image=$request->file('front_vehical_image');
              $back_vehical_image=$request->file('back_vehical_image');
              $firebase_token=$request->input('firebase_token');
            

                
          $firebase_token=$request->input('firebase_token');
          
        
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));


           if(isset($image)) {
                        $new_name = $image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/users';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
               }else{
                  $images = NULL;       
              }

               if(isset($card_image)) {
                        $new_name = $card_image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/cards';
                        $card_image->move($destinationPath_id, $savedFileName);
            
                        $card_images = $savedFileName;
                      
               }else{
                  $card_images = NULL;       
              }
     
          if(isset($driving_license_image)) {
                        $new_name = $driving_license_image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/license';
                        $driving_license_image->move($destinationPath_id, $savedFileName);
            
                        $driving_license_images = $savedFileName;
                      
               }else{
                  $driving_license_images = NULL;       
              }
     
    
                if(isset($car_license_image)) {
                          $new_name = $car_license_image->getClientOriginalName();
                          $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                          $destinationPath_id = 'uploads/license';
                          $car_license_image->move($destinationPath_id, $savedFileName);
              
                          $car_license_images = $savedFileName;
                        
                 }else{
                    $car_license_images = NULL;       
                }
     
          if(isset($front_vehical_image)) {
                        $new_name = $front_vehical_image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/vehicels';
                        $front_vehical_image->move($destinationPath_id, $savedFileName);
            
                        $front_vehical_images = $savedFileName;
                      
               }else{
                  $front_vehical_images = NULL;       
              }
     
          if(isset($back_vehical_image)) {
                        $new_name = $back_vehical_image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/vehicels';
                        $back_vehical_image->move($destinationPath_id, $savedFileName);
            
                        $back_vehical_images = $savedFileName;
                      
               }else{
                  $back_vehical_images = NULL;       
              }
     

     

                //verification
           
            
             $emailexist=User::select('id','email')->where('email',$email)->get();
             $phoneexist=User::select('id','phone')->where('phone',$phone)->get();
           
           if(count($phoneexist) >0){
                  $message['error']=4;
                   $message['message']='phone  exist';
           }elseif(count($emailexist) >0){
                  $message['error']=5;
                   $message['message']='email  exist';
           }else{
                 
               
            

          $user=User::insertGetId(['first_name'=>$fname,'last_name'=>$lname,'email'=>$email,'phone'=>$phone,'password'=>$pass,'state'=>5,'image'=>$images,'government_id'=>$government, 'city_id'=>$city, 'birth_date'=>$date,'gender'=>$gender,'agent'=>$check_token['id'],
            'firebase_token'=>$firebase_token,'created_at'=>$dateTime]);
          
                  
           /* $select=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwods','users.phone','roles.role as state','users.image','users.longitude','users.latitude','users.firebase_token')
              ->join('roles','users.state','=','roles.id')
           //  ->join('driver_vehicles_details','users.id','=','driver_vehicles_details.driver_id')
               ->where('users.id',$user)->first();*/

                
      
                if($user >0){

               $insert=new deriver_details;
               $insert->driver_id=$user;
               $insert->vehicle_id=$vehicle_id;
               $insert->card_number=$card_number;
               $insert->vehicle_type=$vehicle_type;
                $insert->production_year=$production_year;
                $insert->work_way=$work_way;
                $insert->how_you_hear_arabawy=$how_you_hear_arabawy;
                $insert->card_image=$card_images;
                 $insert->driving_license_image=$driving_license_images;
                  $insert->car_license_image=$car_license_images;
                  $insert->front_vehical_image=$front_vehical_images;
                   $insert->back_vehical_image=$back_vehical_images;
                   $insert->created_at=$dateTime; 
                    $insert->save();



                  $set=new setting;
                  $set->user_id=$user;
                  $set->notification="ON";
                  $set->language=1;
                  $set->created_at=$dateTime;
                  $set->save();


                  $wal=new Wallet;
                  $wal->user_id=$user;
                  $wal->credit=0;
                  
                  $wal->created_at=$dateTime;
                  $wal->save();
                  
                     // $message['data']=$insert;
                      $message['error']=0;
                       $message['message']='add agent success';
                }else{
                     // $message['data']=$insert;
                      $message['error']=1;
                       $message['message']='error in save data';
                }
              

            }
           }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }
            }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
            return response()->json($message);
     }

}
