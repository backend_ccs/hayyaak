<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use App\compliance_image;
use App\Shopes;
use App\complain_question;
use Illuminate\Support\Facades\DB;

use App\Compliance;
use App\City;
use App\Notification;
use App\Messages;
use App\shope_category;
use App\Wallet;
use App\Copon;
use App\Requests;


use carbon\carbon;
define( 'API_ACCESS_KEY12','AAAAnSDEgLc:APA91bFisJ6mf6QmnpUvaC48ND3u4u_ULaQNnR4fxCRur392hErem3qi3fjRQ05wXg45D8NrhehK4Bp8h1f_uUGW0YbSQBmxDRWg3pi9PBkHvYnE8GHqjYs3JRTmCab08jUJQph9luxS');

class dashcontroller extends Controller
{
     public $message=array();
     //count
      public function data_count(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){



                 $created_at = carbon::now()->toDateTimeString();
           $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
           
             $drivers=User::where('state',2)->count();
             $users=User::where('state',3)->count();
              $allcomplaince=Compliance::count(); 
             $wcomplaince=Compliance::where('state','wait')->count(); 
             $complaince=Compliance::where('state','!=','wait')->count();
             $worders=Requests::count();
             $Shopes=Shopes::where('branch',NULL)->count();
             $cat=shope_category::where('id','!=',4)->count();
             $activecopon=Copon::where('end_date','>',$dateTime)->count();
              $notactivecopon=Copon::where('end_date','<',$dateTime)->count();
             
            
          
          
            $message['drivers']=$drivers;
            $message['users']=$users;
            $message['allcomplaince']=$allcomplaince;
            $message['orders']=$worders;
            $message['Shopes']=$Shopes;
            $message['category']=$cat;
            $message['activecopon']=$activecopon;
            $message['notactive_copon']=$notactivecopon;
        
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
     
     
     
     //d
   public function show_activedrivers(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwods','users.phone','roles.role as state','gender.name as gender','users.rate','users.image','users.created_at')
              ->join('roles','users.state','=','roles.id')
             ->join('offers','users.id','=','offers.driver_id')
             ->leftjoin('gender','users.gender','=','gender.id')

               ->where('users.state',2)->distinct()->get();

          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
     
  //  
  
   public function show_notificationstate(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=DB::select('select id,name from notification_state');
         
              

          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
  
  
  
    public function show_allusers(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=User::select('users.id','users.first_name','users.last_name','users.email','gender.name as gender','users.password as passwods','users.phone','roles.role as state','users.image','users.rate','users.created_at')
              ->join('roles','users.state','=','roles.id')
               ->leftjoin('gender','users.gender','=','gender.id')
               ->where([['users.state',3],['type','=',NULL]])->get();

          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }


  public function show_users(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=User::select('users.id','users.first_name','users.last_name')
         
              
               ->where([['users.state',3],['type','=',NULL]])
               ->orwhere([['users.state',2],['type','=',NULL]])->get();

          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
   
   
    public function search_users(Request $request)
   {
     try{
       $token=$request->input('user_token');
       $name=$request->input('name');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

           
           
           
         $select=User::select('users.id','users.first_name','users.last_name')
              
               ->where('users.first_name','Like','%'.$name.'%')
                ->orWhere('users.last_name','Like','%'.$name.'%')->get();

          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
   
   
 
  public function show_blockedusers(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=User::select('users.id','users.first_name','users.last_name','users.email','gender.name as gender','users.password as passwods','users.phone','roles.role as state','users.image','users.rate','users.created_at')
              ->join('roles','users.state','=','roles.id')
               ->leftjoin('gender','users.gender','=','gender.id')
               ->where('users.state',9)->get();

          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }

 
    public function show_activeusers(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=User::select('users.id','users.first_name','users.last_name','users.email','gender.name as gender','users.password as passwods','users.phone','roles.role as state','users.image','users.rate','users.created_at')
              ->join('roles','users.state','=','roles.id')
             ->join('requestes','users.id','=','requestes.user_id')
              ->leftjoin('gender','users.gender','=','gender.id')
               ->where('users.state',3)->distinct()->get();

          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
     
     
    Public function show_chat(Request $request)
    {
         
         try{

    	      $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
        


               $phone=$request->input('phone');
               $order=$request->input('order');
               
               
               if($request->has('phone') && $request->has('order')){
               

                  $all_mess=Messages::select('messages.id','messages.request_id','messages.message','sender.id as sender_id','sender.first_name as sender_name',
                  'sender.phone as sender_phone','sender.image as sender_image','receiver.id as receiver_id','receiver.first_name as receiver_name','receiver.image as receiver_image',
                  'receiver.phone as receiver_phone','messages.image','messages.voice','messages.who_sent','messages.created_at','messages.updated_at')
                    ->join('users as sender','messages.sender_id','=','sender.id')
                    ->leftJoin('users as receiver','messages.receiver_id','=','receiver.id')
                
                    
                  
                  ->where([['messages.request_id',$order],['receiver.phone' ,'Like','%'.$phone.'%']])
                    ->orWhere([['messages.request_id',$order],['sender.phone' ,'Like','%'.$phone.'%']])->get();
                    
                    
                    
                        if(count($all_mess) >0)
                	      {
                	        $message['data']=$all_mess;
            
                	      $message['error']=0;
                	      $message['message']='show message body';
                	      }else{
            
                	        $message['data']=$all_mess;
            
                	      $message['error']=1;
                	      $message['message']='no message';
                	      }
                    
                    
               }elseif($request->has('order')){
                   
                       $all_mess=Messages::select('messages.id','messages.request_id','messages.message','sender.id as sender_id','sender.first_name as sender_name',
                  'sender.phone as sender_phone','sender.image as sender_image','receiver.id as receiver_id','receiver.first_name as receiver_name','receiver.image as receiver_image',
                  'receiver.phone as receiver_phone','messages.image','messages.voice','messages.who_sent','messages.created_at','messages.updated_at')
                    ->join('users as sender','messages.sender_id','=','sender.id')
                    ->join('users as receiver','messages.receiver_id','=','receiver.id')
                    
                  
                  ->where('messages.request_id',$order)
                    ->get(); 
                   
                   
                    if(count($all_mess) >0)
            	      {
            	        $message['data']=$all_mess;
        
            	      $message['error']=0;
            	      $message['message']='show message body';
            	      }else{
        
            	        $message['data']=$all_mess;
        
            	      $message['error']=1;
            	      $message['message']='no message';
            	      }
                   
               }else{
                   
                     $message['error']=4;
            	      $message['message']='please enter data to search';
                   
                   
               }
            


    	      
    	     }else{
    	           $message['error']=3;
    	           $message['message']='this token is\'t exist';
    	      }

           }catch(Exception $ex){
             
                $message['error']=2;
                $message['message']='error'.$ex->getMessage();

           }

      return response()->json($message);
    }   
     
     
     // credit
     
  public function show_lesscredit(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwods','users.phone','roles.role as state','users.image','wallet.credit','users.created_at')
              ->join('roles','users.state','=','roles.id')
              ->join('wallet','users.id','=','wallet.user_id')
               ->where([['users.state',2],['wallet.credit','<=',100]])->distinct()->get();

          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
     
     
     
  public function show_morecredit(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwods','users.phone','roles.role as state','users.image','wallet.credit','users.created_at')
              ->join('roles','users.state','=','roles.id')
              ->join('wallet','users.id','=','wallet.user_id')
               ->where([['users.state',2],['wallet.credit','>',100]])->distinct()->get();

          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
   
   
   
       public function  send_credit_notify(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
            $id=$request->input('user_id');
            $comment=$request->input('comment');
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     


                  
               $user_data=Wallet::where('user_id',$id)->value('credit');
               
               if($user_data < 100){
                   
                       try{       
                   //  $cc = 0;
                     
                    $title ='notification from admin ';
                    $body =$comment;
                    $body_ar = $comment;
                    $title_ar = "أشعار من الادمن";
                     
            
            
                   $get_user_token =User::select('firebase_token')->where('id', '=',$id)->first();
                   
                      // return $get_user_token[0]->firebase_token;
            
            
                                 ////////////////////////////////////////////////////////
                                 
                                   $msg = array
                                          (
                                    'body'  => $body,
                                    'title' => $title,
                                    'state'=>NULL,
                                              
                                          );
                                  $fields = array
                                      (
                                        'to'    => $get_user_token['firebase_token'],
                                        'notification'  => $msg
                                      );
                                  
                                                   
            
                                  $headers = array
                                      (
                                        'Authorization: key=' . API_ACCESS_KEY12,
                                        'Content-Type: application/json'
                                      );
                                #Send Reponse To FireBase Server  
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    //echo $result;
                                    curl_close( $ch );
                             
                    
                    
                        $save=new Notification;
                        $save->title=$title;
                        $save->body=$body;
                        $save->title_ar = $title_ar;
                        $save->body_ar = $body_ar;
                        $save->sender=1;
                        $save->is_read=0;
                        // $save->request_id=$complain_data['order_id'];
                        $save->user_id=$id;
                        $save->created_at=$dateTime;
                        $save->save();
                        
                        
                        $message['error']=0;
                        $message['message']='send notification success';
                        
                        
                     }catch(Exception $ex){
                          $message['error']=4;
                           $message['message']='error in send notification';
                          
                     }
            
                   
                   
                   
                   
                 
               }else{
                   
                    
                       try{      
                           
                   //  $cc = 0;
                     
                    $title ='notification from admin ';
                    $body =$comment;
                    $body_ar = $comment;
                    $title_ar = "أشعار من الادمن";
                     
                     
            
            
                   $get_user_token =User::select('firebase_token')->where('id', '=',$id)->first();
                   
                      // return $get_user_token[0]->firebase_token;
            
            
                                 ////////////////////////////////////////////////////////
                                 
                                   $msg = array
                                          (
                                    'body'  => $body,
                                    'title' => $title,
                                    'state'=>NULL,
                                              
                                          );
                                  $fields = array
                                      (
                                        'to'    => $get_user_token['firebase_token'],
                                        'notification'  => $msg
                                      );
                                  
                                                   
            
                                  $headers = array
                                      (
                                        'Authorization: key=' . API_ACCESS_KEY12,
                                        'Content-Type: application/json'
                                      );
                                #Send Reponse To FireBase Server  
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    //echo $result;
                                    curl_close( $ch );
                             
                    
                    
                        $save=new Notification;
                        $save->title=$title;
                        $save->body=$body;
                        $save->title_ar = $title_ar;
                        $save->body_ar = $body_ar;
                        $save->sender=1;
                        $save->is_read=0;
                        // $save->request_id=$complain_data['order_id'];
                        $save->user_id=$id;
                        $save->created_at=$dateTime;
                        $save->save();
                        
                        
                           
                        $message['error']=0;
                        $message['message']='send notification success';
                        
                        
                        
                     }catch(Exception $ex){
                          $message['error']=4;
                           $message['message']='error in send notification';
                          
                     }
                     
                     
                     
                     
             $update=User::where('id',$id)
                  ->update(
                   [   
                       'state'=>10,
                       'updated_at'=>$dateTime]);
               }
          
           }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }
   
   
   
   
     public function  send_copon_specialusers(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
          
             $notify_arr=array();  
             
             if($request->has('user_token') && $check_token !=NULL){
           
            $id=$request->input('user_id'); 
            $copon=$request->input('copon');
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     

        
                       
                              
                                try{       
                           //  $cc = 0;
                             
                         $title ='copon from admin ';
                         $body ='admin sent for  you a copon of number='.$copon;
                         $title_ar = "كوبون من الادمن";
                         $body_ar =  $copon."= أرسل الادمن لك كوبون من الرقم ";
                    
                           $get_user_token =User::select('firebase_token')->where('id', '=',$id)->first();
                           

                    
                                         ////////////////////////////////////////////////////////
                             
                               $msg = array
                                      (
                                'body'  => $body,
                                'title' => $title,
                                'state'=>NULL,
                                          
                                      );
                              $fields = array
                                  (
                                    'to'    => $get_user_token->firebase_token,
                                    'notification'  => $msg
                                  );
                              
                                               
        
                              $headers = array
                                  (
                                    'Authorization: key=' . API_ACCESS_KEY12,
                                    'Content-Type: application/json'
                                  );
                            #Send Reponse To FireBase Server  
                                $ch = curl_init();
                                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                curl_setopt( $ch,CURLOPT_POST, true );
                                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                $result = curl_exec($ch );
                                //echo $result;
                                curl_close( $ch );
                                
                                
                             
                        $save=new Notification;
                        $save->title=$title;
                        $save->body=$body;
                        $save->title_ar = $title_ar;
                        $save->body_ar = $body_ar;
                        $save->sender=1;
                        $save->is_read=0;
                         //$save->request_id=$complain_data['order_id'];
                        $save->user_id=$id;
                        $save->created_at=$dateTime;
                        $save->save();
                
                
                           if($save == true){
                                
                           $message['error']=0;
                            $message['message']='notification with copon sent ';
                            }else{
                                
                                
                            $message['error']=1;
                            $message['message']='error in send notification ';
                            }
                    
                 }catch(Exception $ex){
                      $message['error']=4;
                       $message['message']='error in send notification';
                      
                 }  
                              
                 

               }else{
                     $message['error']=3;
                 $message['message']='this token is not exist';
                }

          }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
          }  

       return response()->json($message);
     }
     
     
     
        

      public function  send_copon_allusers(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
          
             $notify_arr=array();  
             
             if($request->has('user_token') && $check_token !=NULL){
           
          
            $copon=$request->input('copon');
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     

        
                        
                              
                                        try{       
                             $cc = 0;
                             
                    $title ='copon from admin ';
                    $body ='admin sent for  you a copon of number='.$copon;
                    $title_ar = "كوبون من الادمن";
                    $body_ar =  $copon." = أرسل الادمن لك كوبون من الرقم ";
                           

                    $get_user_token =User::select('id','firebase_token')->where('state', '=',3)->get();


                     ////////////////////////////////////////////////////////
                     
            foreach($get_user_token as $token){
                                     
                                    // echo 'Hello';
                     //   $registrationIds = ;
                    #prep the bundle
                         $msg = array
                              (
                    		'body' 	=> $body,
                    		'title'	=> $title,
                    		'state'=>NULL,
                                 	
                              );
                    	$fields = array
                    			(
                    				'to'		=> $get_user_token[$cc]->firebase_token,
                    				'notification'	=> $msg
                    			);
                    	
                    	                 

                    	$headers = array
                    			(
                    				'Authorization: key=' . API_ACCESS_KEY12,
                    				'Content-Type: application/json'
                    			);
                    #Send Reponse To FireBase Server	
                    		$ch = curl_init();
                    		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                    		curl_setopt( $ch,CURLOPT_POST, true );
                    		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                    		$result = curl_exec($ch );
                    		//echo $result;
                    		curl_close( $ch );
             
           
                         $save=new Notification;
                         $save->title=$title;
                         $save->body=$body;
                         $save->title_ar = $title_ar;
                         $save->body_ar = $body_ar;
                         $save->sender=1;
                         $save->is_read=0;
                         $save->user_id=$get_user_token[$cc]->id;
                         $save->created_at=$dateTime;
                         $save->save();
             
                       
                        
                          $cc++;   
            }
                                            
                                            
                                         
                                  $agent_ids=User::where('state',3)->pluck('id')->toArray();  
                                  
                                  
                                  foreach($agent_ids as $id){
                                      
                                      $notify_arr[]=array('title'=>$title,'body'=>$body,'title_ar'=>$title_ar, 'body_ar'=> $body_ar,'sender'=>1,'is_read'=>0,'user_id'=>$id,'created_at'=>$dateTime);
                                  }
                            
                            
                                $save=Notification::insert($notify_arr);
                                
                                if($save == true){
                                    
                               $message['error']=0;
                                $message['message']='notification with copon sent ';
                                }else{
                                    
                                    
                                $message['error']=1;
                                $message['message']='error in send notification ';
                                }
                           
                                
                             }catch(Exception $ex){
                                  $message['error']=4;
                                   $message['message']='error in send notification';
                                  
                             }
                       

               }else{
                     $message['error']=3;
                 $message['message']='this token is not exist';
                }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }
     
     
     public function  send_copon_activeusers(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
          
             $notify_arr=array();  
             
             if($request->has('user_token') && $check_token !=NULL){
           
          
            $copon=$request->input('copon');
            
            $ids=$request->input('id');//2,3,5
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     

        $my_arr=array();
        $ids=rtrim($ids,',');
        $my_arr=explode(',',$ids);
                        
                              
                     try{       
                             $cc = 0;
                             
                         $title ='copon from admin ';
                         $body ='admin sent for  you a copon of number= '.$copon;
                         $title_ar = "كوبون من الادمن";
                         $body_ar =  $copon." = أرسل الادمن لك كوبون من الرقم ";
                             

                     $get_user_token=User::select('users.id','users.firebase_token')
                                             ->whereIn('users.id',$my_arr)->get();


                     ////////////////////////////////////////////////////////
                     
            foreach($get_user_token as $token){
                                     
                                    // echo 'Hello';
                     //   $registrationIds = ;
                    #prep the bundle
                         $msg = array
                              (
                    		'body' 	=> $body,
                    		'title'	=> $title,
                    		'state'=>NULL,
                                 	
                              );
                    	$fields = array
                    			(
                    				'to'		=> $get_user_token[$cc]->firebase_token,
                    				'notification'	=> $msg
                    			);
                    	
                    	                 

                    	$headers = array
                    			(
                    				'Authorization: key=' . API_ACCESS_KEY12,
                    				'Content-Type: application/json'
                    			);
                    #Send Reponse To FireBase Server	
                    		$ch = curl_init();
                    		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                    		curl_setopt( $ch,CURLOPT_POST, true );
                    		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                    		$result = curl_exec($ch );
                    		//echo $result;
                    		curl_close( $ch );
             
           
             
                         $save=new Notification;
                         $save->title=$title;
                         $save->body=$body;
                         $save->title_ar = $title_ar;
                         $save->body_ar = $body_ar;
                         $save->sender=1;
                         $save->is_read=0;
                         $save->user_id=$get_user_token[$cc]->id;
                         $save->created_at=$dateTime;
                         $save->save();
                  
             
                       
                        
                          $cc++;   
            }
                                            
                                   
                                  foreach($my_arr as $id){
                                      
                                      $notify_arr[]=array('title'=>$title,'body'=>$body,'title_ar'=>$title_ar, 'body_ar'=>$body_ar,'sender'=>1,'is_read'=>0,'user_id'=>$id,'created_at'=>$dateTime);
                                  }
                            
                            
                                $save=Notification::insert($notify_arr);
                                
                                if($save == true){
                                    
                               $message['error']=0;
                                $message['message']='notification with copon sent ';
                                }else{
                                    
                                    
                                $message['error']=1;
                                $message['message']='error in send notification ';
                                }
                           
                                
                             }catch(Exception $ex){
                                  $message['error']=4;
                                   $message['message']='error in send notification';
                                  
                             }
                              
                              
               }else{
                     $message['error']=3;
                 $message['message']='this token is not exist';
                }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }
     
     
     
     
     
     
    public function  send_copon_allagents(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
          
             $notify_arr=array();  
             
             if($request->has('user_token') && $check_token !=NULL){
           
          
            $copon=$request->input('copon');
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     

        
                        
                              
                                        try{       
                             $cc = 0;
                             
                         $title ='copon from admin ';
                         $body ='admin sent for  you a copon of number='.$copon;
                         $title_ar = "كوبون من الادمن";
                         $body_ar =  $copon." = أرسل الادمن لك كوبون من الرقم ";
                             

                     $get_user_token =User::select('id','firebase_token')->where('state', '=',2)->get();


                     ////////////////////////////////////////////////////////
                     
            foreach($get_user_token as $token){
                                     
                                    // echo 'Hello';
                     //   $registrationIds = ;
                    #prep the bundle
                         $msg = array
                              (
                    		'body' 	=> $body,
                    		'title'	=> $title,
                    		'state'=>NULL,
                                 	
                              );
                    	$fields = array
                    			(
                    				'to'		=> $get_user_token[$cc]->firebase_token,
                    				'notification'	=> $msg
                    			);
                    	
                    	                 

                    	$headers = array
                    			(
                    				'Authorization: key=' . API_ACCESS_KEY12,
                    				'Content-Type: application/json'
                    			);
                    #Send Reponse To FireBase Server	
                    		$ch = curl_init();
                    		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                    		curl_setopt( $ch,CURLOPT_POST, true );
                    		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                    		$result = curl_exec($ch );
                    		//echo $result;
                    		curl_close( $ch );
             
           
                         $save=new Notification;
                         $save->title=$title;
                         $save->body=$body;
                         $save->title_ar = $title_ar;
                         $save->body_ar = $body_ar;
                         $save->sender=1;
                         $save->is_read=0;
                         $save->user_id=$get_user_token[$cc]->id;
                         $save->created_at=$dateTime;
                         $save->save();
             
                       
                        
                          $cc++;   
            }
                                            
                                            
                                         
                                  $agent_ids=User::where('state',2)->pluck('id')->toArray();  
                                  
                                  
                                  foreach($agent_ids as $id){
                                      
                                      $notify_arr[]=array('title'=>$title,'body'=>$body,'title_ar'=>$title_ar, 'body_ar'=> $body_ar,'sender'=>1,'is_read'=>0,'user_id'=>$id,'created_at'=>$dateTime);
                                  }
                            
                            
                                $save=Notification::insert($notify_arr);
                                
                                if($save == true){
                                    
                               $message['error']=0;
                                $message['message']='notification with copon sent ';
                                }else{
                                    
                                    
                                $message['error']=1;
                                $message['message']='error in send notification ';
                                }
                           
                                
                             }catch(Exception $ex){
                                  $message['error']=4;
                                   $message['message']='error in send notification';
                                  
                             }
                              
                              
                 

               }else{
                     $message['error']=3;
                 $message['message']='this token is not exist';
                }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }
     
     
     
     
     
     
     
      public function  edit_credit(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
            $id=$request->input('user_id');
            
            $credit=$request->input('credit');
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     
                $update=Wallet::where('user_id',$id)
                  ->update(
                   [   'credit'=>$credit,
                      
                       'updated_at'=>$dateTime
                    ]);

                  
           

                      if($update == true){
                           
                             $message['error']=0;
                            $message['message']=' incease credit of user successfully';
                            
                            
                                     try{       
                   //  $cc = 0;
                     
                    $title ='your credit increased  ';
                    $body ='admin transefer money to your credit';
                    $title_ar =  "تم زياده رصيدك";
                    $body_ar = "تم تحويل الاموال الى رصيدك من قبل الادمن ";
            
            
                   $get_user_token =User::select('firebase_token')->where('id', '=',$id)->first();
                   
                      // return $get_user_token[0]->firebase_token;
            
            
                                 ////////////////////////////////////////////////////////
                                 
                                   $msg = array
                                          (
                                    'body'  => $body,
                                    'title' => $title,
                                    'state'=>NULL,
                                              
                                          );
                                  $fields = array
                                      (
                                        'to'    => $get_user_token['firebase_token'],
                                        'notification'  => $msg
                                      );
                                  
                                                   
            
                                  $headers = array
                                      (
                                        'Authorization: key=' . API_ACCESS_KEY12,
                                        'Content-Type: application/json'
                                      );
                                #Send Reponse To FireBase Server  
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    //echo $result;
                                    curl_close( $ch );
                             
                    
                    
                        $save=new Notification;
                        $save->title=$title;
                        $save->body=$body;
                        $save->title_ar = $title_ar;
                        $save->body_ar = $body_ar;
                        $save->sender=1;
                        $save->is_read=0;
                         //$save->request_id=$complain_data['order_id'];
                        $save->user_id=$id;
                        $save->created_at=$dateTime;
                        $save->save();
                        
                        
                     }catch(Exception $ex){
                          $message['error']=4;
                           $message['message']='error in send notification';
                          
                     }
            
                            
                         
                        }else{
                           
                             $message['error']=1;
                             $message['message']='error in transefer money';
                        }
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }
        
        
        
        
        
     public function  send_notification_allusers(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){


            $comment=$request->input('comment');
             $image=$request->file('image');
             
                if(isset($image)) {
                        $new_name = $image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/notification';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
               }else{
                  $images = NULL;       
              }
     
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     
                         
          try{
         
         $cc = 0;
         
         $title ='notification from admin';
         $body =$comment;
         $title_ar = "أشعارات  من الادمن";
         $body_ar = $comment;

                     $get_user_token =User::select('id','firebase_token')->where('state', '=',3)->distinct()->get();


                     ////////////////////////////////////////////////////////
                     
            foreach($get_user_token as $token){
                                     
                                    // echo 'Hello';
                     //   $registrationIds = ;
                    #prep the bundle
                         $msg = array
                              (
                    		'body' 	=> $body,
                    		'title'	=> $title,
                    	//	'image'=>$images,
                    		'state'=>NULL,
                                 	
                              );
                    	$fields = array
                    			(
                    				'to'		=> $get_user_token[$cc]->firebase_token,
                    				'notification'	=> $msg
                    			);
                    	
                    	                 

                    	$headers = array
                    			(
                    				'Authorization: key=' . API_ACCESS_KEY12,
                    				'Content-Type: application/json'
                    			);
                    #Send Reponse To FireBase Server	
                    		$ch = curl_init();
                    		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                    		curl_setopt( $ch,CURLOPT_POST, true );
                    		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                    		$result = curl_exec($ch );
                    		//echo $result;
                    		curl_close( $ch );
             
           
             
             
                       
                        
                          $cc++;   
            }
            
         $code = mt_rand(100000, 999999);
            
                      
                                  $users_ids=User::where('state',3)->pluck('id')->toArray();  
                                  
                                  
                                  foreach($users_ids as $id){
                                      
                                      $notify_arr[]=array('title'=>$title,'body'=>$comment,'title_ar'=> $title_ar , 'body_ar'=> $body_ar,'sender'=>1,'is_read'=>0,'user_id'=>$id,'code'=>$code,'state'=>'all_users','image'=>$images,'created_at'=>$dateTime);
                                  }
                                  
                                  
                           $save=Notification::insert($notify_arr);
                                
                                if($save == true){
                                    
                               $message['error']=0;
                                $message['message']='notification sent ';
                                }else{
                                    
                                    
                                $message['error']=1;
                                $message['message']='error in send notification ';
                                }
         
         
                                 }catch(Exception $ex){
                            	    $message['error'] = 2;
                                    $message['message'] = "error('DataBase Error :{$ex->getMessage()}')";
                                 }

                        
            
                       
            }else{
                 $message['error']=3;
                 $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }   
     
     
     public function  send_notification_allagent(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){


            $comment=$request->input('comment');
      $image=$request->file('image');
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
          
                 if(isset($image)) {
                        $new_name = $image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/notification';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
               }else{
                  $images = NULL;       
              }
     
                              
                            try{
         
         $cc = 0;
         
          $title ='notification from admin';
          $body =$comment;
          $title_ar = "أشعارات  من الادمن";
          $body_ar = $comment;   

                     $get_user_token =User::select('id','firebase_token')->where('state', '=',2)->distinct()->get();


                     ////////////////////////////////////////////////////////
                     
            foreach($get_user_token as $token){
                                     
                                    // echo 'Hello';
                     //   $registrationIds = ;
                    #prep the bundle
                         $msg = array
                              (
                    		'body' 	=> $body,
                    		'title'	=> $title,
                    	//	'image'=>$images,
                    		'state'=>NULL,
                                 	
                              );
                    	$fields = array
                    			(
                    				'to'		=> $get_user_token[$cc]->firebase_token,
                    				'notification'	=> $msg
                    			);
                    	
                    	                 

                    	$headers = array
                    			(
                    				'Authorization: key=' . API_ACCESS_KEY12,
                    				'Content-Type: application/json'
                    			);
                    #Send Reponse To FireBase Server	
                    		$ch = curl_init();
                    		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                    		curl_setopt( $ch,CURLOPT_POST, true );
                    		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                    		$result = curl_exec($ch );
                    		//echo $result;
                    		curl_close( $ch );
             
           
             
             
                       
                        
                          $cc++;   
            }
            
                           $code = mt_rand(10000, 99999);
    
                                  $users_ids=User::where('state',2)->pluck('id')->toArray();  
                                  
                                  
                                  foreach($users_ids as $id){
                                      
                                      $notify_arr[]=array('title'=>$title,'body'=>$comment,'title_ar' => $title_ar, 'body_ar'=> $body_ar,'sender'=>1,'is_read'=>0,'user_id'=>$id,'code'=>$code,'state'=>'all_agents','image'=>$images,'created_at'=>$dateTime);
                                  }
                                  
                                  
                           $save=Notification::insert($notify_arr);
                                
                                if($save == true){
                                    
                               $message['error']=0;
                                $message['message']='notification sent ';
                                }else{
                                    
                                    
                                $message['error']=1;
                                $message['message']='error in send notification ';
                                }
         
         
                                 }catch(Exception $ex){
                            	    $message['error'] = 2;
                                    $message['message'] = "error('DataBase Error :{$ex->getMessage()}')";
                                 }

                        
            
                       
            }else{
                 $message['error']=3;
                 $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }   
     
        public function  save_notification(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){

          $title="notification from admin ";
          $comment=$request->input('comment');
          $title_ar = "أشعارات من الادمن";
          $body_ar = $comment;
          
          $state=$request->input('state');
          $image=$request->file('image');


              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
          
                 if(isset($image)) {
                        $new_name = $image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/notification';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
               }else{
                  $images = NULL;       
              }
     
                              
                           
            
                           $code = mt_rand(100, 999);
    
    
        $check=Notification::where([['type',$state],['state','general']])->first();
        
        if($check !=null){
            
            $save=Notification::where([['type',$state],['state','general']])->update([
                
                'title'=>$title,
                'body'=>$comment,
                'title_ar' => $title_ar,
                'body_ar' => $body_ar,
                'image'=>$images,
                'updated_at'=>$dateTime
                
                ]);
            
        }else{
            
                        $save=new Notification;
                        $save->title=$title;
                        $save->body=$comment;
                        $save->title_ar = $title_ar;
                        $save->body_ar = $body_ar;
                        $save->sender=1;
                        $save->is_read=0;
                        $save->image=$images;
                        $save->code=$code;
                        $save->type=$state;
                        $save->state='general';
                        $save->created_at=$dateTime;
                        $save->save();
            
        }
                                         
                       
                                
                                if($save == true){
                                    
                               $message['error']=0;
                                $message['message']='notification sent ';
                                }else{
                                    
                                    
                                $message['error']=1;
                                $message['message']='error in send notification ';
                                }
         
         
                                 

                        
            
                       
            }else{
                 $message['error']=3;
                 $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }   
     
     
     
     public function  send_notification_specialusers(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
           
          $ids=$request->input('user_id');
          $image=$request->file('image');
          $body=$request->input('comment');
            
               
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     

        $my_arr=array();
        $ids=rtrim($ids,',');
        $my_arr=explode(',',$ids);

                  
           
             if(isset($image)) {
                        $new_name = $image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/notification';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
               }else{
                  $images = NULL;       
              }
     
                          
                          
                          
                      
               try{       
                             $cc = 0;
                             
                         $title ='notification from admin ';
                         $body =$body;
                         $title_ar = "أشعارات من الادمن";
                         $body_ar = $body;



                     $get_user_token=User::select('users.id','users.firebase_token')->whereIn('users.id',$my_arr)->get();


                     ////////////////////////////////////////////////////////
                     
            foreach($get_user_token as $token){
                                     
                                    // echo 'Hello';
                     //   $registrationIds = ;
                    #prep the bundle
                         $msg = array
                              (
                    		'body' 	=> $body,
                    		'title'	=> $title,
                    		'state'=>NULL,
                                 	
                              );
                    	$fields = array
                    			(
                    				'to'		=> $get_user_token[$cc]->firebase_token,
                    				'notification'	=> $msg
                    			);
                    	
                    	                 

                    	$headers = array
                    			(
                    				'Authorization: key=' . API_ACCESS_KEY12,
                    				'Content-Type: application/json'
                    			);
                    #Send Reponse To FireBase Server	
                    		$ch = curl_init();
                    		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                    		curl_setopt( $ch,CURLOPT_POST, true );
                    		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                    		$result = curl_exec($ch );
                    		//echo $result;
                    		curl_close( $ch );
             
                       
                        
                          $cc++;   
            }
                                            
                                            
                                         

                                  
                                  foreach($my_arr as $id){
                                      
                                      $notify_arr[]=array('title'=>$title,'body'=>$body, 'title_ar'=> $title_ar, 'body_ar'=>$body_ar,'sender'=>1,'is_read'=>0,'user_id'=>$id,'created_at'=>$dateTime);
                                  }
                            
                            
                                $save=Notification::insert($notify_arr);
                                
                                if($save == true){
                                    
                               $message['error']=0;
                                $message['message']='notification with copon sent ';
                                }else{
                                    
                                    
                                $message['error']=1;
                                $message['message']='error in send notification ';
                                }
                           
                                
                             }catch(Exception $ex){
                                  $message['error']=4;
                                   $message['message']='error in send notification';
                                  
                             }
                              
                   
            
                       
            }else{
                 $message['error']=3;
                 $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }   
   
   
   
        public function show_defaultmessages(Request  $request){
        try{


          $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
        $data =Notification::select('notification.code','notification.body','notification.image','notification_state.name as state','notification.created_at')->distinct()
        ->join('notification_state','notification.type','=','notification_state.id')
        ->where([['notification.code','!=',NULL],['notification.state','=','general']])->get();
        
          if(count($data)>0){

          $message['data']=$data;
          $message['error']=0;
          $message['message']='show data success';
          }else{
          $message['data']=$data;
          $message['error']=1;
          $message['message']='no data';

          }



    
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }
        
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
        }  
         

       
         return response()->json($message);
    }

   
   
     public function show_messagebycode(Request  $request){
        try{


          $token=$request->input('user_token');
            $code=$request->input('code');
          
             $check_token=User::where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
                 
                 
                 
           
        $data =Notification::select('code','body','image')->where('code','=',$code)->first();
        
          if($data != null){

          $message['data']=$data;
          $message['error']=0;
          $message['message']='show data success';
          }else{
          $message['data']=NULL;
          $message['error']=1;
          $message['message']='no data';

          }



    
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }
        
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
        }  
         

       
         return response()->json($message);
    }
    
         public function show_allmessages(Request  $request){
        try{


          $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
        $data =Notification::select('code','body','image','state','created_at')->distinct()->where([['code','!=',NULL],['state','!=','general']])->get();
        
          if(count($data)>0){

          $message['data']=$data;
          $message['error']=0;
          $message['message']='show data success';
          }else{
          $message['data']=$data;
          $message['error']=1;
          $message['message']='no data';

          }



    
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }
        
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
        }  
         

       
         return response()->json($message);
    }

    public function delete_message(Request $request){
    	try{

     $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
        $code = $request->input('code');
        
//$get_code = Notification::where('code',$msg_id)->value('code');
        
        $deleteData = Notification::where('code',$code)->delete();
        
        
          if($deleteData ==true){

          $message['error']=0;
          $message['message']='delete data success';
          }else{
          $message['error']=1;
          $message['message']='error in delete data';

          }


   
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }
 
        
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
        }  
         

       
         return response()->json($message);
    }
   
     public function  resend_notification(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){


            $id=$request->input('code');
            
           
           
           $data=Notification::where('code',$id)->first();
           $title=$data['title'];
           $body=$data['body'];
           $title_ar = $data['title_ar'];
           $body_ar = $data['body_ar'];
           $image=$data['image'];
           
           $who=$data['state'];  //all_users ,all_agents,special_user
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
          
                              
                         if($who =='all_agents'){     
                            try{
         
         $cc = 0;
         
      

                     $get_user_token =User::select('id','firebase_token')->where('state', '=',2)->distinct()->get();


                     ////////////////////////////////////////////////////////
                     
            foreach($get_user_token as $token){
                                     
                                    // echo 'Hello';
                     //   $registrationIds = ;
                    #prep the bundle
                         $msg = array
                              (
                    		'body' 	=> $body,
                    		'title'	=> $title,
                    		'image'=>$image,
                    		'state'=>NULL,
                                 	
                              );
                    	$fields = array
                    			(
                    				'to'		=> $get_user_token[$cc]->firebase_token,
                    				'notification'	=> $msg
                    			);
                    	
                    	                 

                    	$headers = array
                    			(
                    				'Authorization: key=' . API_ACCESS_KEY12,
                    				'Content-Type: application/json'
                    			);
                    #Send Reponse To FireBase Server	
                    		$ch = curl_init();
                    		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                    		curl_setopt( $ch,CURLOPT_POST, true );
                    		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                    		$result = curl_exec($ch );
                    		//echo $result;
                    		curl_close( $ch );
             
           
             
             
                       
                        
                          $cc++;   
            }
            

                                  $users_ids=User::where('state',2)->pluck('id')->toArray();  
                                  
                                  
                                  foreach($users_ids as $id){
                                      
                                      $notify_arr[]=array('title'=>$title,'body'=>$body,'title_ar'=> $title_ar, 'body_ar'=> $body_ar,'sender'=>1,'is_read'=>0,'user_id'=>$id,'state'=>'all_agents','image'=>$image,'created_at'=>$dateTime);
                                  }
                                  
                                  
                           $save=Notification::insert($notify_arr);
                                
                                if($save == true){
                                    
                               $message['error']=0;
                                $message['message']='notification sent ';
                                }else{
                                    
                                    
                                $message['error']=1;
                                $message['message']='error in send notification ';
                                }
         
         
                                 }catch(Exception $ex){
                            	    $message['error'] = 2;
                                    $message['message'] = "error('DataBase Error :{$ex->getMessage()}')";
                                 }
                         }elseif($who =='all_users'){
                             
                                        
                            try{
         
         $cc = 0;
         
         

                     $get_user_token =User::select('id','firebase_token')->where('state', '=',3)->distinct()->get();


                     ////////////////////////////////////////////////////////
                     
            foreach($get_user_token as $token){
                                     
                                    // echo 'Hello';
                     //   $registrationIds = ;
                    #prep the bundle
                         $msg = array
                              (
                    		'body' 	=> $body,
                    		'title'	=> $title,
                    		'image'=>$image,
                    		'state'=>NULL,
                                 	
                              );
                    	$fields = array
                    			(
                    				'to'		=> $get_user_token[$cc]->firebase_token,
                    				'notification'	=> $msg
                    			);
                    	
                    	                 

                    	$headers = array
                    			(
                    				'Authorization: key=' . API_ACCESS_KEY12,
                    				'Content-Type: application/json'
                    			);
                    #Send Reponse To FireBase Server	
                    		$ch = curl_init();
                    		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                    		curl_setopt( $ch,CURLOPT_POST, true );
                    		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                    		$result = curl_exec($ch );
                    		//echo $result;
                    		curl_close( $ch );
             
           
             
             
                       
                        
                          $cc++;   
            }
            

                      
                                  $users_ids=User::where('state',3)->pluck('id')->toArray();  
                                  
                                  
                                  foreach($users_ids as $id){
                                      
                                      $notify_arr[]=array('title'=>$title,'body'=>$body , 'title_ar'=> $title_ar, 'body_ar'=> $body_ar,'sender'=>1,'is_read'=>0,'user_id'=>$id,'state'=>'all_users','image'=>$image,'created_at'=>$dateTime);
                                  }
                                  
                                  
                           $save=Notification::insert($notify_arr);
                                
                                if($save == true){
                                    
                               $message['error']=0;
                                $message['message']='notification sent ';
                                }else{
                                    
                                    
                                $message['error']=1;
                                $message['message']='error in send notification ';
                                }
         
         
                                 }catch(Exception $ex){
                            	    $message['error'] = 2;
                                    $message['message'] = "error('DataBase Error :{$ex->getMessage()}')";
                                 }
                         }else{
                             
                            $id=$data['user_id'];

                             
                                try{       
                   //  $cc = 0;
                  
                     
            
            
                   $get_user_token =User::select('firebase_token')->where('id', '=',$id)->first();
                   
                      // return $get_user_token[0]->firebase_token;
            
            
                                 ////////////////////////////////////////////////////////
                                 
                                   $msg = array
                                          (
                                    'body'  => $body,
                                    'title' => $title,
                                    'image'=>$image,
                                    'state'=>NULL,
                                              
                                          );
                                  $fields = array
                                      (
                                        'to'    => $get_user_token['firebase_token'],
                                        'notification'  => $msg
                                      );
                                  
                                                   
            
                                  $headers = array
                                      (
                                        'Authorization: key=' . API_ACCESS_KEY12,
                                        'Content-Type: application/json'
                                      );
                                #Send Reponse To FireBase Server  
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    //echo $result;
                                    curl_close( $ch );
                             

                    
                        $save=new Notification;
                        $save->title=$title;
                        $save->body=$body;
                        $save->title_ar = $title_ar;
                        $save->body_ar = $body_ar;
                        $save->sender=1;
                        $save->is_read=0;
                         //$save->request_id=$complain_data['order_id'];
                        $save->user_id=$id;
                        $save->image=$image;
                       // $save->code=$code;
                        $save->state='special_user';
                        $save->created_at=$dateTime;
                        $save->save();
                        
                        
                        $message['error']=0;
                        $message['message']='notification sent ';
                         
                        
                        
                                         }catch(Exception $ex){
                                              $message['error']=4;
                                               $message['message']='error in send notification';
                                              
                                         }
                         }

                        
            
                       
            }else{
                 $message['error']=3;
                 $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }   
   
  public function  edit_message(Request $request){
      
      try{
        $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
                 
                       $id=$request->input('code');

                      $image=$request->file('image');
            
           
            
                      $comment=$request->input('comment');
                      
                      
                      
                        $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
          
                 if(isset($image)) {
                        $new_name = $image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/notification';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
               }else{
                  $images = Notification::where('code',$id)->value('image');       
              }
            
            
             $update=Notification::where('code',$id)->update([
                 
                 'body'=>$comment,
                 'image'=>$images,
                 
                 'updated_at'=>$dateTime
                 
                  ]);
                  
                  if($update ==true){
                     
                       $message['error']=0;
                      $message['message']='update success';
                  }else{
                      $message['error']=1;
                      $message['message']='error in update';
                      
                  }
            
            
      
      
      
             }else{
                 $message['error']=3;
                 $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
      
      
      
      
  }
  
  
   
   public function time(){
       
       $string='3hours';
       
       
        $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
          
          
          
         
          
           $string_after=$string[0];
          
          
          
          $after=date('Y-m-d H:i:s',strtotime('+'.$string_after.'hours',strtotime($dateTime)));
          
          
          return $after;
   }
   
   
   //new
    public function  regist_again(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
            $id=$request->input('user_id');
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     
             
             
             $check=User::where('id',$id)->first();
             
             if($check['is_driver']==1){
                 
                 $action=User::where('id',$id)->delete();
             }else{
                   $action=User::where('id',$id)->update([
                       'state'=>3
                       
                       ]);
                 
             }
             
             
             
             
                                           try{       
                   //  $cc = 0;
                     
                    $title ='notification from admin ';
                    $body = "
                    We have reviewed your application to join us as an agent and your request has been rejected for not meeting the conditions. Please re-register";
                   
                    $title_ar = "أشعارات من الادمن";
                    $body_ar =   ' قمنا بمراجعه طلبك للانضمام لنا كمندوب وتم رفض طلبك لعدم استيفاء الشروط من فضلك اعد التسجيل ';
                                             
            
                   $get_user_token =User::select('firebase_token')->where('id', '=',$id)->first();
                   
                      // return $get_user_token[0]->firebase_token;
                      
                      
            
            
                                 ////////////////////////////////////////////////////////
                                 
                                   $msg = array
                                          (
                                    'body'  => $body,
                                    'title' => $title,
                                    'state'=>NULL,
                                              
                                          );
                                  $fields = array
                                      (
                                        'to'    => $get_user_token['firebase_token'],
                                        'notification'  => $msg
                                      );
                                  
                                                   
            
                                  $headers = array
                                      (
                                        'Authorization: key=' . API_ACCESS_KEY12,
                                        'Content-Type: application/json'
                                      );
                                #Send Reponse To FireBase Server  
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    //echo $result;
                                    curl_close( $ch );
                             
                    
                                    $save=new Notification;
                                    $save->title=$title;
                                    $save->body=$body;
                                    $save->title_ar = $title_ar;
                                    $save->body_ar = $body_ar;
                                    $save->sender= 1;
                                    $save->is_read=0;
                                     $save->request_id= 0;
                                    $save->user_id=$get_user_token->id;
                                    $save->created_at=$dateTime;
                                    $save->save();
                      
                        
                        
                        $message['error']=0;
                        $message['message']='notification sent ';
                         
                        
                        
                                         }catch(Exception $ex){
                                              $message['error']=4;
                                               $message['message']='error in send notification';
                                              
                                         }
             
             
             
             
             
             
             
             
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }
     
      public function  delete_driver(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
            $id=$request->input('user_id');
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     
                $delete=User::where('id',$id)->delete();

                  

          

                      if($delete ==true){
                           
                             $message['error']=0;
                            $message['message']='delete agent successfully';
                        }else{
                           
                             $message['error']=1;
                             $message['message']='error in delete agent';
                        }
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }
     
     //rating
   public function show_usersRate(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=DB::select('select users.id,users.first_name,users.phone,users.rate,(select count(requestes.id) from requestes where requestes.user_id=users.id AND state=4)as count,(select count(rating.id) from rating where user_id=users.id AND who_rate="driver")as rating_count from users  where  users.state=3');

          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
   
    public function show_driversRate(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=DB::select('select users.id,users.first_name,users.phone,users.state,(select IF(users.state=2,(SELECT  sum(rating.rate)/count(rating.user_id) 
                             from rating
 
                   where  rating.driver_id=users.id
                   AND who_rate="user"   ),(SELECT  sum(rating.rate)/count(rating.user_id) 
                             from rating
 
                   where  rating.user_id=users.id
                   AND who_rate="driver" )))as rate,(select count(requestes.id) from offers join requestes ON offers.request_id=requestes.id where offers.driver_id=users.id AND offers.state="accepted" AND requestes.state=4 )as count,(select count(rating.id) from rating where driver_id=users.id AND who_rate="user")as rating_count from users  where  users.state=2');


    
          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
     
     
     
      public function search_usersRate(Request $request)
   {
     try{
       $token=$request->input('user_token');
         $user=$request->input('user_id');

          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              
              $check_user=User::where('id',$user)->orwhere('phone',$user)->first();
              
              if($check_user['state']==2){

               $select=DB::select('select users.id,users.first_name,users.phone,users.rate,(select count(requestes.id) from offers join requestes ON offers.request_id=requestes.id where offers.driver_id=users.id AND offers.state="accepted" AND requestes.state=4 )as count from users  where  users.state=2 AND ( users.phone='.$user.' OR users.id='.$user.')');

              }else{
                  
          $select=DB::select('select users.id,users.first_name,users.phone,users.rate,(select count(requestes.id) from requestes where requestes.user_id=users.id AND state=4)as count from users  where users.state !=2 AND ( users.phone='.$user.' OR users.id='.$user.')');

              }
          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
   
   
}

