<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use carbon\carbon;
use App\compliance_image;
use App\complain_question;
use App\Compliance;
use App\City;
use App\Notification;
use App\Wallet;
use App\setting;
use App\deriver_details;



class AdminController extends Controller
{
    public $message=array();
    
    
      public function add_user(Request $request)
    {
       try{
           $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              
              $fname=$request->input('first_name');
              $lname=$request->input('last_name');
         
	          $email=$request->input('email');
	          $phone=$request->input('phone');
	          $image=$request->file('image');
	          $pass=$request->input('passwords');
              $gender=$request->input('gender_id');
       

        
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));


           if(isset($image)) {
                        $new_name = $image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/users';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
               }else{
                  $images = NULL;       
              }
     
              
                //verification
  
             
              if($request->has('email')){
                  $emailexist=User::where('email',$email)->get();
              }else{
                  
                 $emailexist=array(); 
              }
              
              
               if($request->has('phone') && $phone !='' && !empty($phone)){
                   
                  $phoneexist=User::select('id','phone')->where('phone',$phone)->get();
               }else{
                  
                 $phoneexist=array(); 
              }
           if(count($phoneexist)> 0){
                  $message['error']=4;
                   $message['message']='phone  exist';
           }elseif(count($emailexist) >0){
                  $message['error']=5;
                   $message['message']='email  exist';
           }else{
                 
               
            

          $insert=User::insertGetId([
              'first_name'=>$fname,
              'last_name'=>$lname,
              'email'=>$email,
              'phone'=>$phone,
              'password'=>$pass,
              'image'=>$images,
              'gender'=>$gender,
              
              'state'=>3,
              'registy_state'=>1,
              
              
              'created_at'=>$dateTime]);
          
            

                
      
                if($insert ==true){

                	$set=new setting;
                	$set->user_id=$insert;
                	$set->notification="ON";
                     $set->language=1;
                	$set->created_at=$dateTime;
                	$set->save();

                  $wal=new Wallet;
                  $wal->user_id=$insert;
                  $wal->credit=0;
                  
                  $wal->created_at=$dateTime;
                  $wal->save();
                  
                      $message['error']=0;
                       $message['message']='add user success';
                }else{
                      $message['error']=1;
                       $message['message']='error in save';
                }
              

            }
          }else{
         $message['error']=3;
         $message['message']='this token is not exit';
         }
            }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
            return response()->json($message);
    }

     public function update_user(Request $request)
    {
       try{
           
             $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              $id=$request->input('user_id');
              
              
              $fname=$request->input('first_name');
              $lname=$request->input('last_name');
         
	          $email=$request->input('email');
	          $phone=$request->input('phone');
	          $image=$request->file('image');
	          $pass=$request->input('passwords');
            $gender=$request->input('gender_id');
         
          
        
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));


           if(isset($image)) {
                        $new_name = $image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/users';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
               }else{
                  $images = NULL;       
              }
     
              

             
              if($request->has('email')){
                  $emailexist=User::select('id','email')->where([['email',$email],['id','!=',$id]])->get();
              }else{
                  
                 $emailexist=array(); 
              }
              
              
               if($request->has('phone') && $phone !='' && !empty($phone)){
                   
                  $phoneexist=User::select('id','phone')->where([['phone',$phone],['id','!=',$id]])->get();
               }else{
                  
                 $phoneexist=array(); 
              }
           if(count($phoneexist)> 0){
                  $message['data']=$phoneexist;
                  $message['error']=4;
                   $message['message']='phone  exist';
           }elseif(count($emailexist) >0){
                  $message['error']=5;
                   $message['message']='email  exist';
           }else{
                 
               
            

          $update=User::where('id',$id)->update([
              'first_name'=>$fname,
              'last_name'=>$lname,
              'email'=>$email,
              'phone'=>$phone,
              'password'=>$pass,
              'image'=>$images,
              'gender'=>$gender,
             
              'updated_at'=>$dateTime]);
          
                  
           

                
      
                if($update ==true){

                
                  
                      $message['error']=0;
                       $message['message']='update success';
                }else{
                      $message['error']=1;
                       $message['message']='error in save';
                }
              

            }
          }else{
         $message['error']=3;
         $message['message']='this token is not exit';
         }
            }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
            return response()->json($message);
    }


   public function show_userbyid(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              
              $id=$request->input('id');
               
            $select=User::select('users.id','users.first_name','users.last_name','users.phone','users.email','users.image','roles.role as state','users.password as passwords','users.gender as gender_id','gender.name as gender')
            ->join('roles','users.state','=','roles.id') 
            ->leftjoin('gender','users.gender','=','gender.id')         
             ->where('users.id',$id)->first();
        
        
                
          if($select !=null){

            $message['data']=$select;
           
         
            $message['error']=0;
            $message['message']='user data';

          }else{
            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data for user ';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }



   public function add_driver(Request $request)
     {
       try{
              $token=$request->input('user_token');
              
              
              $fname=$request->input('first_name');
              $lname=$request->input('last_name');
              $email=NULL;
	            $email=$request->input('email');
	            $phone=$request->input('phone');
	            $image=$request->file('image');
	            $pass=$request->input('passwords');
              $gender=$request->input('gender_id');

              $card_number=$request->input('card_number');
  
              $card_image=$request->file('card_image');
              $driving_license_image=$request->file('driving_license_image');
              $car_license_image=$request->file('car_license_image');
              $front_vehical_image=$request->file('front_vehical_image');
      



          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

        
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));


           if(isset($image)) {
                        $new_name = $image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/users';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
               }else{
                  $images = NULL;       
              }

               if(isset($card_image)) {
                        $new_name = $card_image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/cards';
                        $card_image->move($destinationPath_id, $savedFileName);
            
                        $card_images = $savedFileName;
                      
               }else{
                  $card_images = NULL;       
              }
     
          if(isset($driving_license_image)) {
                        $new_name = $driving_license_image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/license';
                        $driving_license_image->move($destinationPath_id, $savedFileName);
            
                        $driving_license_images = $savedFileName;
                      
               }else{
                  $driving_license_images = NULL;       
              }
     
          if(isset($car_license_image)) {
                            $new_name = $car_license_image->getClientOriginalName();
                            $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads/license';
                            $car_license_image->move($destinationPath_id, $savedFileName);
                
                            $car_license_images = $savedFileName;
                          
                   }else{
                      $car_license_images = NULL;       
                  }
         
          if(isset($front_vehical_image)) {
                            $new_name = $front_vehical_image->getClientOriginalName();
                            $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads/vehicels';
                            $front_vehical_image->move($destinationPath_id, $savedFileName);
                
                            $front_vehical_images = $savedFileName;
                          
                   }else{
                      $front_vehical_images = NULL;       
                  }
         
        
     


            
              if($request->has('phone')){
             $phoneexist=User::where('phone',$phone)->get();
              }else{
                  $phoneexist=array();
              }
           if(count($phoneexist) >0){
                  $message['error']=4;
                   $message['message']='phone  exist';
           }else{
                 

              
                   
                     
                $user=User::insertGetId(['first_name'=>$fname,'last_name'=>$lname,'email'=>$email,'phone'=>$phone,'password'=>$pass,'state'=>2,
                'image'=>$images,'gender'=>$gender,'created_at'=>$dateTime,'registy_state'=>1,'is_driver'=>1]); 
              	
              

                
      
                if($user >0){

                 $insert=new deriver_details;
                 $insert->driver_id=$user;
                 $insert->card_number=$card_number;
                 $insert->card_image=$card_images;
                 $insert->driving_license_image=$driving_license_images;
                 $insert->car_license_image=$car_license_images;
                 $insert->front_vehical_image=$front_vehical_images;
                 $insert->created_at=$dateTime; 
                 $insert->save();



                	$set=new setting;
                	$set->user_id=$user;
                	$set->notification="ON";
                  $set->language=1;
                	$set->created_at=$dateTime;
                	$set->save();

                  $wal=new Wallet;
                  $wal->user_id=$user;
                  $wal->credit=0;
                  $wal->created_at=$dateTime;
                  $wal->save();

             
                  
                      $message['error']=0;
                       $message['message']='add driver success';
                }else{
                      $message['error']=1;
                       $message['message']='error in save';
                }
              

            }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

            }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
            return response()->json($message);
     }


    public function update_driver(Request $request)
     {
       try{
              $token=$request->input('user_token');
              
            $id=$request->input('id');
              $fname=$request->input('first_name');
              $lname=$request->input('last_name');
              $email=NULL;
	            $email=$request->input('email');
	            $phone=$request->input('phone');
	            $image=$request->file('image');
	            $pass=$request->input('passwords');
              $gender=$request->input('gender_id');

              $card_number=$request->input('card_number');
  
              $card_image=$request->file('card_image');
              $driving_license_image=$request->file('driving_license_image');
              $car_license_image=$request->file('car_license_image');
              $front_vehical_image=$request->file('front_vehical_image');
      



          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

        
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));


           if(isset($image)) {
                        $new_name = $image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/users';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
               }else{
                  $images = User::where('id',$id)->value('image');       
              }

               if(isset($card_image)) {
                        $new_name = $card_image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/cards';
                        $card_image->move($destinationPath_id, $savedFileName);
            
                        $card_images = $savedFileName;
                      
               }else{
                  $card_images = deriver_details::where('driver_id',$id)->value('card_image');       
              }
     
          if(isset($driving_license_image)) {
                        $new_name = $driving_license_image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/license';
                        $driving_license_image->move($destinationPath_id, $savedFileName);
            
                        $driving_license_images = $savedFileName;
                      
               }else{
                  $driving_license_images = deriver_details::where('driver_id',$id)->value('driving_license_image');   ;       
              }
     
          if(isset($car_license_image)) {
                            $new_name = $car_license_image->getClientOriginalName();
                            $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads/license';
                            $car_license_image->move($destinationPath_id, $savedFileName);
                
                            $car_license_images = $savedFileName;
                          
                   }else{
                      $car_license_images =deriver_details::where('driver_id',$id)->value('car_license_image');        
                  }
         
          if(isset($front_vehical_image)) {
                            $new_name = $front_vehical_image->getClientOriginalName();
                            $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads/vehicels';
                            $front_vehical_image->move($destinationPath_id, $savedFileName);
                
                            $front_vehical_images = $savedFileName;
                          
                   }else{
                      $front_vehical_images =deriver_details::where('driver_id',$id)->value('front_vehical_image');       
                  }
         
        
     


            
              if($request->has('phone')){
             $phoneexist=User::where([['phone',$phone],['id','!=',$id]])->get();
              }else{
                  $phoneexist=array();
              }
           if(count($phoneexist) >0){
                  $message['error']=4;
                   $message['message']='phone  exist';
           }else{
                 

              
                   
                     
                $update=User::where('id',$id)->update(['first_name'=>$fname,'last_name'=>$lname,'email'=>$email,'phone'=>$phone,'password'=>$pass,
                'image'=>$images,'gender'=>$gender,'updated_at'=>$dateTime]); 
              	
              

                
      
                if($update ==true){

                 $insert=deriver_details::where('driver_id',$id)->update([
                     'card_number'=>$card_number,
                     'card_image'=>$card_images,
                     'driving_license_image'=>$driving_license_images,
                     'car_license_image'=>$car_license_images,
                     'front_vehical_image'=>$front_vehical_images,
                     'updated_at'=>$dateTime
                     
                     
                     ]);
              

                      $message['error']=0;
                       $message['message']='update driver success';
                }else{
                      $message['error']=1;
                       $message['message']='error in save';
                }
              

            }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

            }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
            return response()->json($message);
     }
     
     

        public function admin_login(Request $request)
     {
     try{
         $email=$request->input('email');
         $pass=$request->input('passwords');

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
         
        
              $userexist=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwods','users.phone','users.state','users.image','users.user_token','users.created_at','users.updated_at')
              ->where([['users.email',$email],['users.password',$pass],['users.state','=',1]])->first();
              
              
              
                try{
                    
		                  
		                    $user_token=sha1($userexist['id']).rand(1000000,9999999).time();
		                    //rand(1000000,9999999).time();// give a unique name to file to be saved
		                    
		                    $insert=User::where([['users.email',$email],['users.password',$pass],['users.state','=',1]])->update([
		                        'user_token'=>$user_token,
		                        'updated_at'=>$dateTime
		                    ]);
		                    
		           }catch(Exception $ex){
		                $message['error']=4;
                   $message['message']='error in generate key';    
		          }
              
              
              
               $userexist=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwords','users.phone','roles.role as state','users.image','users.user_token','users.created_at','users.updated_at')
                ->join('roles','users.state','=','roles.id')
              ->where([['users.email',$email],['users.password',$pass],['users.state','=',1]])->first();
              

              if($userexist !=null){
               
                   $message['data']=$userexist;
                   $message['error']=0;
                   $message['message']='login success';

             }else{
                    $message['data']=$userexist;
                    $message['error']=1;
                     $message['message']='data is wrong';
             }
            
            }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
            }
      
            return response()->json($message);
     }

  public function show_allagents(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwods','users.phone','roles.role as state','users.image','gender.name','users.created_at')
              ->join('roles','users.state','=','roles.id')
              ->join('gender','users.gender','=','gender.id')
               ->where('users.state',4)->get();

          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }



  public function show_pendingAgents(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwods','users.phone','roles.role as state','users.image','government.name','city.name','gender.name','users.created_at')
              ->join('roles','users.state','=','roles.id')
              ->join('government','users.government_id','=','government.id')
              ->join('city','users.city_id','=','city.id')
              ->join('gender','users.gender','=','gender.id')
               ->where('users.state',7)->get();

          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }


     public function  accept_agent(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
            $id=$request->input('user_id');
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     
                $userupdate=User::where('id',$id)
                  ->update(
                   ['state'=>4,
                    'updated_at'=>$dateTime]);

                  

          

                      if($userupdate==true){
                           
                             $message['error']=0;
                            $message['message']='accept agent successfully';
                        }else{
                           
                             $message['error']=1;
                             $message['message']='error in accept agent';
                        }
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }

    public function  block_agent(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
            $id=$request->input('user_id');
            $reason=$request->input('reason');
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
              
              $check=User::where('id',$id)->value('state');
              
              
              if($check ==2 || $check ==5){
     
                $userupdate=User::where('id',$id)
                  ->update(
                   ['state'=>6,
                   'block_reason'=>$reason,
                    'updated_at'=>$dateTime]);
                  
              }else{
                  $userupdate=User::where('id',$id)
                  ->update(
                   ['state'=>9,
                   'block_reason'=>$reason,
                    'updated_at'=>$dateTime]); 
                  
              }

                  

          

                      if($userupdate==true){
                           
                             $message['error']=0;
                            $message['message']='block  successfully';
                        }else{
                           
                             $message['error']=1;
                             $message['message']='error in block ';
                        }
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }


     public function  unblock_agent(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
            $id=$request->input('user_id');
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     
                              $check=User::where('id',$id)->value('state');
              
              
              if($check ==6 || $check ==5){
     
                $userupdate=User::where('id',$id)
                  ->update(
                   ['state'=>2,
                    'updated_at'=>$dateTime]);
                  
              }else{
                  $userupdate=User::where('id',$id)
                  ->update(
                   ['state'=>3,
                    'updated_at'=>$dateTime]); 
                  
              }

                  

          

                      if($userupdate==true){
                           
                             $message['error']=0;
                            $message['message']='block agent successfully';
                        }else{
                           
                             $message['error']=1;
                             $message['message']='error in block agent';
                        }
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }





   public function show_alldrivers(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=User::select('users.id','users.first_name','users.last_name','users.email','gender.name as gender','users.password as passwods','users.phone','roles.role as state','users.image','users.rate','users.created_at')
             
              ->join('roles','users.state','=','roles.id')
               ->leftJoin('gender','users.gender','=','gender.id')
               ->where([['users.state',2],['type','=',NULL]])->get();

          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }



  public function show_pendingdrivers(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=User::select('users.id','users.first_name','users.last_name','users.phone','users.image','gender.name as gender','users.email','users.created_at')->distinct()
            ->leftjoin('driver_vehicles_details','users.id','=','driver_vehicles_details.driver_id')
             ->join('roles','users.state','=','roles.id')
              ->leftjoin('gender','users.gender','=','gender.id')
          
               ->where('users.state',5)->get();

          
       if(count($select) > 0){
           
           $count=User::where('users.state',5)->count();
           
            $message['data']=$select;
            $message['count']=$count;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
             $message['count']=0;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
   
   
   
  public function show_blockeddrivers(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=User::select('users.id','users.first_name','users.last_name','users.email','gender.name as gender','users.password as passwods','users.phone','roles.role as state','users.image','users.rate','users.block_reason','users.created_at')
              ->join('roles','users.state','=','roles.id')
               ->leftjoin('gender','users.gender','=','gender.id')
               ->where('users.state',6)->get();

          
       if(count($select) > 0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }




  public function  accept_driver(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
            $id=$request->input('user_id');
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     
                $userupdate=User::where('id',$id)
                  ->update(
                   ['state'=>2,
                    'updated_at'=>$dateTime]);

                  

          

                      if($userupdate==true){
                          
                          
                             try{       
                   //  $cc = 0;
                     
                    $title = "Notification from admin";
                    $body = "Our dear customer, we have reviewed your account and your account has been successfully activated";
                    $title_ar = 'اشعار من الادمن  ';
                    $body_ar = ' عميلنا العزيز قمنا بمراجعه حسابك وتم تفعيل حسابك بنجاح';
                 define( 'API_ACCESS_KEY12','AAAAnSDEgLc:APA91bFisJ6mf6QmnpUvaC48ND3u4u_ULaQNnR4fxCRur392hErem3qi3fjRQ05wXg45D8NrhehK4Bp8h1f_uUGW0YbSQBmxDRWg3pi9PBkHvYnE8GHqjYs3JRTmCab08jUJQph9luxS');
            
            
                   $get_user_token =User::select('firebase_token')->where('id', '=',$id)->first();
                   
                      // return $get_user_token[0]->firebase_token;
                      
                      
            
            
                                 ////////////////////////////////////////////////////////
                                 
                                   $msg = array
                                          (
                                    'body'  => $body,
                                    'title' => $title,
                                              
                                          );
                                  $fields = array
                                      (
                                        'to'    => $get_user_token['firebase_token'],
                                        'notification'  => $msg
                                      );
                                  
                                                   
            
                                  $headers = array
                                      (
                                        'Authorization: key=' . API_ACCESS_KEY12,
                                        'Content-Type: application/json'
                                      );
                                #Send Reponse To FireBase Server  
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    //echo $result;
                                    curl_close( $ch );
                             
                                $save=new Notification;
                                $save->title=$title;
                                $save->body=$body;
                                $save->title_ar = $title_ar;
                                $save->body_ar = $body_ar;
                                $save->sender=1;
                                $save->is_read=0;
                                $save->request_id=0;
                                $save->user_id=$id;
                                $save->created_at=$dateTime;
                                $save->save();
                                         }catch(Exception $ex){
                                              $message['error']=4;
                                               $message['message']='error in send notification';
                                              
                                         }
                           
                             $message['error']=0;
                            $message['message']='accept agent successfully';
                        }else{
                           
                             $message['error']=1;
                             $message['message']='error in accept agent';
                        }
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }
     
     public function  close_driveraccount(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
            $id=$request->input('user_id');
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     
                $userupdate=User::where('id',$id)
                  ->update(
                   ['state'=>10,
                    'updated_at'=>$dateTime]);

                  

          

                      if($userupdate==true){
                           
                             $message['error']=0;
                            $message['message']='closed agent account successfully';
                        }else{
                           
                             $message['error']=1;
                             $message['message']='error in closed agent';
                        }
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }
     
     
  public function  block_driver(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
            $id=$request->input('user_id');
            $reason=$request->input('reason');
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     
                $userupdate=User::where('id',$id)
                  ->update(
                   ['state'=>6,
                   'block_reason'=>$reason,
                    'updated_at'=>$dateTime]);

                  

          

                      if($userupdate==true){
                           
                             $message['error']=0;
                            $message['message']='block agent successfully';
                        }else{
                           
                             $message['error']=1;
                             $message['message']='error in accept agent';
                        }
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }

  


  public function edit_adminprofile(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
		       
		        $fname=$request->input('first_name');
		         $lname=$request->input('last_name');
		        $phone=$request->input('phone');
		         $image=$request->file('image');
		        $email=$request->input('email');
		        $pass=$request->input('passwords');
		       
		      

		       $updated_at = carbon::now()->toDateTimeString();
		      $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
		             if(isset($image)) {
		                    $new_name = $image->getClientOriginalName();
		                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
		                    $destinationPath_id = 'uploads/users';
		                    $image->move($destinationPath_id, $savedFileName);
		        
		                    $images = $savedFileName;
		                  
		           }else{
		              $images = User::where('id',$check_token['id'])->value('image');       
		          }
		        
		 
		            $userupdate=User::where('user_token',$token)
		              ->update(
		               ['first_name'=>$fname,
		               'last_name'=>$lname,
		                'email'=>$email,
		                 'image'=>$images,
		                'phone'=>$phone,
		                'password'=>$pass,
		                'updated_at'=>$dateTime]);

		              

		        $user_data=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwods','users.phone','users.state','users.image','users.user_token','users.created_at','users.updated_at')

                  ->where('user_token','=',$token)->first();

		                  if($userupdate==true){
		                        $message['data']=$user_data;
		                         $message['error']=0;
		                        $message['message']='update data successfully';
		                    }else{
		                         $message['data']=$user_data;
		                         $message['error']=1;
		                         $message['message']='error in update data';
		                    }
	          }else{
	               $message['error']=3;
			       $message['message']='this token is not exist';
	          }

			            }catch(Exception $ex){
			                 $message['error']=2;
			                 $message['message']='error'.$ex->getMessage();
			            }  

			         return response()->json($message);
     }



  public function show_adminById(Request $request)
    {
       try{
           $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
        
        

         $user_data=User::select('users.id','users.first_name','users.last_name','users.email','users.password as passwords','users.phone','roles.role as state','users.image','users.user_token','users.created_at','users.updated_at')
             ->join('roles','users.state','=','roles.id')
            ->where('users.user_token','=',$token)->first();
          

                  if($user_data !=null){
        
                    $message['data']=$user_data;
                    $message['error']=0;
                    $message['message']='user data';
        
                  }else{
                    $message['data']=$user_data;
                    $message['error']=1;
                    $message['message']='no data for user ';
                  }
          }else{
              $message['error']=3;
            $message['message']='this token is\'t exist';
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }



   public function show_driverbyid(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
          $id=$request->input('user_id');
         
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

          
         


            
                  
                 $select=User::select('users.id','users.first_name','users.last_name','users.phone','users.image','users.email','users.gender as gender_id','gender.name as gender' ,'driver_vehicles_details.card_number',
                   'driver_vehicles_details.card_image','driver_vehicles_details.driving_license_image', 'driver_vehicles_details.car_license_image', 'driver_vehicles_details.front_vehical_image','users.created_at')
                       ->leftjoin('driver_vehicles_details','users.id','=','driver_vehicles_details.driver_id')

                        ->leftjoin('gender','users.gender','=','gender.id')
                          ->where('users.id',$id)->first();

               
        
        
                
          if($select !=null){

            $message['data']=$select;
          
           
         
            $message['error']=0;
            $message['message']='user data';

          }else{
            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data for user ';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

 

  //compliance

   public function show_allcompliance(Request $request)
   {
     try{
       $token=$request->input('user_token');
        //$lang=$request->input('lang');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


      // $check_lang=setting::where('user_id',$check_token['id'])->value('language');

       //1 =>arabic     2=>english
                      

       $show=Compliance::select('compliance.id as complain_id','client.first_name','client.last_name','client.image','driver.first_name as driver_name','driver.image as driver_image','shopes.name as shope_name','compliance.order_id','compliance.content','compliance.details','compliance.state','compliance.created_at')
                    ->join('users as client','compliance.user_id','=','client.id')
                    ->join('users as driver','compliance.driver_id','=','driver.id')
                    ->join('requestes','compliance.order_id','=','requestes.id')
                    ->join('shopes','requestes.shop_id','=','shopes.id')
                  //  ->join('complain_answer','compliance.id','=','complain_answer.complain_id')

                   // ->join('complain_questions','complain_answer.question_id','=','complain_questions.id')
                     ->where([['compliance.state','wait'],['who_complain','user']])->get();

       if(count($show) > 0){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }

  public function show_oldcompliance(Request $request)
   {
     try{
       $token=$request->input('user_token');
        //$lang=$request->input('lang');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


   
                      

      $show=Compliance::select('compliance.id as complain_id','client.first_name','client.last_name','client.image','driver.first_name as driver_name','driver.image as driver_image','shopes.name as shope_name','compliance.order_id','compliance.content','compliance.details','compliance.state','compliance.created_at')
                    ->join('users as client','compliance.user_id','=','client.id')
                    ->join('users as driver','compliance.driver_id','=','driver.id')
                    ->join('requestes','compliance.order_id','=','requestes.id')
                    ->join('shopes','requestes.shop_id','=','shopes.id')
                  //  ->join('complain_answer','compliance.id','=','complain_answer.complain_id')

                   // ->join('complain_questions','complain_answer.question_id','=','complain_questions.id')
                     ->where([['compliance.state','!=','wait'],['who_complain','user']])->get();
    
                    

       if(count($show) > 0){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }


   public function show_allDrivercompliance(Request $request)
   {
     try{
       $token=$request->input('user_token');
        //$lang=$request->input('lang');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


      // $check_lang=setting::where('user_id',$check_token['id'])->value('language');

       //1 =>arabic     2=>english
                      

       $show=Compliance::select('compliance.id as complain_id','client.first_name','client.last_name','client.image','driver.first_name as driver_name','driver.image as driver_image','shopes.name as shope_name','compliance.order_id','compliance.content','compliance.details','compliance.state','compliance.created_at')
                    ->join('users as client','compliance.user_id','=','client.id')
                    ->join('users as driver','compliance.driver_id','=','driver.id')
                    ->join('requestes','compliance.order_id','=','requestes.id')
                    ->join('shopes','requestes.shop_id','=','shopes.id')
                  //  ->join('complain_answer','compliance.id','=','complain_answer.complain_id')

                   // ->join('complain_questions','complain_answer.question_id','=','complain_questions.id')
                     ->where([['compliance.state','wait'],['who_complain','driver']])->get();

       if(count($show) > 0){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }

  public function show_oldDrivercompliance(Request $request)
   {
     try{
       $token=$request->input('user_token');
        //$lang=$request->input('lang');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


   
                      

      $show=Compliance::select('compliance.id as complain_id','client.first_name','client.last_name','client.image','driver.first_name as driver_name','driver.image as driver_image','shopes.name as shope_name','compliance.order_id','compliance.content','compliance.details','compliance.state','compliance.created_at')
                    ->join('users as client','compliance.user_id','=','client.id')
                    ->join('users as driver','compliance.driver_id','=','driver.id')
                    ->join('requestes','compliance.order_id','=','requestes.id')
                    ->join('shopes','requestes.shop_id','=','shopes.id')
                  //  ->join('complain_answer','compliance.id','=','complain_answer.complain_id')

                   // ->join('complain_questions','complain_answer.question_id','=','complain_questions.id')
                     ->where([['compliance.state','!=','wait'],['who_complain','driver']])->get();
         
                    

       if(count($show) > 0){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }








 public function show_onecompliance(Request $request)
   {
     try{
       $token=$request->input('user_token');
       $id=$request->input('id');   
          
       $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

    
                    $show=Compliance::select('compliance.id','users.id','users.first_name','users.last_name','users.image','shopes.name as shope_name','compliance.order_id','compliance.content','compliance.details','compliance.created_at')
                    ->join('users','compliance.user_id','=','users.id')
                    ->join('requestes','compliance.order_id','=','requestes.id')
                    ->join('shopes','requestes.shop_id','=','shopes.id')
                  //  ->join('complain_answer','compliance.id','=','complain_answer.complain_id')

                   // ->join('complain_questions','complain_answer.question_id','=','complain_questions.id')
                     ->where('compliance.id',$id)->first();
        
           if($show !=null){
               $images=compliance_image::where('complain_id',$id)->get();
                    $question_answer=complain_question::select('complain_questions.name','complain_answer.answer')->join('complain_answer','complain_questions.id','=','complain_answer.question_id')->where('complain_answer.complain_id',$id)->get();
                $message['data']=$show;
                 $message['answers']=$question_answer;
                $message['images']=$images;
                $message['error']=0;
                $message['message']='show data';
           }else{

                $message['data']=$show;
                $message['error']=1;
                $message['message']='no data';
           }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
   
   
     public function  accept_complaince(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
            $id=$request->input('id');
            $credit=$request->input('credit');
            
            $comment=$request->input('comment');
            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     
                $update=Compliance::where('id',$id)
                  ->update(
                   [   'answer'=>$comment,
                       'state'=>'accepted',
                    'updated_at'=>$dateTime]);

                  
               $complain_data=Compliance::where('id',$id)->first();
               
               if($complain_data['who_complain']=='user'){
                   
                   $user=Compliance::where('id',$id)->value('user_id');
               }else{
                   
                   $user=Compliance::where('id',$id)->value('driver_id');
               }
          

                      if($update==true){
                           
                             $message['error']=0;
                            $message['message']='accept complaince successfully';
                            
                             if($request->has('credit')){
                                 
                                 $credit=$request->credit;
                             }else{
                                 $credit=0;
                             }
                            
                            $increse=Wallet::where('user_id',$user)->increment('credit',$credit);
                            
                            
                            
                                     try{       
                   //  $cc = 0;
                     
                    $title ='your complaince is accepted ';
                    $body ='your complaince is accepted '.$comment;
                    $title_ar =  "تم قبول شكواك";
                    $body_ar = $comment." تم قبول شكواك ";
                    
                 define( 'API_ACCESS_KEY12','AAAAnSDEgLc:APA91bFisJ6mf6QmnpUvaC48ND3u4u_ULaQNnR4fxCRur392hErem3qi3fjRQ05wXg45D8NrhehK4Bp8h1f_uUGW0YbSQBmxDRWg3pi9PBkHvYnE8GHqjYs3JRTmCab08jUJQph9luxS');
            
            
                   $get_user_token =User::select('firebase_token')->where('id', '=',$user)->first();
                   
                      // return $get_user_token[0]->firebase_token;
            
            
                                 ////////////////////////////////////////////////////////
                                 
                                   $msg = array
                                          (
                                    'body'  => $body,
                                    'title' => $title,
                                              
                                          );
                                  $fields = array
                                      (
                                        'to'    => $get_user_token['firebase_token'],
                                        'notification'  => $msg
                                      );
                                  
                                                   
            
                                  $headers = array
                                      (
                                        'Authorization: key=' . API_ACCESS_KEY12,
                                        'Content-Type: application/json'
                                      );
                                #Send Reponse To FireBase Server  
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    //echo $result;
                                    curl_close( $ch );
                             
                    
                    
                        $save=new Notification;
                        $save->title=$title;
                        $save->body=$body;
                        $save->title_ar = $title_ar;
                        $save->body_ar = $body_ar;
                        $save->sender=1;
                        $save->is_read=0;
                        $save->request_id=$complain_data['order_id'];
                        $save->user_id=$user;
                        $save->created_at=$dateTime;
                        $save->save();
                        
                        
                     }catch(Exception $ex){
                          $message['error']=4;
                           $message['message']='error in send notification';
                          
                     }
            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        }else{
                           
                             $message['error']=1;
                             $message['message']='error in accept complaince';
                        }
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }
      public function  refuse_complaince(Request $request)
     {
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
           
            $id=$request->input('id');
                      $comment=$request->input('comment');

            
           
          

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
     
                $update=Compliance::where('id',$id)
                  ->update(
                   [   'answer'=>$comment,
                       'state'=>'refused',
                    'updated_at'=>$dateTime]);

                  

          

                      if($update==true){
                           
                             $message['error']=0;
                            $message['message']='refuse complaince successfully';
                            
                            
               $complain_data=Compliance::where('id',$id)->first();
               
               if($complain_data['who_complain']=='user'){
                   
                   $user=Compliance::where('id',$id)->value('user_id');
               }else{
                   
                   $user=Compliance::where('id',$id)->value('driver_id');
               }
                            
                            
                            
                                     try{       
                   //  $cc = 0;
                     
                    $title ='your complaince is refused ';
                    $body ='your complaince is refused ';
                    $title_ar =  "تم رفض شكواك";
                    $body_ar = "تم رفض شكواك";
                 define( 'API_ACCESS_KEY12','AAAAnSDEgLc:APA91bFisJ6mf6QmnpUvaC48ND3u4u_ULaQNnR4fxCRur392hErem3qi3fjRQ05wXg45D8NrhehK4Bp8h1f_uUGW0YbSQBmxDRWg3pi9PBkHvYnE8GHqjYs3JRTmCab08jUJQph9luxS');
            
            
                   $get_user_token =User::select('firebase_token')->where('id', '=',$user)->first();
                   
                      // return $get_user_token[0]->firebase_token;
            
            
                                 ////////////////////////////////////////////////////////
                                 
                                   $msg = array
                                          (
                                    'body'  => $body,
                                    'title' => $title,
                                              
                                          );
                                  $fields = array
                                      (
                                        'to'    => $get_user_token['firebase_token'],
                                        'notification'  => $msg
                                      );
                                  
                                                   
            
                                  $headers = array
                                      (
                                        'Authorization: key=' . API_ACCESS_KEY12,
                                        'Content-Type: application/json'
                                      );
                                #Send Reponse To FireBase Server  
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    //echo $result;
                                    curl_close( $ch );
                             
                    
                    
                        $save=new Notification;
                        $save->title=$title;
                        $save->body=$body;
                        $save->title_ar = $title_ar;
                        $save->body_ar = $body_ar;
                        $save->sender=1;
                        $save->is_read=0;
                         $save->request_id=$complain_data['order_id'];
                        $save->user_id=$user;
                        $save->created_at=$dateTime;
                        $save->save();
                        
                        
                     }catch(Exception $ex){
                          $message['error']=4;
                           $message['message']='error in send notification';
                          
                     }
                        }else{
                           
                             $message['error']=1;
                             $message['message']='error in refuse complaince';
                        }
            }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }

                  }catch(Exception $ex){
                       $message['error']=2;
                       $message['message']='error'.$ex->getMessage();
                  }  

               return response()->json($message);
     }
}
