<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Requests;
use App\Wallet;
use App\Messages;
use App\Offers;
use App\User;
use App\Shopes;
use App\Menu;
use App\Product_type;
use App\Bill;
use App\Notification;
use App\Rating;
use App\Copon;
use carbon\carbon;
use App\request_details;
use App\About;
use App\setting;
      
define( 'API_ACCESS_KEY12','AAAAnSDEgLc:APA91bFisJ6mf6QmnpUvaC48ND3u4u_ULaQNnR4fxCRur392hErem3qi3fjRQ05wXg45D8NrhehK4Bp8h1f_uUGW0YbSQBmxDRWg3pi9PBkHvYnE8GHqjYs3JRTmCab08jUJQph9luxS');

class OrdersController extends Controller
{
    public $message=array();

   public function make_order(Request $request)
    {
       try{
        
          $token=$request->input('user_token');    
          
        
          
       
          $check_token=User::where('user_token',$token)->first();
          
          if($request->has('user_token') && $check_token !=NULL){
              
              $shope=$request->input('shope_id');
          	  $receive=$request->input('receive_place');
          	  $receiving_longitude=$request->input('receiving_longitude');
          	  $receiving_latitude=$request->input('receiving_latitude');

          	  $delivery=$request->input('delivery_place');
              $delivery_longitude=$request->input('delivery_longitude');
              $delivery_latitude=$request->input('delivery_latitude');

          	  $time=$request->input('delivery_time');
          	  $description=$request->input('description');  
              $total_price=$request->input('total_price');
              //details
              $menu_items= json_decode($request->input('menu_items'));
              $image=$request->file('image');
              $myimage=$request->file('image');
              $payment=$request->input('payment_id');
          	  $copon=$request->input('copon');
          	    $state=1;



          	  $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at))); 	 

       $check_offers=Requests::where([['user_id',$check_token['id']],['state',1]])->first();
       
      if($check_offers !=null){
             $message['error']=4;
            $message['message']='sorry you can\'t make an order';
      }else{
           
           
       
             if(isset($image)) {
                  $new_name = $image->getClientOriginalName();
                  $savedFileName = rand(100000,999999).time()."_".$new_name; 
                    // give a unique name to file to    be saved
                        $destinationPath_id = 'uploads/orders';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
                 }else{
                    $images =NULL;     
                 }
                 

                 $code=NULL;
                 $total=$total_price;
                 $now_date=date('Y-m-d',strtotime('+2 hours',strtotime($updated_at)));

            if($request->has('copon')){
                 $check_copon=Copon::where('code',$copon)->first();
        
                   if($check_copon !=null){
                       
                        if($check_copon['end_date'] < $now_date){
                          $code=$check_copon['id'];
                          
                          $update_copon=Copon::where('code',$copon)->increment('use_count',1);
                          $total=0;
                        }else{
                            $code=$check_copon['id'];
                             $total=$total_price;
                        }
                   }else{
                        $code=NULL;
                   }
            }
           
             
            $description_string='';
          
          
           
             $details='';
               if(empty($menu_items)){
                    $details=$menu_items;
               }
                   
                 
           if($request->has('shope_id') &&  $shope != 3)
           {
               
                 if($request->has('menu_items')){
                         
                          if(count($menu_items)>0){

                               for($i=0;$i<count($menu_items);$i++){
                                   
                                   
                                   $select=Menu::where('id',$menu_items[$i]->product)->first();
                                   $Product_type=Product_type::where('id',$menu_items[$i]->type)->value('name');
                                   $amount=$menu_items[$i]->amount;
                                   $description_string.=PHP_EOL." ".$amount.' '.$select['product'].'  of type '.$Product_type;
                                     
                                 //  $total+=$menu_items[$i]->price * $menu_items[$i]->amount ;
                                 }
                          }else{
                              
                              $details=$menu_items;
                          }
               }

           $shop_data=Shopes::where('id',$shope)->first();


           $insert=new Requests;
           $insert->user_id=$check_token['id'];
           $insert->receiving_address=$receive;
           $insert->receiving_longitude=$receiving_longitude;
           $insert->receiving_latitude= $receiving_latitude;

           $insert->description=$description_string.'    '.PHP_EOL." ".'  '.PHP_EOL.$description.'    '.PHP_EOL.$details;
           $insert->shop_id=$shope;
           $insert->details=$description.' '.$description_string;
           $insert->delivery_address=$delivery;
           $insert->delivery_longitude=$delivery_longitude;
           $insert->delivery_latitude=$delivery_latitude;

           $insert->delivery_time=$time;
           $insert->copon=$code;
           $insert->state=1;
           $insert->image=$images;
           $insert->total_price=$total;
           $insert->delivery_type='shop_request';
           $insert->payment_id=$payment;
           $insert->accepted_time = $dateTime;
           $insert->created_at=$dateTime;
           $insert->save();
           
         
 
   //product  type amount ,price
           $arr=array();
            if($insert ==true){
  if($request->has('menu_items')){
      
      if(count($menu_items)>0){
             for($i=0;$i<count($menu_items);$i++){
               $arr[]=array(
                 "request_id"=>$insert->id,
                 "product_id"=>$menu_items[$i]->product,
                "type"=>$menu_items[$i]->type,
                "amount"=>$menu_items[$i]->amount,
                "price"=>$menu_items[$i]->price


                );
                
             }

             $insert_list=request_details::insert($arr);
      }
  }
                         
                        
                         
     
            $message['data']=$insert; 
            $message['details']=$insert_list;
            $message['error']=0;
            $message['message']='add shope data';

          }else{
            $message['data']=$insert;
            $message['error']=1;
            $message['message']='error in insert shope data ';
          }
     }elseif( $shope == 3){
         
         $insert=new Requests;
           $insert->shop_id=3;
           $insert->user_id=$check_token['id'];
           $insert->receiving_address=$receive;
           $insert->receiving_longitude=$receiving_longitude;
           $insert->receiving_latitude=$receiving_latitude;
           $insert->description=$description." ".$menu_items;       
           $insert->delivery_address=$delivery;
           $insert->delivery_longitude=$delivery_longitude;
           $insert->delivery_latitude=$delivery_latitude;
           $insert->delivery_time=$time;
           $insert->copon=$code;
           $insert->state=1;
           $insert->image=$images;
           $insert->total_price=$total;
           $insert->delivery_type='Store_request';
           $insert->payment_id=$payment;   
           $insert->accepted_time = $dateTime;
           $insert->created_at=$dateTime;
           $insert->save();

            if($insert ==true){
      
     
            $message['data']=$insert; 
            $message['error']=0;
            $message['message']='add resturant data';

          }else{
            $message['data']=$insert;
            $message['error']=1;
            $message['message']='error in insert resturant data ';
          }
          
     }else{
        
           $insert=new Requests;
           $insert->shop_id=4;
           $insert->user_id=$check_token['id'];
           $insert->receiving_address=$receive;
           $insert->receiving_longitude=$receiving_longitude;
           $insert->receiving_latitude=$receiving_latitude;
           $insert->description=$description." ".$menu_items;       
           $insert->delivery_address=$delivery;
           $insert->delivery_longitude=$delivery_longitude;
           $insert->delivery_latitude=$delivery_latitude;
           $insert->delivery_time=$time;
           $insert->copon=$code;
           $insert->state=1;
           $insert->image=$images;
           $insert->total_price=$total;
           $insert->delivery_type='tard';
           $insert->payment_id=$payment;
           $insert->accepted_time = $dateTime;
           $insert->created_at=$dateTime;
           $insert->save();

            if($insert ==true){
                 $get_user_token = DB::select("SELECT users.id , users.firebase_token, ( 6371 * acos( cos( radians($receiving_latitude) ) * cos( radians( `latitude` ) ) * cos( radians( `longitude` ) - radians('$receiving_longitude') ) + sin( radians('$receiving_latitude') ) * sin( radians( `latitude` ) ) ) ) AS distance FROM users
                            where users.firebase_token != 'null'  and users.state = 2 HAVING distance <= 50  ORDER BY distance ASC");
                 
                    foreach($get_user_token as $user){    
                           $msg = array
                                  (
                            'body'  => "يوجد طلب جديد بجوارك",
                            'title' => "طلبات جديدة",
                            'click_action' => '6',
                            'request_id' => "$insert->id"
    
                                  );
                          $fields = array
                              (
                                'to'    => $user->firebase_token,
                                'data' => $msg,
                                'notification'  => $msg
                              );
                          
                                           
    
                          $headers = array
                              (
                                'Authorization: key=' . API_ACCESS_KEY12,
                                'Content-Type: application/json'
                              );
                        #Send Reponse To FireBase Server  
                            $ch = curl_init();
                            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                            curl_setopt( $ch,CURLOPT_POST, true );
                            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                            $result = curl_exec($ch );
                            //echo $result;
                            curl_close( $ch );
                     
            
                        
                            $save=new Notification;
                            $save->title= "Hayyaak New Orders";
                            $save->body= "There is a new order next to you";
                            $save->title_ar = "طلبات جديدة";
                            $save->body_ar = "يوجد طلب جديد بجوارك";
                            $save->sender=$check_token['id'];
                            $save->is_read=0;
                            $save->request_id=$insert->id;
                            $save->user_id= $user->id;
                            $save->click_action = 6;
                            $save->created_at=$dateTime;
                            $save->save();
                    }
                    
                $message['data']=$insert; 
                $message['error']=0;
                $message['message']='add shope data';

            }else{
                $message['data']=$insert;
                $message['error']=1;
                $message['message']='error in insert shope data ';
            }
     
      }
       }
          
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
     public function make_order1(Request $request)
    {
       try{
        
          $token=$request->input('user_token');    
          
        
          
          $list= $request->json()->all();
          $image=$request->file('image');
          //$request->getContent();
         // $request->json()->all();
          //json_decode($request->input('list'));
          //      var_dump($list);
                
          $check_token=User::where('user_token',$list['user_token'])->first();
          
          if($check_token !=NULL){
              
            
          	    $state='wait';

            

                
          	  $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at))); 	 

          $check_copon=Copon::where('code',$list['copon'])->first();

             if(isset($image)) {
                  $new_name = $image->getClientOriginalName();
                  $savedFileName = rand(100000,999999).time()."_".$new_name; 
                    // give a unique name to file to    be saved
                        $destinationPath_id = 'uploads/orders';
                       $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
                 }else{
                    $images =NULL;     
                 }

           if($check_copon !=null){
                $code=$check_copon['id'];
           }else{
                $code=NULL;
           }

           if(!empty($list['shope_id'])  &&$list['shope_id'] !='')
           {

          $shop_data=Shopes::where('id',$list['shope_id'])->first();


           $insert=new Requests;
           $insert->user_id=$check_token['id'];
           $insert->receiving_address=$shop_data['addresss'];
           $insert->receiving_longitude=$shop_data['longitude'];
           $insert->receiving_latitude=$shop_data['latitude'];

           $insert->description=$list['description'];
           $insert->shop_id=$list['shope_id'];
           $insert->delivery_address=$list['delivery_place'];
           $insert->delivery_longitude=$list['delivery_longitude'];
           $insert->delivery_latitude=$list['delivery_latitude'];

           $insert->delivery_time=$list['delivery_time'];
           $insert->image=$images;
           $insert->copon=$code;
           $insert->state=$state;
           $insert->total_price=$list['total_price'];
           $insert->delivery_type='shop_request';
           $insert->payment_id=$list['payment_id'];
           $insert->created_at=$dateTime;
           $insert->save();
 
   //product  type amount ,price
           $arr=array();
            if($insert ==true){

             for($i=0;$i<count($list['menu']);$i++){
               $arr[]=array(
                 "request_id"=>$insert->id,
                  "product_id"=>$list['menu'][$i]['product'],
                "type"=>$list['menu'][$i]['type'],
                "amount"=>$list['menu'][$i]['amount'],
                "price"=>$list['menu'][$i]['price']


                );
             }

             $insert_list=request_details::insert($arr);
     
            $message['data']=$insert; 
       //     $message['details']=$insert_list;
            $message['error']=0;
            $message['message']='add shope data';

          }else{
            $message['data']=$insert;
            $message['error']=1;
            $message['message']='error in insert shope data ';
          }
     }else{
        
           $insert=new Requests;
           $insert->shop_id=4;
           $insert->user_id=$check_token['id'];
           $insert->receiving_address=$receive;
           $insert->receiving_longitude=$receiving_longitude;
           $insert->receiving_latitude=$receiving_latitude;
           $insert->description=$description;       
           $insert->delivery_address=$delivery;
           $insert->delivery_longitude=$delivery_longitude;
           $insert->delivery_latitude=$delivery_latitude;
           $insert->delivery_time=$time;
           $insert->copon=$code;
           $insert->state=$state;
           $insert->image=$images;
             $insert->total_price=$total_price;
           $insert->delivery_type='tard';
           $insert->payment_id=$payment;
           $insert->created_at=$dateTime;
           $insert->save();

            if($insert ==true){
     
            $message['data']=$insert; 
            $message['error']=0;
            $message['message']='add shope data';

          }else{
            $message['data']=$insert;
            $message['error']=1;
            $message['message']='error in insert shope data ';
          }
     
      }
          
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
     public function make_order2(Request $request)
    {
       try{
        
          $token=$request->input('user_token');    
          
        
          
          $list=json_decode($request->input('list'));
          $image=$request->file('image');
          //$request->getContent();
         // $request->json()->all();
          //json_decode($request->input('list'));
          //      var_dump($list);
                
          $check_token=User::where('user_token',$list[0]->user_token)->first();
          
          if($check_token !=NULL){
              
            
          	    $state='wait';

            

                
          	  $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at))); 	 

          $check_copon=Copon::where('code',$list[0]->copon)->first();

             if(isset($image)) {
                  $new_name = $image->getClientOriginalName();
                  $savedFileName = rand(100000,999999).time()."_".$new_name; 
                    // give a unique name to file to    be saved
                        $destinationPath_id = 'uploads/orders';
                       $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
                 }else{
                    $images =NULL;     
                 }

           if($check_copon !=null){
                $code=$check_copon['id'];
           }else{
                $code=NULL;
           }

           if(!empty($list[0]->shope_id)  &&$list[0]->shope_id !='')
           {

          $shop_data=Shopes::where('id',$list[0]->shope_id)->first();


           $insert=new Requests;
           $insert->user_id=$check_token['id'];
           $insert->receiving_address=$shop_data['addresss'];
           $insert->receiving_longitude=$shop_data['longitude'];
           $insert->receiving_latitude=$shop_data['latitude'];

           $insert->description=$list[0]->description;
           $insert->shop_id=$list[0]->shope_id;
           $insert->delivery_address=$list[0]->delivery_place;
           $insert->delivery_longitude=$list[0]->delivery_longitude;
           $insert->delivery_latitude=$list[0]->delivery_latitude;

           $insert->delivery_time=$list[0]->delivery_time;
           $insert->image=$images;
           $insert->copon=$code;
           $insert->state=$state;
           $insert->total_price=$list[0]->total_price;
           $insert->delivery_type='shop_request';
           $insert->payment_id=$list[0]->payment_id;
           $insert->created_at=$dateTime;
           $insert->save();
 
   //product  type amount ,price
           $arr=array();
            if($insert ==true){

             for($i=0;$i<count($list[0]->menu);$i++){
               $arr[]=array(
                "request_id"=>$insert->id,
                "product_id"=>$list[0]->menu[$i]->product,
                "type"=>$list[0]->menu[$i]->type,
                "amount"=>$list[0]->menu[$i]->amount,
                "price"=>$list[0]->menu[$i]->price


                );
             }

             $insert_list=request_details::insert($arr);
     
            $message['data']=$insert; 
       //     $message['details']=$insert_list;
            $message['error']=0;
            $message['message']='add shope data';

          }else{
            $message['data']=$insert;
            $message['error']=1;
            $message['message']='error in insert shope data ';
          }
     }else{
        
           $insert=new Requests;
           $insert->shop_id=4;
           $insert->user_id=$check_token['id'];
           $insert->receiving_address=$receive;
           $insert->receiving_longitude=$receiving_longitude;
           $insert->receiving_latitude=$receiving_latitude;
           $insert->description=$description;       
           $insert->delivery_address=$delivery;
           $insert->delivery_longitude=$delivery_longitude;
           $insert->delivery_latitude=$delivery_latitude;
           $insert->delivery_time=$time;
           $insert->copon=$code;
           $insert->state=$state;
           $insert->image=$images;
             $insert->total_price=$total_price;
           $insert->delivery_type='tard';
           $insert->payment_id=$payment;
           $insert->created_at=$dateTime;
           $insert->save();

            if($insert ==true){
     
            $message['data']=$insert; 
            $message['error']=0;
            $message['message']='add shope data';

          }else{
            $message['data']=$insert;
            $message['error']=1;
            $message['message']='error in insert shope data ';
          }
     
      }
          
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
     public function make_order3(Request $request)
    {
       try{
        
          $token=$request->input('user_token');    
          
        
          
       
          $check_token=User::where('user_token',$token)->first();
          
          if($request->has('user_token') && $check_token !=NULL){
              
              $shope=$request->input('shope_id');
          	  $receive=json_decode($request->input('receive'));
          	  

          	  $delivery=json_decode($request->input('delivery'));
           

          	  $time=$request->input('delivery_time');
          	  $description=$request->input('description');  
              $total_price=$request->input('total_price');
              //details
              $menu_items= json_decode($request->input('menu_items'));
              $image=$request->file('image');
              $payment=$request->input('payment_id');
          	  $copon=$request->input('copon');
          	    $state='wait';



                
          	  $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at))); 	 

          $check_copon=Copon::where('code',$copon)->first();

             if(isset($image)) {
                  $new_name = $image->getClientOriginalName();
                  $savedFileName = rand(100000,999999).time()."_".$new_name; 
                    // give a unique name to file to    be saved
                        $destinationPath_id = 'uploads/orders';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
                 }else{
                    $images =NULL;     
                 }

           if($check_copon !=null){
                $code=$check_copon['id'];
           }else{
                $code=NULL;
           }

           if($request->has('shope_id'))
           {

          $shop_data=Shopes::where('id',$shope)->first();


           $insert=new Requests;
           $insert->user_id=$check_token['id'];
           $insert->receiving_address=$shop_data['addresss'];
           $insert->receiving_longitude=$shop_data['longitude'];
           $insert->receiving_latitude=$shop_data['latitude'];

           $insert->description=$description;
           $insert->shop_id=$shope;
           $insert->delivery_address=$delivery[0]->delivery_place;
           $insert->delivery_longitude=$delivery[0]->delivery_longitude;
           $insert->delivery_latitude=$delivery[0]->delivery_latitude;

           $insert->delivery_time=$time;
           $insert->copon=$code;
           $insert->state=$state;
           $insert->image=$images;
           $insert->total_price=$total_price;
           $insert->delivery_type='shop_request';
           $insert->payment_id=$payment;
           $insert->created_at=$dateTime;
           $insert->save();
 
   //product  type amount ,price
           $arr=array();
            if($insert ==true){

             for($i=0;$i<count($menu_items);$i++){
               $arr[]=array(
                 "request_id"=>$insert->id,
                  "product_id"=>$menu_items[$i]->product,
                "type"=>$menu_items[$i]->type,
                "amount"=>$menu_items[$i]->amount,
                "price"=>$menu_items[$i]->price


                );
             }

             $insert_list=request_details::insert($arr);
     
            $message['data']=$insert; 
            $message['details']=$insert_list;
            $message['error']=0;
            $message['message']='add shope data';

          }else{
            $message['data']=$insert;
            $message['error']=1;
            $message['message']='error in insert shope data ';
          }
     }else{
        
           $insert=new Requests;
           $insert->shop_id=4;
           $insert->user_id=$check_token['id'];
           $insert->receiving_address=$receive;
           $insert->receiving_longitude=$receiving_longitude;
           $insert->receiving_latitude=$receiving_latitude;
           $insert->description=$description;       
           $insert->delivery_address=$delivery;
           $insert->delivery_longitude=$delivery_longitude;
           $insert->delivery_latitude=$delivery_latitude;
           $insert->delivery_time=$time;
           $insert->copon=$code;
           $insert->state=$state;
           $insert->image=$images;
             $insert->total_price=$total_price;
           $insert->delivery_type='tard';
           $insert->payment_id=$payment;
           $insert->created_at=$dateTime;
           $insert->save();

            if($insert ==true){
     
            $message['data']=$insert; 
            $message['error']=0;
            $message['message']='add shope data';

          }else{
            $message['data']=$insert;
            $message['error']=1;
            $message['message']='error in insert shope data ';
          }
     
      }
          
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

     public function make_order4(Request $request)
    {
       try{
        
          $token=$request->input('user_token');    
          
        
          
          $list= $request->json()->all();
          $image=$request->file('image');
          //$request->getContent();
         // $request->json()->all();
          //json_decode($request->input('list'));
          //      var_dump($list);
                
          $check_token=User::where('user_token',$list['user_token'])->first();
          
          if($check_token !=NULL){
              
            
          	    $state='wait';

            

                
          	  $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at))); 	 

          $check_copon=Copon::where('code',$list['copon'])->first();

             if(isset($image)) {
                  $new_name = $image->getClientOriginalName();
                  $savedFileName = rand(100000,999999).time()."_".$new_name; 
                    // give a unique name to file to    be saved
                        $destinationPath_id = 'uploads/orders';
                       $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
                 }else{
                    $images =NULL;     
                 }

           if($check_copon !=null){
                $code=$check_copon['id'];
           }else{
                $code=NULL;
           }

           if(!empty($list['shope_id'])  &&$list['shope_id'] !='')
           {

          $shop_data=Shopes::where('id',$list['shope_id'])->first();


           $insert=new Requests;
           $insert->user_id=$check_token['id'];
           $insert->receiving_address=$shop_data['addresss'];
           $insert->receiving_longitude=$shop_data['longitude'];
           $insert->receiving_latitude=$shop_data['latitude'];

           $insert->description=$list['description'];
           $insert->shop_id=$list['shope_id'];
           $insert->delivery_address=$list['delivery'][0]['delivery_place'];
           $insert->delivery_longitude=$list['delivery'][0]['delivery_longitude'];
           $insert->delivery_latitude=$list['delivery'][0]['delivery_latitude'];

           $insert->delivery_time=$list['delivery_time'];
           $insert->image=$images;
           $insert->copon=$code;
           $insert->state=$state;
           $insert->total_price=$list['total_price'];
           $insert->delivery_type='shop_request';
           $insert->payment_id=$list['payment_id'];
           $insert->created_at=$dateTime;
           $insert->save();
 
   //product  type amount ,price
           $arr=array();
            if($insert ==true){

             for($i=0;$i<count($list['menu']);$i++){
               $arr[]=array(
                "request_id"=>$insert->id,
                "product_id"=>$list['menu'][$i]['product'],
                "type"=>$list['menu'][$i]['type'],
                "amount"=>$list['menu'][$i]['amount'],
                "price"=>$list['menu'][$i]['price']


                );
             }

             $insert_list=request_details::insert($arr);
     
            $message['data']=$insert; 
       //     $message['details']=$insert_list;
            $message['error']=0;
            $message['message']='add shope data';

          }else{
            $message['data']=$insert;
            $message['error']=1;
            $message['message']='error in insert shope data ';
          }
     }else{
        
           $insert=new Requests;
           $insert->shop_id=4;
           $insert->user_id=$check_token['id'];
           $insert->receiving_address=$receive;
           $insert->receiving_longitude=$receiving_longitude;
           $insert->receiving_latitude=$receiving_latitude;
           $insert->description=$description;       
           $insert->delivery_address=$delivery;
           $insert->delivery_longitude=$delivery_longitude;
           $insert->delivery_latitude=$delivery_latitude;
           $insert->delivery_time=$time;
           $insert->copon=$code;
           $insert->state=$state;
           $insert->image=$images;
             $insert->total_price=$total_price;
           $insert->delivery_type='tard';
           $insert->payment_id=$payment;
           $insert->created_at=$dateTime;
           $insert->save();

            if($insert ==true){
     
            $message['data']=$insert; 
            $message['error']=0;
            $message['message']='add shope data';

          }else{
            $message['data']=$insert;
            $message['error']=1;
            $message['message']='error in insert shope data ';
          }
     
      }
          
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
    
    
  public function user_Datacounts(Request $request){
      try{
        
          $token=$request->input('user_token');
         
         
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

  
                        
              

           
            
            if($check_token['state']==2){
            
               $credit= Wallet::where('user_id',$check_token['id'])->value('credit');
            //   Requests::join('offers','requestes.id','=','offers.request_id')
            //             ->where([['requestes.state',4],['offers.driver_id',$check_token['id']]])->sum('requestes.total_price');
            
                       $requests=Requests::join('offers','requestes.id','=','offers.request_id')->
                    where([['offers.driver_id',$check_token['id']],['requestes.state',4],['offers.state','accepted']])->count();
        
                  //  $journy=Requests::where([['user_id',$check_token['id']],['type',2]])->count();
        
                    $rate=Rating::where([['driver_id',$check_token['id']],['who_rate','user']])->count();
                    
                    $noty=setting::where('user_id',$check_token['id'])->value('notification');
                    
            }elseif($check_token['state']==NULL || empty($check_token['state']) ||$check_token['state']=='' || $check_token['type']=="training"){
            
                     $credit=0;
            
                       $requests=0;
        
                  //  $journy=Requests::where([['user_id',$check_token['id']],['type',2]])->count();
        
                      $rate=0;
                    
                      $noty="ON";
            
            
            
            }else{
              $credit=Wallet::where('user_id',$check_token['id'])->value('credit');
                    $requests=Requests::where([['user_id',$check_token['id']],['requestes.state',4]])->count();
        
                   
        
                    $rate=Rating::where([['user_id',$check_token['id']],['who_rate','driver']])->count();
                     $noty=setting::where('user_id',$check_token['id'])->value('notification');


            }
          
             $message['credit']=$credit;
             $message['requests']=$requests;
            
              
             $message['rate']=$rate;
           $message['notify']=$noty;
           
         
            

          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);

  }



    public function track_order(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
         
         
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

                $id=$request->input('request_id');


                $request_data=Requests::select('requestes.id','requestes.shop_id','requestes.receiving_longitude','requestes.receiving_latitude','requestes.receiving_address','requestes.delivery_longitude','requestes.delivery_latitude','requestes.delivery_address',
                  'requestes.delivery_time','requestes.state','requestes.created_at')
                
                ->where('requestes.id',$id)
                ->get();

        
          if(count($request_data)  >0){


            $message['data']=$request_data;
            $message['error']=0;
            $message['message']='add shope data';

          }else{
            $message['data']=$request_data;
            $message['error']=1;
            $message['message']='error in insert shope data ';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


    public function user_cancel_order(Request $request){
       
         
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));                  

          $token=$request->input('user_token');
          $cancel_reason = $request->input('cancel_reason');
          $request_id = $request->input('request_id');
          $driver_id = $request->input('driver_id');
             
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

                
            if( $cancel_reason == '-1'  && $driver_id == '-1' ){
                  
                  $cancelRequest = \App\Requests::where('id' , $request_id)->update(['state' => 6]);
                  
                  if($cancelRequest == true){
                      $message['error'] = 0;
                      $message['message'] = "order is canceled";
                      return response()->json($message);
                  }
                   
              }

            $accpted_time =0;
            
            $order_state=Offers::where([['state' , "accepted"],['request_id',$request_id]])->first();

            if($order_state != NULL){
                $accpted_time = $order_state->accepted_time;
                $change_time = date('Y-m-d H:i:s',strtotime('+7 minutes',strtotime($accpted_time)));

            }
        
        
        $check_bill = \App\Bill::where([['request_id' , $request_id], ['user_id' ,$check_token->id ]])->first();
              
     if( $check_bill != NULL && $accpted_time != 0){
          if($dateTime < $change_time){
              
            
              if($request->has('cancel_reason') && $request->has('driver_id')){
                  
                  $check = \App\Cancel_request::where([['user_id' ,$check_token->id ],['request_id' ,$request_id ] , ['driver_id' ,$driver_id ]])->first();
                  if($check != NULL){
                      $message['error'] = 6;
                      $message['message'] = "you have send cancel request before";      
                      return response()->json($message);

                  }else{
                      $add = new \App\Cancel_request;
                      $add->user_id = $check_token->id;
                      $add->request_id = $request_id;
                      $add->text = $cancel_reason;
                      $add->driver_id = $driver_id;
                      $add->state = "wait";
                      $add->created_at = $dateTime;
                      $add->save();
                      
                      if($add == true){
                          
    
    
                        $get_user_token = User::select('id','firebase_token')->where('id', $driver_id)->first();
           
                         // return $get_user_token[0]->firebase_token;
    
    
                         ////////////////////////////////////////////////////////
                         
                           $msg = array
                                  (
                            'body'  =>$cancel_reason ."العميل يريد الانسحاب من الطلب بسبب",
                            'title' => "انسحاب من الطلب",
                            'click_action' => '2',
                            'request_id' => "$request_id"
    
                                  );
                          $fields = array
                              (
                                'to'    => $get_user_token->firebase_token,
                                'data' => $msg,
                                'notification'  => $msg
                              );
                          
                                           
    
                          $headers = array
                              (
                                'Authorization: key=' . API_ACCESS_KEY12,
                                'Content-Type: application/json'
                              );
                        #Send Reponse To FireBase Server  
                            $ch = curl_init();
                            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                            curl_setopt( $ch,CURLOPT_POST, true );
                            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                            $result = curl_exec($ch );
                            //echo $result;
                            curl_close( $ch );
                     
            
                        
                            $save=new Notification;
                            $save->title= "Withdrawal from the request";
                            $save->body= "The customer wants to withdraw from the order because ".$cancel_reason;
                            $save->title_ar = "انسحاب من الطلب";
                            $save->body_ar = $cancel_reason ."العميل يريد الانسحاب من الطلب بسبب";
                            $save->sender=$check_token['id'];
                            $save->is_read=0;
                            $save->request_id=$request_id;
                            $save->user_id= $driver_id;
                            $save->click_action = 2;
                            $save->created_at=$dateTime;
                            $save->save();
                            
                          $message['error'] = 0;
                          $message['message'] = "cancel request is send successfuly";
                      }else{
                          $message['error'] = 1;
                          $message['message'] = "error, please try again";
                      }
                      
                  }
              }else{
                  $message['error'] =  1;
                  $message['message'] = "please enter all the data";
                return response()->json($message);

              }
              
              
              
              
          }else{
                  $message['error'] =  1;
                  $message['message'] = "time is more that 7 mins";
                return response()->json($message);

              }
          
          }else{
            $driverxs = Offers::where('request_id',$request_id)->get();

              $action = Requests::where('id',$request_id)->delete();

              $delete_offers=Offers::where('request_id',$request_id)->delete();

              $delete_details=request_details::where('request_id',$request_id)->delete();

              $update_user=User::where('id',$check_token['id'])->increment('cancel_times',1);
                  
              $delete_chat=Messages::where('request_id',$request_id)->delete();

              $delete_bill=Bill::where('request_id',$request_id)->delete();


            foreach( $driverxs  as $each_driver){
              
              $get_user_token = User::select('id','firebase_token')->where('id', $each_driver->driver_id)->first();
       
              // return $get_user_token[0]->firebase_token;


              ////////////////////////////////////////////////////////
              
                $msg = array
                       (
                'body'  =>" تم إلغاء الطلب بواسطه العميل",
                'title' => "انسحاب من الطلب",
                'click_action' => '10',
                'request_id' => "$request_id"

                       );
               $fields = array
                   (
                     'to'    => $get_user_token['firebase_token'],
                     'data' => $msg,
                     'notification'  => $msg
                   );
               
                                

               $headers = array
                   (
                     'Authorization: key=' . API_ACCESS_KEY12,
                     'Content-Type: application/json'
                   );
             #Send Reponse To FireBase Server  
                 $ch = curl_init();
                 curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                 curl_setopt( $ch,CURLOPT_POST, true );
                 curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                 curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                 curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                 curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                 $result = curl_exec($ch );
                 //echo $result;
                 curl_close( $ch );
          
 
             
                  $save=new Notification;
                  $save->title = "Order Cancelation ";
                  $save->body = "User Cancel the order";
                  $save->title_ar = "انسحاب من الطلب";
                  $save->body_ar = " تم إلغاء الطلب بواسطه العميل";
                  $save->sender=$check_token['id'];
                  $save->is_read=0;
                  $save->request_id=$request_id;
                  $save->user_id = $each_driver->driver_id;
                  $save->click_action = 10;
                  $save->created_at=$dateTime;
                  $save->save();
              }
                 
              $message['error']=0;
              $message['message']='order is canceled sucess';


             
                             
          }
        
       }else{
            $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       
         return response()->json($message);
    }
    



    public function accept_cancelRequest(Request $request){
        
        $user_token = $request->input('user_token');
        $request_id = $request->input('request_id');
        $action = $request->input('action');
        $updated_at = carbon::now()->toDateTimeString();
        $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));                  

        $check_token=User::where('user_token',$user_token)->first();
      
      
        if($request->has('user_token') && $check_token !=NULL){
            
            if( $action == "yes"){
                
                $update_cancel  = \App\Cancel_request::where([['request_id' ,$request_id] , ['driver_id' ,  $check_token->id]] )->update(["state" => $action]);
                
                $action = Requests::where('id',$request_id)->delete();


                 if($action == true){
                     
                 $delete_offers=Offers::where('request_id',$request_id)->delete();

                 $delete_details=request_details::where('request_id',$request_id)->delete();

                  $update_user=User::where('id',$check_token['id'])->increment('cancel_times',1);
                  
                  
                  $delete_chat=Messages::where('request_id',$request_id)->delete();

                 
                     
                   $delete_bill=Bill::where('request_id',$request_id)->delete();

                      $message['error']=0;
                      $message['message']='order is canceled sucess';
                 }else{
                    $message['error']=1;
                    $message['message']='error, in cancel request ';
                 }
            }else{
                $update_cancel  = \App\Cancel_request::where([['request_id' ,$request_id] , ['driver_id' ,  $check_token->id]] )->update(["state" => $action]);

                $user_id  = \App\Cancel_request::where([['request_id' ,$request_id] , ['driver_id' ,  $check_token->id]] )->value('user_id');


                    $get_user_token = User::select('id','firebase_token')->where('id', $user_id)->first();
       
                     // return $get_user_token[0]->firebase_token;


                     ////////////////////////////////////////////////////////
                     
                       $msg = array
                              (
                        'body'  =>"المندوب لم يوافق على الانسحاب",
                        'title' => "انسحاب من الطلب",
                        'click_action' => '2',
                        'data' => '$request_id'

                              );
                      $fields = array
                          (
                            'to'    => $get_user_token['firebase_token'],
                            'data' => $mg = array(
                                                     'click_action' => '2',
                                                     'data' =>'$request_id'
                                                ),
                            'notification'  => $msg
                          );
                      
                                       

                      $headers = array
                          (
                            'Authorization: key=' . API_ACCESS_KEY12,
                            'Content-Type: application/json'
                          );
                    #Send Reponse To FireBase Server  
                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        $result = curl_exec($ch );
                        //echo $result;
                        curl_close( $ch );
                 
        
                    
                        $save=new Notification;
                        $save->title = "Withdrawal from the request ";
                        $save->body = "The delegate did not agree to the withdrawal";
                        $save->title_ar = "انسحاب من الطلب";
                        $save->body_ar = "المندوب لم يوافق على الانسحاب";
                        $save->sender=$check_token['id'];
                        $save->is_read=0;
                        $save->request_id=$request_id;
                        $save->user_id = $user_id;
                        $save->click_action = 2;
                        $save->created_at=$dateTime;
                        $save->save();
                        
                        
                        $add  = new \App\Messages;
                        $add->sender_id = $check_token['id'];
                        $add->receiver_id = $user_id;
                        $add->message = "المندوب لم يوافق على الانسحاب";
                        $add->request_id = $request_id;
                        $add->created_at = $dateTime;
                        $add->save();
                        
                $message['error']=0;
                $message['message']='order is not canceled sucess';
                        
            }
        }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }
         return response()->json($message);

    }
   

     public function user_accept_offer(Request $request)
    {
       try{
            $token=$request->input('user_token');
            $id=$request->input('offer_id');
          
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
               
            $check_token=User::where('user_token',$token)->first();
          
                  
            if($request->has('user_token') && $check_token !=NULL){
        
                    
                  
                    $update=Offers::where('id',$id)->update([
                                                              'state'=>'accepted',
                                                              'accepted_time'=>$dateTime,
                                                              'updated_at'=>$dateTime
                                                        
                                                            ]);
                    
                  
        
                  if($update ==true){
                      
                        $offer_data=Offers::where('id',$id)->first();
                        $request_data=Requests::where('id',$offer_data['request_id'])->first();
                                            

                        $update=Offers::where([['id','!=',$id],['request_id',$offer_data['request_id']]])->update([
                                                                                                          'state'=>'refused',
                                                                                                          'updated_at'=>$dateTime
                                                                                                    
                                                                                                        ]);
                        $string=$request_data['delivery_time'];
                        $string_after=$string[0];
          
          
          
                   $after=date('Y-m-d H:i:s',strtotime('+'.$string_after.'hours',strtotime($dateTime)));
                   
                   
                   
                   
                        
                        if($request_data['delivery_type']=='tard'){
                            
                             if($request_data['copon'] ==NULL){
                     
                              $update_request=Requests::where('id',$request_data['id'])
                                ->update([
                                  'state'=>2,
                                  'total_price'=>$offer_data['price'],
                                   'accepted_time'=>$after,
                                  'updated_at'=>$dateTime
                            
                                ]);
                             }else{
                                 
                                   $update_request=Requests::where('id',$request_data['id'])
                                ->update([
                                  'state'=>2,
                                  'accepted_time'=>$after,
                                  'updated_at'=>$dateTime
                            
                                ]);
                                 
                             }
                            
                        }else{
                                $update_request=Requests::where('id',$request_data['id'])
                                ->update([
                                  'state'=>2,
                                  'accepted_time'=>$after,
                                  'updated_at'=>$dateTime
                            
                                ]);
                            
                        }
                    
                        
                        
                     
                             
                              $mess=new Messages;     
                              $mess->sender_id=$check_token['id'];
                              $mess->receiver_id=$offer_data['driver_id'];
                              $mess->request_id=$request_data['id'];
                              $mess->message=$request_data['description'];
                           
                              $mess->created_at=$dateTime;
                              $mess->save();   

                          

                              if($request_data['image'] !=NULL && !empty($request_data['image']) && $request_data['image'] !=' ') {
                                  
                                    $image=$request_data['image'];
                                    
                                
                                    //  $new_name = $image->getClientOriginalName();
                                     // $savedFileName = rand(100000,999999).time()."_".$new_name; 
                                        // give a unique name to file to    be saved
                                            $destinationPath = 'uploads/messages';
                                           copy('uploads/orders/'.$image, $destinationPath.'/'.$image);
                                
                                            $images = $image;
                                            
                                            
                                            $mess1=new Messages;
                    
                                  $mess1->sender_id=$check_token['id'];
                                  $mess1->request_id=$request_data['id'];
                                  $mess1->image=$images;
                                  $mess1->receiver_id=$offer_data['driver_id'];
                                  $mess1->created_at=$dateTime;
                                  $mess1->save();
                                          
                                    
                                  
                                /*  //  $new_name = $image->getClientOriginalName();
                                    //$savedFile = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                    $destinationPath ="uploads/messages";
                                    
                                 
                                    copy($destinationPath_id.'/'.$savedFileName, $destinationPath.'/'.$image);
                        
                                    $images = $savedFile;*/
                                    
                              }
                      

                            
                                    $user_tax=About::where('id',1)->value('user_tax');
               
                                      $tax=str_replace('%', '', $user_tax);
                 
                              //           $tax=round(($tax/$request['total_price'])*100,2);
                
                                 
                                     
                          /*   $action=new Bill;
                             $action->user_id=$check_token['id'];
                             $action->driver_id=$offer_data['driver_id'];
                             $action->request_id=$offer_data['request_id'];
                             $action->order_price=$request['total_price'];
                             $action->delivery_price=$offer_data['price'];
                             $action->tax=$tax;
                             $action->total_price=$offer_data['price']+$tax+$request['total_price'];
                             $action->created_at=$dateTime;
                             $action->save();*/
                             
                             
                             
                     
                          $mess2=new Messages;     
                          $mess2->sender_id=$check_token['id'];
                          $mess2->receiver_id=$offer_data['driver_id'];
                          $mess2->request_id=$request_data['id'];
                          $mess2->message='

سعر الاوردر :'.$request_data['total_price'].'  '.PHP_EOL.'وسيتم خصم ضريبه :'.$tax.'ر.س'.PHP_EOL.',وسعر التكلفه :'.$offer_data['price'];

                          $mess2->created_at=$dateTime;
                          $mess2->save(); 
                             
                             
                             
                             
                         
            
                        
           $state_data=Requests::select('state.name')->join('state','requestes.id','=','state.id')->where('requestes.id',$request_data['id'])->first();

             
        
                      try{       
               //  $cc = 0;
                 
                $title ='your offer is accepted';
                $body  =$check_token['first_name'].' has accepted your offer on  order '.$offer_data['request_id'];
                $title_ar =  "عرضك مقبول";
                $body_ar = $offer_data['request_id']." قبل عرضك على الطلب ".$check_token['first_name'];
        
        
               $get_user_token =User::select('firebase_token')->where('id', '=',$offer_data['driver_id'])->first();
               
                  // return $get_user_token[0]->firebase_token;
        
        
                             ////////////////////////////////////////////////////////
                             
                               $msg = array
                                      (
                                'body'  => $body,
                                'title' => $title,
                                'state'=>$state_data,
                                          
                                      );
                              $fields = array
                                  (
                                    'to'    => $get_user_token['firebase_token'],
                                    'notification'  => $msg
                                  );
                              
                                               
        
                              $headers = array
                                  (
                                    'Authorization: key=' . API_ACCESS_KEY12,
                                    'Content-Type: application/json'
                                  );
                            #Send Reponse To FireBase Server  
                                $ch = curl_init();
                                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                curl_setopt( $ch,CURLOPT_POST, true );
                                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                $result = curl_exec($ch );
                                //echo $result;
                                curl_close( $ch );
                         
                
                
                    $save=new Notification;
                    $save->title=$title;
                    $save->body=$body;
                    $save->title_ar = $title_ar;
                    $save->body_ar = $body_ar;
                    $save->sender=$check_token['id'];
                    $save->is_read=0;
                    $save->request_id=$offer_data['request_id'];
                    $save->user_id=$offer_data['driver_id'];
                    $save->created_at=$dateTime;
                    $save->save();
                    
                    
                    $all_users = Offers::select('users.id','users.firebase_token')
                                        ->join('users' , 'offers.driver_id' ,'=' ,'users.id')
                                        ->where([['offers.id','!=',$id],['offers.request_id',$offer_data['request_id']]])->get();
                   
                    foreach( $all_users as $driverx){
                          $msg = array
                                      (
                                'body'  => "اعتذر منك ☹️ العميل اختار عرض اخر 🙂 ولا يهمك انتظر طلبات القادمة",
                                'title' => "عروض الطلبات",
                                
                                          
                                      );
                              $fields = array
                                  (
                                    'to'    => $driverx->firebase_token,
                                    'notification'  => $msg
                                  );
                              
                                               
        
                              $headers = array
                                  (
                                    'Authorization: key=' . API_ACCESS_KEY12,
                                    'Content-Type: application/json'
                                  );
                            #Send Reponse To FireBase Server  
                                $ch = curl_init();
                                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                curl_setopt( $ch,CURLOPT_POST, true );
                                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                $result = curl_exec($ch );
                                //echo $result;
                                curl_close( $ch );
                         
                
                        
                            $save=new Notification;
                            $save->title= "Offers requests";
                            $save->body= "I apologize ☹️ The customer chose another offer 🙂 and do not care to wait for the next requests";
                            $save->title_ar = "عروض الطلبات";
                            $save->body_ar = "اعتذر منك ☹️ العميل اختار عرض اخر 🙂 ولا يهمك انتظر طلبات القادمة";
                            $save->sender=$check_token['id'];
                            $save->is_read=0;
                            $save->request_id=$offer_data['request_id'];
                            $save->user_id = $driverx->id;
                            $save->created_at=$dateTime;
                            $save->save();
                    }
                    
                     }catch(Exception $ex){
                       $message['error']=4;
                       $message['message']='error in send notification';
                      
                      }
                        $datax = [
                            "total_price" => $request_data['total_price'],
                            "taxs" => $tax,
                            "price" => $offer_data['price'],
                            ];
                      
                            $message['data'] = $datax;
                            $message['error']=0;
                            $message['message']='offer is accepted sucess';
                
                          }else{
                            
                            $message['error']=1;
                            $message['message']='error in accept offer';
                          }
                  }else{
                      $message['error']=3;
                    $message['message']='this token is not exist'; 
                  }

           
          
          
       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

    //for driver
    public function order_byid(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
          $id=$request->input('request_id');
          $long=$request->input('longitude');
          $lat=$request->input('latitude');
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


            $check_shope=Requests::where('id',$id)->value('shop_id');
    
                if($long ==null|| $long==''){
    
                  $long=$check_token['longitude'];
                }
    
                  if($lat ==null|| $lat==''){
    
                  $lat=$check_token['latitude'];
                }
    
    
         if($check_token['state']==2){
             
              if($check_shope !=4){
                
                      
                $request_data=DB::select('select requestes.id,(select offers.price from offers where offers.request_id=requestes.id AND offers.state="accepted" limit 1)as offer_price,requestes.shop_id,shopes.name,shopes.image as shope_image,requestes.user_id,users.first_name,users.image,users.phone,
                requestes.receiving_address, requestes.receiving_longitude,requestes.receiving_latitude,requestes.delivery_longitude as client_longitude,requestes.delivery_latitude as client_latitude,requestes.delivery_address,requestes.description,requestes.delivery_time,state.name as state,requestes.created_at,
                ( 6371 * acos( cos( radians(requestes.delivery_latitude) ) * cos(radians(shopes.latitude) ) * cos( radians(shopes.longitude) - radians(requestes.delivery_longitude) ) 
             + sin( radians(requestes.delivery_latitude) ) * sin( radians(shopes.latitude)))) AS client_shope_distance,
             ( 6371 * acos( cos( radians(shopes.latitude) ) * cos( radians('.$lat.') ) * cos( radians('.$long.') - radians(shopes.longitude) ) 
             + sin( radians(shopes.latitude) ) * sin( radians('.$lat.')))) AS agent_shope_distance , requestes.payment_id , requestes.updated_at as deliveryTime
             FROM requestes
                    join shopes ON requestes.shop_id=shopes.id
                     join state ON requestes.state=state.id
                    join users ON requestes.user_id=users.id
                    
                    where  requestes.id ='.$id);
                    
                   // return $request_data;
                    
    
                        
                        $user=Requests::where('id',$id)->value('user_id');
                        
                         $check=Rating::where([['rating.user_id',$user],['driver_id',$check_token['id']],['who_rate','driver'],['request_id',$id]])->get();
                         if( count($request_data)>0){
                          if(count($check)>0  ){
                               
                               $request_data[0]->rate_state='rated';
                          }else{
                              
                               $request_data[0]->rate_state=NULL;
                          }
                             
                         }
                         $request_data[0]->accepted_time = \App\Offers::where([['state' , "accepted"], ['request_id' , $id]])->value('accepted_time');

    
                        
                    
    
              
            }else{
                    
            
                $request_data=DB::select('select requestes.id,(select offers.price from offers where offers.request_id=requestes.id AND offers.state="accepted" limit 1)as offer_price,requestes.shop_id,shopes.name,shopes.image as shope_image,requestes.user_id,users.first_name,users.image,users.phone,
                requestes.receiving_address, requestes.receiving_longitude,requestes.receiving_latitude,requestes.delivery_longitude as client_longitude,requestes.delivery_latitude as client_latitude,requestes.delivery_address,requestes.description,requestes.delivery_time,state.name as state,requestes.created_at,
              
                ( 6371 * acos( cos( radians(requestes.delivery_latitude) ) * cos(radians(requestes.receiving_latitude) ) * cos( radians(requestes.receiving_longitude) - radians(requestes.delivery_longitude) ) 
             + sin( radians(requestes.delivery_latitude) ) * sin( radians(requestes.receiving_latitude)))) AS client_shope_distance,
             
             
             ( 6371 * acos( cos( radians(requestes.receiving_latitude) ) * cos( radians('.$lat.') ) * cos( radians('.$long.') - radians(requestes.receiving_longitude) ) 
             + sin( radians(requestes.receiving_latitude) ) * sin( radians('.$lat.')))) AS agent_shope_distance, requestes.payment_id, requestes.updated_at as deliveryTime
             FROM requestes
                    join shopes ON requestes.shop_id=shopes.id
                     join state ON requestes.state=state.id
                    join users ON requestes.user_id=users.id
                    
                    where  requestes.id ='.$id);
                    
                   // return $request_data;
                    
                      $user=Requests::where('id',$id)->value('user_id');
                        
                         $check=Rating::where([['rating.user_id',$user],['driver_id',$check_token['id']],['who_rate','driver'],['request_id',$id]])->get();
                         
                          if( count($request_data)>0){
                          if(count($check)>0  ){
                               
                               $request_data[0]->rate_state='rated';
                          }else{
                              
                               $request_data[0]->rate_state=NULL;
                          }
                          }
                          $request_data[0]->accepted_time = \App\Offers::where([['state' , "accepted"], ['request_id' , $id]])->value('accepted_time');

          }
    
    
    
    
    }else{
        
             if($check_shope !=4){
                
                      
                $request_data=DB::select('select requestes.id,(select offers.price from offers where offers.request_id=requestes.id AND offers.state="accepted" limit 1)as offer_price,requestes.shop_id,shopes.name,shopes.image as shope_image,(select offers.driver_id from offers where offers.request_id=requestes.id AND offers.state="accepted" limit 1)as user_id,
                (select users.first_name  from offers join users ON offers.driver_id=users.id where offers.request_id=requestes.id AND offers.state="accepted" limit 1)as first_name,(select users.image  from offers join users ON offers.driver_id=users.id where offers.request_id=requestes.id AND offers.state="accepted" limit 1)as image,(select users.phone  from offers join users ON offers.driver_id=users.id where offers.request_id=requestes.id AND offers.state="accepted" limit 1)as phone,
                requestes.receiving_address, requestes.receiving_longitude,requestes.receiving_latitude,requestes.delivery_longitude as client_longitude,requestes.delivery_latitude as client_latitude,requestes.delivery_address,requestes.description,requestes.delivery_time,state.name as state,requestes.created_at,
                ( 6371 * acos( cos( radians(requestes.delivery_latitude) ) * cos(radians(shopes.latitude) ) * cos( radians(shopes.longitude) - radians(requestes.delivery_longitude) ) 
             + sin( radians(requestes.delivery_latitude) ) * sin( radians(shopes.latitude)))) AS client_shope_distance,
             ( 6371 * acos( cos( radians(shopes.latitude) ) * cos( radians('.$lat.') ) * cos( radians('.$long.') - radians(shopes.longitude) ) 
             + sin( radians(shopes.latitude) ) * sin( radians('.$lat.')))) AS agent_shope_distance, requestes.payment_id, requestes.updated_at as deliveryTime
             FROM requestes
                    join shopes ON requestes.shop_id=shopes.id
                     join state ON requestes.state=state.id
                    
                    
                    where  requestes.id ='.$id);
                    
                   // return $request_data;
                    
               
                        
                        $offer=Offers::where([['request_id',$id],['offers.state','accepted']])->first();
                        
                          $check=Rating::where([['user_id',$check_token['id']],['driver_id',$offer['driver_id']],['who_rate','user'],['request_id',$id]])->get();
                         
                          if( count($request_data)>0){
                          if(count($check)>0){
                               
                               $request_data[0]->rate_state='rated';
                          }else{
                              
                               $request_data[0]->rate_state=NULL;
                          }
                          }
                          $request_data[0]->accepted_time = \App\Offers::where([['state' , "accepted"], ['request_id' , $id]])->value('accepted_time');

    
              
            }else{
                    
            
                $request_data=DB::select('select requestes.id,(select offers.price from offers where offers.request_id=requestes.id AND offers.state="accepted" limit 1)as offer_price,requestes.shop_id,shopes.name,shopes.image as shope_image,(select offers.driver_id from offers where offers.request_id=requestes.id AND offers.state="accepted" limit 1)as user_id,
                (select users.first_name  from offers join users ON offers.driver_id=users.id where offers.request_id=requestes.id AND offers.state="accepted" limit 1)as first_name,(select users.image  from offers join users ON offers.driver_id=users.id where offers.request_id=requestes.id AND offers.state="accepted" limit 1)as image,(select users.phone  from offers join users ON offers.driver_id=users.id where offers.request_id=requestes.id AND offers.state="accepted" limit 1)as phone,
                requestes.receiving_address, requestes.receiving_longitude,requestes.receiving_latitude,requestes.delivery_longitude as client_longitude,requestes.delivery_latitude as client_latitude,requestes.delivery_address,requestes.description,requestes.delivery_time,state.name as state,requestes.created_at,
                ( 6371 * acos( cos( radians(requestes.delivery_latitude) ) * cos(radians(requestes.receiving_latitude) ) * cos( radians(requestes.receiving_longitude) - radians(requestes.delivery_longitude) ) 
             + sin( radians(requestes.delivery_latitude) ) * sin( radians(requestes.receiving_latitude)))) AS client_shope_distance,
             ( 6371 * acos( cos( radians(requestes.receiving_latitude) ) * cos( radians('.$lat.') ) * cos( radians('.$long.') - radians(requestes.receiving_longitude) ) 
             + sin( radians(requestes.receiving_latitude) ) * sin( radians('.$lat.')))) AS agent_shope_distance, requestes.payment_id, requestes.updated_at as deliveryTime
             FROM requestes
                    join shopes ON requestes.shop_id=shopes.id
                     join state ON requestes.state=state.id
                     
                    
                    where  requestes.id ='.$id);
                    
                   // return $request_data;
                    
                   $offer=Offers::where([['request_id',$id],['offers.state','accepted']])->first();
                        
                          $check=Rating::where([['user_id',$check_token['id']],['driver_id',$offer['driver_id']],['who_rate','user'],['request_id',$id]])->get();
                         
                          if( count($request_data)>0){
                          if(count($check)>0 ){
                               
                               $request_data[0]->rate_state='rated';
                          }else{
                              
                               $request_data[0]->rate_state=NULL;
                          }
                          }
                          $request_data[0]->accepted_time = \App\Offers::where([['state' , "accepted"], ['request_id' , $id]])->value('accepted_time');

    
          }
        
        
        
        
        
    }
    
          $details=request_details::select('request_details.id','product_type.name as type','shope_menu.product','request_details.price','request_details.amount')
                                  ->join('shope_menu','request_details.product_id','=','shope_menu.id')
                                  ->join('product_type','request_details.type','=','product_type.id')
                                  ->where('request_details.request_id',$id)->get();
            
              if( $request_data !=null){
                
                $bill = \App\Bill::where('request_id' ,$id )->value('is_bill');
    
                if( $bill != NULL){
                    $is_bill = $bill;
                }else{
                    $is_bill = "0";
                }
                $message['data']=$request_data;
                $message['details']=$details;
                $message['is_bill']= $is_bill;
                $message['error']=0;
                $message['message']='show  order data';
    
              }else{
                $message['data']=$request_data;
                $message['error']=1;
                $message['message']='no order data';
              }
          }else{
              $message['error']=3;
              $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


     public function show_userOf_order(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
         
         
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

                $id=$request->input('user_id');



        $user_data=DB::select('select users.id,users.first_name,users.image,users.phone,users.state,
 IF( (select IF(users.state=2,(SELECT  sum(rating.rate)/count(rating.user_id) 
                             from rating
 
                   where  rating.driver_id=users.id
                   AND who_rate="user"),(SELECT  sum(rating.rate)/count(rating.user_id) 
                             from rating
 
                   where  rating.user_id=users.id
                   AND who_rate="driver" ))) IS NULL,0,(select IF(users.state=2,(SELECT  sum(rating.rate)/count(rating.user_id) 
                             from rating
 
                   where  rating.driver_id=users.id
                   AND who_rate="user"),(SELECT  sum(rating.rate)/count(rating.user_id) 
                             from rating
 
                   where  rating.user_id=users.id
                   AND who_rate="driver" ))))as rate,
              ( 6371 * acos( cos( radians(users.latitude) ) * cos( radians('.$check_token['latitude'].') ) * cos( radians('.$check_token['longitude'].') - radians(users.longitude) ) 
         + sin( radians(users.latitude) ) * sin( radians('.$check_token['latitude'].')))) AS distance  
              
             from users 
             where users.id='.$id);
                
             

              $state=User::where('id',$id)->value('state');


        
          if( $user_data !=null){
              
                 if($state==2){
                     
                      $requests=Offers::where([['driver_id',$id],['state','accepted']])->count();
        
                   $rate=Rating::where([['driver_id',$id],['who_rate','user']])->count();
                  
                  }else{
                   $requests=Requests::where('user_id',$id)->count();
        
                   $rate=Rating::where([['user_id',$id],['who_rate','driver']])->count();
        
                  }
           $user_data[0]->requestes_count=$requests;
           $user_data[0]->user_comments=$rate;
            $message['data']=$user_data;
            $message['error']=0;
            $message['message']='show  user data';

          }else{
            $message['data']=$user_data;
            $message['error']=1;
            $message['message']='no user data';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


   public function shop_orders(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              
             
                $id=$request->input('shope_id');


        $request_data=DB::select('select requestes.id,requestes.shop_id,shopes.name,requestes.user_id,users.first_name,users.image,users.phone,requestes.description,requestes.delivery_time,state.name as state,requestes.created_at,myoffers.price  as delivery_cost,
        ( 6371 * acos( cos( radians(requestes.delivery_latitude) ) * cos( radians(shopes.latitude) ) * cos( radians( shopes.longitude) - radians(requestes.delivery_longitude) ) 
         + sin( radians(requestes.delivery_latitude) ) * sin( radians(shopes.latitude)))) AS distance FROM requestes
                join shopes ON requestes.shop_id=shopes.id
                join users ON requestes.user_id=users.id
                 join state ON requestes.state=state.id
                left join ( select offers.price,offers.request_id  from offers where offers.driver_id='.$check_token['id'].')as myoffers ON requestes.id=myoffers.request_id
                where  requestes.shop_id ='.$id.'  AND requestes.state=1');


                 

        
          if(count($request_data)  >0){


            $message['data']=$request_data;
            $message['error']=0;
            $message['message']=' shope orders';

          }else{
            $message['data']=$request_data;
            $message['error']=1;
            $message['message']='no  shope orders';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

    public function make_offer(Request $request)
    {
       try{
        
            $token=$request->input('user_token');
            // $long=$request->input('longitude');
            //  $lat=$request->input('latitude');
            $id=$request->input('request_id');
            $price=$request->input('price');
            $lang = $request->input('lang');
            
            if($lang == 'en' || $lang == 'EN'){
                $msg_1 = 'your offer price must be 15';
                $msg_2 = "your offer price must be 18 or more";
                $msg_3 = "your offer price must be 22 or more";
                $msg_4 = "you already made offer on it";
                $msg_5 = "offer is made successfully";
                $msg_6 = "error in make offer";
                $msg_7 = "token is not provided";
            }else{
                $msg_1 = "يجب أن يكون سعر العرض الخاص بك 15";
                $msg_2 = "يجب أن يكون سعر العرض 18 أو أكثر";
                $msg_3 = "يجب أن يكون سعر العرض الخاص بك 22 أو أكثر";
                $msg_4 = "لقد قدمت عرضًا عليه بالفعل";
                $msg_5 = "تم تقديم العرض بنجاح";
                $msg_6 = "خطأ في تقديم العرض";
                $msg_7 = "لم يتم توفير الرمز المميز";
            }
         
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

           
         
              $check_token=User::where('user_token',$token)->first();
              
              
              if($request->has('user_token') && $check_token !=NULL){
                          
                          
                 $select=DB::select('select users.id,( 6371 * acos( cos( radians(requestes.delivery_latitude) ) * cos(radians(requestes.receiving_latitude) ) * cos( radians(requestes.receiving_longitude) - radians(requestes.delivery_longitude) ) 
                                 + sin( radians(requestes.delivery_latitude) ) * sin( radians(requestes.receiving_latitude)))) AS distance from requestes 
                                 join users ON requestes.user_id=users.id
                                 where requestes.id='.$id );
                
                $distance = $select[0]->distance;
                
                $payment_id = \App\Requests::where('id' , $id)->value('payment_id');
                
                 if($select[0]->distance <= 1 ){
                     
                     if($price != 15){
                       $message['error']=5;
                       $message['message']= $msg_1;
                        return response()->json($message);
                        
                     }
                 }else{
                     if($payment_id == 1 &&  $price < 18){
                         $message['error'] = 5;
                         $message['message'] = $msg_2;
                         return response()->json($message);

                     }elseif($payment_id == 2 &&  $price < 22){
                         $message['error'] = 5;
                         $message['message'] = $msg_3;
                         return response()->json($message);

                     }
                 }
         
         
                          
                          
                          
                  $check=Offers::where([['request_id',$id],['driver_id',$check_token['id']]])->first();
                  
                  
                  if($check !=null){
                       $message['error']=4;
                       $message['message']= $msg_4;
                  }else{
                              
                          
            
                      $insert=new Offers;
                      $insert->request_id=$id;
                      $insert->price=$price;
                      $insert->driver_id=$check_token['id'];
                      $insert->state='wait';
                      $insert->created_at=$dateTime;
                      $insert->save();
            
                    
                      if($insert ==true){
            
                      /*  $update=User::where('id',$check_token['id'])->update([
                         'longitude'=>$long,
                         'latitude'=>$lat,
                         'updated_at'=>$dateTime
                        ]);*/
            
                      $user=Requests::where('id',$id)->value('user_id'); 
                      
                      
                     $state_data=Requests::select('state.name')->join('state','requestes.id','=','state.id')->where('requestes.id',$id)->first();

                          
                     try{       
                   //  $cc = 0;
                     
                    $title ='a new offer on your order ';
                    $body =$check_token['first_name'].' has made an offer on your order '.$id;
                    $title_ar =  "عرض جديد على طلبك";
                    $body_ar = $id." قدم عرضًا لطلبك "  . $check_token['first_name']  ;           
            
            
                   $get_user_token =User::select('firebase_token')->where('id', '=',$user)->first();
                   
                      // return $get_user_token[0]->firebase_token;
            
            
                                 ////////////////////////////////////////////////////////
                                 
                                   $msg = array
                                          (
                                    'body'  => $body,
                                    'title' => $title,
                                    'state'=>$state_data,
                                    "distance" => "$distance"          
                                          );
                                  $fields = array
                                      (
                                        'to'    => $get_user_token['firebase_token'],
                                        'notification'  => $msg
                                      );
                                  
                                                   
            
                                  $headers = array
                                      (
                                        'Authorization: key=' . API_ACCESS_KEY12,
                                        'Content-Type: application/json'
                                      );
                                #Send Reponse To FireBase Server  
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    //echo $result;
                                    curl_close( $ch );
                             
                    
                    
                        $save=new Notification;
                        $save->title=$title;
                        $save->body=$body;
                        $save->title_ar = $title_ar;
                        $save->body_ar = $body_ar;
                        $save->sender=$check_token['id'];
                        $save->is_read=0;
                         $save->request_id=$id;
                        $save->user_id=$user;
                        $save->created_at=$dateTime;
                        $save->save();
                        
                        
                     }catch(Exception $ex){
                          $message['error']=4;
                           $message['message']='error in send notification';
                          
                     }
            
            
            
                        
                        $message['error']=0;
                        $message['message']= $msg_5;
            
                      }else{
                        
                        $message['error']=1;
                        $message['message']=$msg_6;
                      }
                          }
                      }else{
                          $message['error']=3;
                        $message['message']= $msg_7; 
                      }
                       
                          
           
       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

    
    public function show_bill_byRequestID(Request $request){
        
        $token=$request->input('user_token');
        $request_id = $request->input('request_id');
        
        $check_token=User::where('user_token',$token)->first();
      
      
        if($request->has('user_token') && $check_token !=NULL){
          
        
            $get_data = \App\Bill::select('id', 'tax', 'order_price','delivery_price','total_price','image')
                                  ->where('request_id' , $request_id)->first();   
            
            $data = (object)[];
                                  
            if($get_data != NULL){
                $data = [
                    "id"             => (int)$get_data->id,
                    "tax"            => (int)$get_data->tax, 
                    "order_price"    => (int)$get_data->order_price,
                    "delivery_price" => (int)$get_data->delivery_price,
                    "total_price"    => (int)$get_data->total_price,
                    "image"          => $get_data->image,
                ];
                $message['data'] = $data;
                $message['error'] = 0;
                $message['message'] = "bill details";
            }else{
                $message['data'] = $data;
                $message['error'] = 1;
                $message['message'] = "no bill for that request";
            }
        }else{
             $message['error']=3;
             $message['message']='this token is not exist'; 
        }
        return response()->json($message);
    }    



    //send

    public function client_say_payment(Request $request){

      $token = $request->input('user_token');
      $request_id = $request->input('request_id');
      $driver_id = $request->input('driver_id');


        $updated_at = carbon::now()->toDateTimeString();
        $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

      $check_token=User::where('user_token',$token)->first();
      
      
        if($request->has('user_token') && $check_token !=NULL){

          $title ='Order payment online';
          $body = $check_token['first_name'].' pay the order: '.$request_id.' online '  ;
          $title_ar =  "الدفع عبر الإنترنت";
          $body_ar = "تم  دفع الاوردر بالفيزا"  . $check_token['first_name']  ;           
  
  
         $get_user_token =User::select('firebase_token')->where('id', '=',$check_token->id)->first();
         
                $msg = array
                      (
                'body'  => $body,
                'title' => $title,        
                      );
              $fields = array
                  (
                    'to'    => $get_user_token['firebase_token'],
                    'notification'  => $msg
                  );
              
                                

              $headers = array
                  (
                    'Authorization: key=' . API_ACCESS_KEY12,
                    'Content-Type: application/json'
                  );
            #Send Reponse To FireBase Server  
                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                curl_setopt( $ch,CURLOPT_POST, true );
                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                $result = curl_exec($ch );
                //echo $result;
                curl_close( $ch );
          
          
          
              $save=new Notification;
              $save->title=$title;
              $save->body=$body;
              $save->title_ar = $title_ar;
              $save->body_ar = $body_ar;
              $save->sender = $check_token->id;
              $save->is_read=0;
               $save->request_id = $request_id;
              $save->user_id=$driver_id;
              $save->created_at=$dateTime;
              $save->save();

              $add  = new \App\Messages;
              $add->sender_id = $check_token->id;
              $add->receiver_id = $driver_id;
              $add->message = "تم دفع الاوردر بالفيزا";
              $add->request_id = $request_id;
              $add->created_at = $dateTime;
              $add->save();   
              
              $message['error'] = 0;
              $message['message'] = "user pay online ";
        }else{
          $message['error']=3;
          $message['message']='this token is not exist'; 
        }
      return response()->json($message);

    }


}
