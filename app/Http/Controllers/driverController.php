<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use carbon\carbon;
use App\setting;
use App\Rating;
use App\Menu;
use App\Notification;
use Illuminate\Support\Facades\DB;
use App\Shopes;
use App\Product_type;
use App\shop_agent;
use App\shope_category;


class driverController extends Controller
{
    public $message=array();

          //for users
    public function show_driverprofile(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
          $id=$request->input('offer_id');
         
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

          


      $user_data=DB::select('select users.id,users.first_name,users.image,users.phone,requestes.delivery_time,offers.price,users.rate,(select count(rating.id) from rating where rating.driver_id=users.id AND who_rate="user")as rating_count,
      (select count(requestes.id) from requestes join offers on requestes.id=offers.request_id where offers.driver_id =users.id  AND  requestes.state="received" AND offers.state="accepted")as request_count,
        ( 6371 * acos( cos( radians(requestes.delivery_latitude) ) * cos( radians(users.latitude) ) * cos( radians( users.longitude) - radians(requestes.delivery_longitude)) 
         + sin( radians(requestes.delivery_latitude) ) * sin( radians(users.latitude)))) AS distance FROM users
                
          join offers ON users.id=offers.driver_id
          join requestes ON offers.request_id=requestes.id
          
           where offers.id ='.$id);
     
                
          if(count($user_data )>0){

            $message['data']=$user_data;   
            $message['error']=0;
            $message['message']='user data';

          }else{
            $message['data']=$user_data;
            $message['error']=1;
            $message['message']='no data for user ';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

    //for admin 
    public function show_driverbyid(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
          $id=$request->input('user_id');
         
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

                  
           $select=User::select('users.id','users.first_name','users.last_name','users.phone','users.image','users.email','driver_vehicles_details.front_vehical_image as car_image','driver_vehicles_details.card_number','driver_vehicles_details.card_image','driver_vehicles_details.driving_license_image', 'driver_vehicles_details.car_license_image', 'driver_vehicles_details.front_vehical_image','users.created_at')
            ->join('driver_vehicles_details','users.id','=','driver_vehicles_details.driver_id')
            ->join('vehicles','driver_vehicles_details.vehicle_id','=','vehicles.id')          
            ->where('users.id',$id)->first();

           
                  if($select !=null){

                    $message['data']=$select;
                    $message['error']=0;
                    $message['message']='user data';

                  }else{
                    $message['data']=$select;
                    $message['error']=1;
                    $message['message']='no data for user ';
                  }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


    public function show_nearshopes(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
    
          $check_token=User::where('user_token',$token)->first();
        
          if($request->has('user_token') && $check_token !=NULL){

            $lat=User::where('id',$check_token['id'])->value('latitude');
            $long=User::where('id',$check_token['id'])->value('longitude');


      $shop_data=DB::select('select shopes.id,shopes.name,shop_category.image,shopes.address,
         ( 6371 * acos( cos( radians('.$lat.') ) * cos( radians(latitude) ) * cos( radians( longitude) - radians('.$long.') ) 
         + sin( radians('.$lat.') ) * sin( radians(latitude)))) AS distance FROM shopes
         join shop_category ON shopes.cat_id=shop_category.id
         join shop_agent ON shopes.id=shop_agent.shop_id
          where shop_agent.agent_id !='.$check_token['id'].'
          having distance <=4 ORDER BY distance');
         
     
          if(count($shop_data )>0){

            $message['data']=$shop_data;
            $message['error']=0;
            $message['message']='shopes data';

          }else{
            $message['data']=$shop_data;
            $message['error']=1;
            $message['message']='no data ';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

 public function show_allnearshopes(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
           
          $check_token=User::where('user_token',$token)->first();
        
          if($request->has('user_token') && $check_token !=NULL){

            $lat=User::where('id',$check_token['id'])->value('latitude');
            $long=User::where('id',$check_token['id'])->value('longitude');
            
            
            $set=setting::where('user_id',$check_token['id'])->value('language');
            
            if($set==2){
                $cat=shope_category::select('id','E_name as name','image')->where('id','!=',4)->get();
            }else{
                $cat=shope_category::select('id','name','image')->where('id','!=',4)->get();
            }

//AND shopes.discount DESC
         if($request->has('name')){

            $shop_data=DB::select('select shopes.id,shopes.name,shopes.image,shopes.address,shop_category.name as category_name,shopes.discount,
                                     ( 6371 * acos( cos( radians('.$lat.') ) * cos( radians(latitude) ) * cos( radians( longitude) - radians('.$long.') ) 
                                     + sin( radians('.$lat.') ) * sin( radians(latitude)))) AS distance FROM shopes
                                     join shop_category ON shopes.cat_id=shop_category.id
                                      where shopes.name LIKE "%'.$request->name.'%" AND shopes.cat_id !=4 AND shopes.branch is NULL
                                      having distance <=10 ORDER BY distance , shopes.discount DESC');

         }elseif($request->has('category')){

           if($request->category =='all'){

            $shop_data=DB::select('select shopes.id,shopes.name,shopes.image,shopes.address,shop_category.name as category_name,shopes.discount,
                 ( 6371 * acos( cos( radians('.$lat.') ) * cos( radians(latitude) ) * cos( radians( longitude) - radians('.$long.') ) 
                 + sin( radians('.$lat.') ) * sin( radians(latitude)))) AS distance FROM shopes
                 join shop_category ON shopes.cat_id=shop_category.id
                 where shopes.cat_id !=4 AND shopes.branch is NULL
                  having distance <=15 ORDER BY distance,shopes.discount DESC');

           }else{

            $shop_data=DB::select('select shopes.id,shopes.name,shopes.image,shopes.address,shop_category.name as category_name,shopes.discount,
         ( 6371 * acos( cos( radians('.$lat.') ) * cos( radians(shopes.latitude) ) * cos( radians(shopes.longitude) - radians('.$long.') ) 
         + sin( radians('.$lat.') ) * sin( radians(shopes.latitude)))) AS distance FROM shopes
         join shop_category ON shopes.cat_id=shop_category.id
          where shopes.cat_id='.$request->category.' AND shopes.branch is NULL
          having distance <=15 ORDER BY distance ASC,shopes.discount DESC');
           }

         

         }else{

           $shop_data=DB::select('select shopes.id,shopes.name,shopes.image,shopes.address,shop_category.name as category_name,shopes.discount,
         ( 6371 * acos( cos( radians('.$lat.') ) * cos( radians(shopes.latitude) ) * cos( radians( shopes.longitude) - radians('.$long.') ) 
         + sin( radians('.$lat.') ) * sin( radians(shopes.latitude)))) AS distance FROM shopes
         join shop_category ON shopes.cat_id=shop_category.id
         where shopes.cat_id !=4 AND shopes.branch is NULL
          having distance <=15 ORDER BY distance ASC,shopes.discount DESC');
          }
         
     
          if(count($shop_data )>0){

            $message['data']=$shop_data;
            $message['category']=$cat;
            $message['error']=0;
            $message['message']='shopes data';

          }else{
            $message['data']=$shop_data;
             $message['category']=$cat;
            $message['error']=1;
            $message['message']='no data ';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


     public function show_shopbyid(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
          $id=$request->input('shop_id');
            $nowdate= date('l');
         
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
                  
         $select=Shopes::select('shopes.id','shopes.name','longitude','latitude','description')
          ->where('id',$id)->first();

          $menu=Menu::where('shop_id',$id)->get();

$me_agent=0;
          $check_aget=shop_agent::where([['shop_id',$id],['agent_id',$check_token['id']]])->first();
        $agent_num=shop_agent::where('shop_id',$id)->count();
        
        $branches=Shopes::select('shopes.id','shopes.name','longitude','latitude')
          ->where('branch',$id)->get();
          
          if($check_aget !=null){
              
              $me_agent=1;
              
          }else{
              $me_agent=0;
          }
    
          if($select !=null){
            
            
            $select['agent_num']=$agent_num;
            $message['data']=$select;
            $message['menu']=$menu; 
            $message['branches']=$branches;
            $message['i_agent']=$me_agent;
            $message['error']=0;
            $message['message']='shope data';

          }else{
            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data ';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

    public function show_product_type(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
          $id=$request->input('product_id');
       
         
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
                  
         $select=Product_type::select('product_type.id','product_type.name','price')
          ->where('product_id',$id)->get();

        
    
          if(count($select )>0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='shope data';

          }else{
            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data ';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }



}
