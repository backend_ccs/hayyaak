<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\contact_image;
use App\User;
use App\shope_contact;
use carbon\carbon;
use App\Roles;


class contactController extends Controller
{
     public $message=array();
     
     
     public function show_contactDetails(Request  $request){
         
          try{
           $token=$request->input('user_token');
           $id=$request->input('id');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=shope_contact::select('id','owner_name', 'employee_name', 'owner_phone', 'employee_phone', 'image','image1','image2','image3','duration', 'state', 'notes')
             
          
               ->where('shope_contract.shope_id',$id)->get();
               
               
             //  $images=contact_image::select('id','image')->where('contact_id',$select[0]->id)->get();

          
       if(count($select) > 0){
            $message['data']=$select;
          //  $message['images']=$images;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
         
         
         
         
         
         
     }
     
     
      public function show_onecontact(Request  $request){
         
          try{
           $token=$request->input('user_token');
           $id=$request->input('id');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


         $select=shope_contact::select('id','owner_name', 'employee_name', 'owner_phone', 'employee_phone', 'image','image1','image2','image3','duration', 'state', 'notes')
             
          
               ->where('shope_contract.shope_id',$id)->first();
               
               
              

          
       if(count($select) > 0){
            $message['data']=$select;
            
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$select;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
         
         
         
         
         
         
     }
     
     
      public function add_contact(Request  $request){
         
          try{
           $token=$request->input('user_token');
           $id=$request->input('shope_id');
           $owner_name=$request->input('owner_name');
           $employee_name=$request->input('employee_name');
           $owner_phone=$request->input('owner_phone'); 
           $employee_phone=$request->input('employee_phone');
           $image=$request->file('image');
              $image1=$request->file('image1');
                 $image2=$request->file('image2');
                    $image3=$request->file('image3');
           $duration=$request->input('duration');
         
           $state=$request->input('state');
           $notes=$request->input('notes');
          
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              
                $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
                       if(isset($image)) {
        		                    $new_name = $image->getClientOriginalName();
        		                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
        		                    $destinationPath_id = 'uploads/contract';
        		                    $image->move($destinationPath_id, $savedFileName);
        		        
        		                    $images = $savedFileName;
        		                  
        		           }else{
        		              $images =NULL;       
        		          }
     
                     
                        if(isset($image1)) {
        		                    $new_name = $image1->getClientOriginalName();
        		                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
        		                    $destinationPath_id = 'uploads/contract';
        		                    $image1->move($destinationPath_id, $savedFileName);
        		        
        		                    $images1 = $savedFileName;
        		                  
        		           }else{
        		              $images1 =NULL;       
        		          }
        		          
        		            if(isset($image2)) {
        		                    $new_name = $image2->getClientOriginalName();
        		                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
        		                    $destinationPath_id = 'uploads/contract';
        		                    $image2->move($destinationPath_id, $savedFileName);
        		        
        		                    $images2 = $savedFileName;
        		                  
        		           }else{
        		              $images2 =NULL;       
        		          }
        		          
        		             if(isset($image3)) {
        		                    $new_name = $image3->getClientOriginalName();
        		                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
        		                    $destinationPath_id = 'uploads/contract';
        		                    $image3->move($destinationPath_id, $savedFileName);
        		        
        		                    $images3= $savedFileName;
        		                  
        		           }else{
        		              $images3 =NULL;       
        		          }
                   
                   
                   
              $check=shope_contact::where('shope_id',$id)->first();
              
              
               if($check !=null){
                     $message['error']=4;
                     $message['message']='this shope have a contract';
               }else{
              
              $insert=new shope_contact;
              $insert->shope_id=$id;
              $insert->owner_name=$owner_name;
              $insert->employee_name=$employee_name;
              $insert->owner_phone=$owner_phone;
              $insert->employee_phone=$employee_phone;
              $insert->image=$images;
               $insert->image1=$images1;
                $insert->image2=$images2;
                 $insert->image3=$images3;
              $insert->duration=$duration;
              $insert->state=$state;
              $insert->notes=$notes;
              $insert->created_at=$dateTime;
              $insert->save();
              
              
              


               
              

          $my_arr=array();
       if($insert ==true){
           
           if(isset($contact_image)){
               
               
                 foreach ($contact_images as $key ) {
          
                 $my_arr[]=array('contact_id'=>$insert->id,'image'=>$key,'created_at'=>$dateTime);
                 }
                 
                   $insert_image=contact_image::insert($my_arr);
           }
           
           
         
            
            
            $message['error']=0;
            $message['message']='add data success';
       }else{

            
            $message['error']=1;
            $message['message']='error in add data';
       }
               }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
         
         
         
         
         
         
     }
     
       public function update_contact(Request  $request){
         
          try{
           $token=$request->input('user_token');
           $id=$request->input('id');
           $owner_name=$request->input('owner_name');
           $employee_name=$request->input('employee_name');
           $owner_phone=$request->input('owner_phone'); 
           $employee_phone=$request->input('employee_phone');
           $image=$request->file('image');
           $duration=$request->input('duration');
           $image1=$request->file('image1');
         $image2=$request->file('image2');
          $image3=$request->file('image3');
           $state=$request->input('state');
           $notes=$request->input('notes');
           
           
           
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              
                $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
                       if(isset($image)) {
        		                    $new_name = $image->getClientOriginalName();
        		                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
        		                    $destinationPath_id = 'uploads/users';
        		                    $image->move($destinationPath_id, $savedFileName);
        		        
        		                    $images = $savedFileName;
        		                  
        		           }else{
        		              $images =shope_contact::where('id',$id)->value('image');       
        		          }
     
                     
                      
                                           if(isset($image1)) {
        		                    $new_name = $image1->getClientOriginalName();
        		                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
        		                    $destinationPath_id = 'uploads/contract';
        		                    $image1->move($destinationPath_id, $savedFileName);
        		        
        		                    $images1 = $savedFileName;
        		                  
        		           }else{
        		              $images1 =shope_contact::where('id',$id)->value('image1');       
        		          }
        		          
        		            if(isset($image2)) {
        		                    $new_name = $image2->getClientOriginalName();
        		                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
        		                    $destinationPath_id = 'uploads/contract';
        		                    $image2->move($destinationPath_id, $savedFileName);
        		        
        		                    $images2 = $savedFileName;
        		                  
        		           }else{
        		              $images2 =shope_contact::where('id',$id)->value('image2');       
        		          }
        		          
        		             if(isset($image3)) {
        		                    $new_name = $image3->getClientOriginalName();
        		                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
        		                    $destinationPath_id = 'uploads/contract';
        		                    $image3->move($destinationPath_id, $savedFileName);
        		        
        		                    $images3= $savedFileName;
        		                  
        		           }else{
        		              $images3 =shope_contact::where('id',$id)->value('image3');       
        		          }
              
              
              $update=shope_contact::where('id',$id)->update([
               
                'owner_name'=>$owner_name,
                 'employee_name'=>$employee_name,
                'owner_phone'=>$owner_phone,
                'employee_phone'=>$employee_phone,
               'image'=>$images,
               'image1'=>$images1,
               'image2'=>$images2,
               'image3'=>$images3,
                'duration'=>$duration,
               'state'=>$state,
               'notes'=>$notes,
               'updated_at'=>$dateTime
              
              ]);
              
             
               
              

          
       if($update ==true){
           
        
            
            
            $message['error']=0;
            $message['message']='updtae data success';
       }else{

            
            $message['error']=1;
            $message['message']='error in update data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
         
         
         
         
         
         
     }
     
     
     
    public function show_allroles(Request $request)
    {
      try{

         
    
          $token=$request->input('user_token');
          
          
     $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              
       $userexist=Roles::select('id','role')->whereIn('id',[1,11,12,13,14])->get();

              if(count($userexist )>0){
                  
                   $message['data']=$userexist;
                   $message['error']=0;
                   $message['message']='show data ';

             }else{
                    $message['data']=$userexist;
                    $message['error']=1;
                     $message['message']='no data';
             }
             
      }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }
            
            }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
            }
      
           
            return response()->json($message);
     } 
     
    public function show_alladmin(Request $request)
     {
      try{

         
    
          $token=$request->input('user_token');
          
          
     $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              
       $userexist=User::select('users.id','users.first_name','users.last_name','users.phone','users.password as passwords','users.email','users.image','users.state as state','roles.role as state_name','users.created_at')
       ->Join('roles','users.state','=','roles.id')
       ->whereIn('users.state',[1,11,12,13,14])->where('users.id','!=',1)->get();

              if(count($userexist )>0){
                  
                   $message['data']=$userexist;
                   $message['error']=0;
                   $message['message']='show data of user';

             }else{
                    $message['data']=$userexist;
                    $message['error']=1;
                     $message['message']='no data';
             }
             
      }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }
            
            }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
            }
      
           
            return response()->json($message);
     } 
     
   public function add_admindata(Request $request)
   {
        try{
            
        $token=$request->input('user_token');
        
        $fname=$request->input('first_name');
        $lname=$request->input('last_name');
        $image=$request->file('image');
        $pass=$request->input('passwords');
        $phone=$request->input('phone');
        $email=$request->input('email');

         $state=$request->input('state');

          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

  
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

                if(isset($image)) {
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                    $destinationPath_id = 'uploads/users';
                    $image->move($destinationPath_id, $savedFileName);
        
                    $images = $savedFileName;
                }else{
                     $images=NULL;      
                }

                 $insert =new User;
                 $insert->first_name=$fname;
                 $insert->last_name=$lname;
                 $insert->phone=$phone;
                 $insert->email=$email;
                 $insert->password=$pass;
                 $insert->image=$images;
                 $insert->state=$state;
                 $insert->created_at=$dateTime;
                 $insert->save();
       
     
                 
                 if($insert ==true){
                               $message['error']=0;
                               $message['message']='add data of user';
                     
                 }else{
                               $message['error']=1;
                               $message['message']='error in add data of user'; 
                 }
    }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

      }catch(Exception $ex){
         
         $message['error']=2;
         $message['message']='error'.$ex->getMessage();
      }
          return response()->json($message);

      }
      
      
      
  public function update_admin(Request $request)
 {
        try{
              $token=$request->input('user_token');
            $id=$request->input('id');
        $fname=$request->input('first_name');
        $lname=$request->input('last_name');
        $image=$request->file('image');
        $pass=$request->input('passwords');
        $phone=$request->input('phone');
        $email=$request->input('email');
       
         $state=$request->input('state');

          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));


     $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
                if(isset($image)) {
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                    $destinationPath_id = 'uploads/users';
                    $image->move($destinationPath_id, $savedFileName);
        
                    $images = $savedFileName;
                }else{
                     $images=User::where('id',$id)->value('image');      
                }
                
               

     $update=User::where('id',$id)->update(['first_name'=>$fname,'last_name'=>$lname,'phone'=>$phone,'email'=>$email,'password'=>$pass,'image'=>$images,'state'=>$state,'updated_at'=>$dateTime]);
       
      
     
     if($update ==true){
                   
                   $message['error']=0;
                   $message['message']='update data of user';
         
     }else{
                  
                   $message['error']=1;
                   $message['message']='error in update data of user'; 
     }
     
     
          
    }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

      }catch(Exception $ex)
      {
         
         $message['error']=2;
         $message['message']='error'.$ex->getMessage();
      }
  return response()->json($message);

      }
         
      




      public function show_adminbyid(Request $request)
     {
      try{
         $id=$request->input('user_id');
         
         
    
          $token=$request->input('user_token');
          
          
     $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              
       $userexist=User::select('users.id','users.first_name','users.last_name','users.phone','users.password as passwords','users.email','users.image','users.state as state','roles.role as state_name')
       ->Join('roles','users.state','=','roles.id')
       ->where('users.id',$id)->first();

              if($userexist !=null){
                  
                   $message['data']=$userexist;
                   $message['error']=0;
                   $message['message']='show data of user';

             }else{
                    $message['data']=NULL;
                    $message['error']=1;
                     $message['message']='no data';
             }
             
      }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }
            
            }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
            }
      
           
            return response()->json($message);
     } 
     
     
      public function delete_admin(Request $request)
     {
      try{
         $id=$request->input('user_id');
         
         
    
          $token=$request->input('user_token');
          
          
     $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              
       $delete=User::where('id',$id)->delete();

              if($delete ==true){
                  
                   $message['error']=0;
                   $message['message']='delete data of user';

             }else{
                    $message['error']=1;
                     $message['message']='error in delete data';
             }
             
      }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }
            
            }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
            }
      
           
            return response()->json($message);
     } 
}
