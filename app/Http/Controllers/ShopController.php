<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Shopes;
use App\Messages;
use App\Offers;
use App\shop_agent;
use App\Bill;
use App\Notification;
use App\Requests;
use App\About;
use carbon\carbon;
use App\City_needService;
use App\shope_category;
use App\Rating;
                
define( 'API_ACCESS_KEY12','AAAAnSDEgLc:APA91bFisJ6mf6QmnpUvaC48ND3u4u_ULaQNnR4fxCRur392hErem3qi3fjRQ05wXg45D8NrhehK4Bp8h1f_uUGW0YbSQBmxDRWg3pi9PBkHvYnE8GHqjYs3JRTmCab08jUJQph9luxS');

class ShopController extends Controller
{
    public $message=array();
     //for admin
     
     
     
     
    public function add_shope(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              $name=$request->input('name');
              $description=$request->input('description');
               $discount=$request->input('discount');
              $image=$request->file('image'); 
              $cat=$request->input('category_id');
              $address=$request->input('address');  

              $longitude=$request->input('longitude');  
              $latitude=$request->input('latitude');
                
                if( $discount == NULL || $discount == ""){
                    $discount = "0";
                }

             if(isset($image)) {
                  $new_name = $image->getClientOriginalName();
                  $savedFileName = rand(100000,999999).time()."_".$new_name; 
                    // give a unique name to file to    be saved
                        $destinationPath_id = 'uploads/shopes';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
                 }else{
                    $images =NULL;     
                 }



                  $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
          	 
             $insert=new Shopes;
             $insert->name=$name;
             $insert->image=$images;
             $insert->description=$description;
             $insert->discount=$discount;
             $insert->cat_id=$cat;
             $insert->address=$address;
             $insert->longitude=$longitude;
             $insert->latitude=$latitude;
             $insert->created_at=$dateTime;
             $insert->save();
    
        
          if($insert ==true){
         
            $message['error']=0;
            $message['message']='add shope data';

          }else{
           
            $message['error']=1;
            $message['message']='error in insert shope data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


    public function update_shope(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              $id=$request->input('shope_id');
              $name=$request->input('name');
              $description=$request->input('description');
             $discount=$request->input('discount');

              $cat=$request->input('category_id');
              $address=$request->input('address');  
              $image=$request->file('image');
              $longitude=$request->input('longitude');  
              $latitude=$request->input('latitude');

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

                  if(isset($image)) {
                  $new_name = $image->getClientOriginalName();
                  $savedFileName = rand(100000,999999).time()."_".$new_name; 
                    // give a unique name to file to    be saved
                        $destinationPath_id = 'uploads/shopes';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
                 }else{
                    $images =Shopes::where('id',$id)->value('image');     
                 }
  
             
             $update=Shopes::where('id',$id)->update([
                'name'=>$name,
                'image'=>$images,
                'description'=>$description,
                'cat_id'=>$cat,
                'discount'=>$discount,
                'address'=>$address,
                'longitude'=>$longitude,
                'latitude'=>$latitude,
                'updated_at'=>$dateTime


             ]);
            
             
    
        
          if($update ==true){
         
            $message['error']=0;
            $message['message']='update shope data';

          }else{
           
            $message['error']=1;
            $message['message']='error in update shope data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

    
    
    public function show_shopes(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
            $id=$request->input('id');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $select=Shopes::where([['cat_id','!=',4],['branch','=',NULL]])->get();
             
             if($request->has('id')){
                 
                 $select=Shopes::where('id',$id)->get();
             }
          
  
          if(count($select) >0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show shope data';

          }else{
             $message['data']=$select;
            $message['error']=1;
            $message['message']='no shope data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
     public function show_category(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $select=shope_category::where('id','!=',4)->get();
          
  
          if(count($select) >0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show  data';

          }else{
             $message['data']=$select;
            $message['error']=1;
            $message['message']='no  data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
 public function show_categorybyid(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
             $id=$request->input('id');     
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $select=shope_category::where('id',$id)->first();
          
  
          if($select !=null){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show  data';

          }else{
             $message['data']=$select;
            $message['error']=1;
            $message['message']='no  data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

   public function add_category(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              $name=$request->input('name');
              $e_name=$request->input('E_name');

              $image=$request->file('image');

      


             if(isset($image)) {
                  $new_name = $image->getClientOriginalName();
                  $savedFileName = rand(100000,999999).time()."_".$new_name; 
                    // give a unique name to file to    be saved
                        $destinationPath_id = 'uploads/shope_category';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
                 }else{
                    $images =NULL;     
                 }



                  $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
          	 
             $insert=new shope_category;
             $insert->name=$name;
             $insert->E_name=$e_name;
             $insert->image=$images;
             $insert->created_at=$dateTime;
             $insert->save();
    
        
          if($insert ==true){
         
            $message['error']=0;
            $message['message']='add category data';

          }else{
           
            $message['error']=1;
            $message['message']='error in insert category data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


    public function update_category(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              $id=$request->input('id');
              $name=$request->input('name');
              $e_name=$request->input('E_name');
       
              $image=$request->file('image');
         


                  $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

                  if(isset($image)) {
                  $new_name = $image->getClientOriginalName();
                  $savedFileName = rand(100000,999999).time()."_".$new_name; 
                    // give a unique name to file to    be saved
                        $destinationPath_id = 'uploads/shope_category';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
                 }else{
                    $images =shope_category::where('id',$id)->value('image');     
                 }
  
             
             $update=shope_category::where('id',$id)->update([
                'name'=>$name,
                'image'=>$images,
                'E_name'=>$e_name,
                'updated_at'=>$dateTime


             ]);
            
             
    
        
          if($update ==true){
         
            $message['error']=0;
            $message['message']='update category data';

          }else{
           
            $message['error']=1;
            $message['message']='error in update category data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


 public function delete_category(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
          $id=$request->input('id');
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $select=shope_category::where('id',$id)->delete();
          
  
          if($select ==true){

          
            $message['error']=0;
            $message['message']='delete  data';

          }else{
           
            $message['error']=1;
            $message['message']='error in delete  data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
   
   

    public function show_oneshope(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
          $id=$request->input('shope_id');
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $select=Shopes::select('shopes.id','shopes.name','shopes.image','shopes.description','shopes.cat_id as category_id','shop_category.name as category_name','shopes.address','shopes.longitude','shopes.latitude','shopes.discount','shopes.created_at','shopes.updated_at')
             ->join('shop_category','shopes.cat_id','=','shop_category.id')
             ->where('shopes.id',$id)->first();
          
  
          if($select !=null){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show shope data';

          }else{
             $message['data']=$select;
            $message['error']=1;
            $message['message']='no shope data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
  
    public function delete_shop(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
          $id=$request->input('shope_id');
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $select=Shopes::where('id',$id)->delete();
          
  
          if($select ==true){

          
            $message['error']=0;
            $message['message']='delete shope data';

          }else{
           
            $message['error']=1;
            $message['message']='error in delete shope data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


    public function add_me_shopAgent(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              $id=$request->input('shope_id');
          


              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
            $insert=new shop_agent;
            $insert->agent_id=$check_token['id'];
            $insert->shop_id=$id;
            $insert->created_at=$dateTime;
            $insert->save();
            
    
        
          if($insert ==true){
         
            $message['error']=0;
            $message['message']='add me agent';

          }else{
           
            $message['error']=1;
            $message['message']='error in insert shope data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

         

    public function make_bill(Request $request)
    {
       try{
        
          $token  = $request->input('user_token');
          $id  = $request->input('request_id');
          $order_price = $request->input('request_price');
          $total = $request->input('total_price');
          $image = $request->file('image');
       

                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              
              
               if(isset($image)) {
                  $new_name = $image->getClientOriginalName();
                  $savedFileName = rand(100000,999999).time()."_".$new_name; 
                    // give a unique name to file to    be saved
                        $destinationPath_id = 'uploads/bill';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
                 }else{
                    $images =NULL;     
                 }
           


                  $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
            $request_user=Requests::where('id',$id)->value('user_id');
             
            $offer_data=Offers::where([['request_id',$id],['driver_id',$check_token['id']]])->first();
             
            $check=Bill::where([['user_id',$request_user],['driver_id',$check_token['id']],['request_id',$id]])->first();
            
            $request_data=Requests::where('id',$id)->first();
            
            
            if($check !=null){
                
           
                
                
                  $user_tax=About::where('id',1)->value('user_tax');
               
                $tax=str_replace('%', '', $user_tax);
                 
                //$tax=round(($tax/$order_price)*100,2);
                
                if($request_data['copon'] !=NULL && $request_data['total_price']==0){
                      $action=Bill::where([['user_id',$request_user],['driver_id',$check_token['id']],['request_id',$id]])
                         ->update([
                           'order_price'=>0,
                           'tax'=>$tax,
                           'total_price'=>$check['delivery_price']+$tax,
                           'image'=>$images,
                           'is_bill' => 1,
                           'updated_at'=>$dateTime
            
            
                           ]);
                    
                    
                }else{
                    
                      $action=Bill::where([['user_id',$request_user],['driver_id',$check_token['id']],['request_id',$id]])
                         ->update([
                           'order_price'=>$order_price,
                           'tax'=>$tax,
                           'total_price'=>$check['delivery_price']+$tax+$order_price,
                           'image'=>$images,
                           'is_bill' => 1,
                           'updated_at'=>$dateTime
            
            
                           ]);
                    
                }
                
              
               
               
               
               
               
                            $mess=new Messages;
                         $mess->receiver_id=$request_user;
                          $mess->sender_id=$check_token['id'];
                          $mess->request_id=$id;
                          $mess->message= ' لقد تم اصدار فاتوره
سعر الاوردر :'.$order_price.'  '.PHP_EOL.'وسيتم خصم ضريبه :'.$tax.'ر.س'.PHP_EOL.',وسعر التكلفه :'.$offer_data['price'];
                          
                          //"order price : ".$order_price.PHP_EOL." tax price : ".$tax.'ر.س'.PHP_EOL." delivery price : ".$offer_data['price'];
                           
                         
                       
                          $mess->created_at=$dateTime;
                          $mess->save();
                          
                          
                              if(isset($image)) {
                                  
                                  
                                    $new_name = $image->getClientOriginalName();
                                    $savedFile = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                    $destinationPath ="uploads/messages";
                                    
                                 
                                    copy($destinationPath_id.'/'.$savedFileName, $destinationPath.'/'.$savedFile);
                        
                                    $images = $savedFile;
                      
                              
                                  $mess=new Messages;
                                  $mess->receiver_id=$request_user;
                                  $mess->sender_id=$check_token['id'];
                                  $mess->request_id=$id;
                                  $mess->image=$images;
                               
                                  $mess->created_at=$dateTime;
                                  $mess->save();
                                  
                          }
                          
                          $update=Requests::where('id',$id)->update([
                                         'state'=>5,
                                         'updated_at'=>$dateTime
                            
                                       ]);
                                                       
      
            try{       
                   //  $cc = 0;
                     
                
                $title ='the agent  '.$check_token['first_name'].' edit the bill';
                $body =$check_token['first_name'].' edit the bill of  your order '.$id;
                $title_ar = " عدل الفاتوره ".$check_token['first_name']." المندوب ";
                $body_ar =  $id." قام بتعديل فاتورة طلبك ".$check_token['first_name'];
                     
            
            
                   $get_user_token =User::select('firebase_token')->where('id', '=',$request_user)->first();
                   
                      // return $get_user_token[0]->firebase_token;
            
            
                                 ////////////////////////////////////////////////////////
                                 
                                   $msg = array
                                          (
                                    'body'  => $body,
                                    'title' => $title,
                                              
                                          );
                                  $fields = array
                                      (
                                        'to'    => $get_user_token['firebase_token'],
                                        'notification'  => $msg
                                      );
                                  
                                                   
            
                                  $headers = array
                                      (
                                        'Authorization: key=' . API_ACCESS_KEY12,
                                        'Content-Type: application/json'
                                      );
                                #Send Reponse To FireBase Server  
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    //echo $result;
                                    curl_close( $ch );
                             
                    
                    
                        $save=new Notification;
                        $save->title=$title;
                        $save->body=$body;
                        $save->title_ar = $title_ar;
                        $save->body_ar = $body_ar;
                        $save->sender=$check_token['id'];
                        $save->is_read=0;
                         $save->request_id=$id;
                        $save->user_id=$request_user;
                        $save->created_at=$dateTime;
                        $save->save();
                        
                        
                     }catch(Exception $ex){
                          $message['error']=4;
                           $message['message']='error in send notification';
                          
                     }
            }else{
                $user_tax=About::where('id',1)->value('user_tax');
                $driver_tax=About::where('id',1)->value('driver_tax');

                $tax=str_replace('%', '', $user_tax);
                $tax_driver=str_replace('%', '', $driver_tax);

               // $tax=round(($tax/$order_price)*100,2);
                
              if($request_data['copon'] !=NULL  && $request_data['total_price']==0){

                     $action=new Bill;
                     $action->user_id=$request_user;
                     $action->driver_id=$check_token['id'];
                     $action->request_id=$id;
                     $action->order_price=0;
                     $action->delivery_price=$offer_data['price'];
                     $action->tax=$tax;
                     $action->image=$images;
                     $action->total_price=$offer_data['price']+$tax+$request_data['total_price'];
                     $action->is_bill = 1;
                     $action->created_at=$dateTime;
                     $action->save();
                     
                    $driverWallet = \App\Wallet::where('user_id' , $check_token['id'])->value('credit');
                    
                    $get_money = ($offer_data['price'] * $tax_driver) / 100;
                    
                    $totalDriverMoney = $driverWallet - $get_money;
                    
                    $update_wallet = \App\Wallet::where('user_id' , $check_token['id'])->update(['credit' => $totalDriverMoney]);
                    
              }else{
                  $action=new Bill;
                     $action->user_id=$request_user;
                     $action->driver_id=$check_token['id'];
                     $action->request_id=$id;
                     $action->order_price=$order_price;
                     $action->delivery_price=$offer_data['price'];
                     $action->tax=$tax;
                     $action->image=$images;
                     $action->total_price=$offer_data['price']+$tax+$request_data['total_price'];
                     $action->is_bill = 1;
                     $action->created_at=$dateTime;
                     $action->save();
                     
                    $driverWallet = \App\Wallet::where('user_id' , $check_token['id'])->value('credit');
                    
                    $get_money = ($offer_data['price'] * $tax_driver) / 100;
                    
                    $totalDriverMoney = $driverWallet - $get_money;
                    
                    $update_wallet = \App\Wallet::where('user_id' , $check_token['id'])->update(['credit' => $totalDriverMoney]);
              }
                     
                     
                     
                          $mess=new Messages;
                          $mess->receiver_id=$request_user;
                          $mess->sender_id=$check_token['id'];
                          $mess->request_id=$id;
                          $mess->message=' لقد تم اصدار فاتوره
سعر الاوردر :'.$order_price.'  '.PHP_EOL.'وسيتم خصم ضريبه :'.$tax.'ر.س'.PHP_EOL.',وسعر التكلفه :'.$offer_data['price'];
                           ;
                       
                          $mess->created_at=$dateTime;
                          $mess->save();
                          
                         if(isset($image)) {
                                  
                                  
                                    $new_name = $image->getClientOriginalName();
                                    $savedFile = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                    $destinationPath ="uploads/messages";
                                    
                                 
                                    copy($destinationPath_id.'/'.$savedFileName, $destinationPath.'/'.$savedFile);
                        
                                    $images = $savedFile;
                                    
                              $mess=new Messages;
                              $mess->receiver_id=$request_user;
                              $mess->sender_id=$check_token['id'];
                              $mess->request_id=$id;
                              $mess->image=$images;
                           
                              $mess->created_at=$dateTime;
                              $mess->save();
                                  
                          }
                          
                          
                             $update=Requests::where('id',$id)->update([
                                         'state'=>5,
                                         'updated_at'=>$dateTime
                            
                                       ]);
                          
                                          try{       
                   //  $cc = 0;
                     
                
                $title ='the agent '.$check_token['first_name'].' edit the bill';
                $body =$check_token['first_name'].'edit the bill of  your order '.$id;
                $title_ar = " عدل الفاتوره ".$check_token['first_name']." المندوب ";
                $body_ar =  $id." قام بتعديل فاتورة طلبك ".$check_token['first_name'];
                
                     
            
            
                   $get_user_token =User::select('firebase_token')->where('id', '=',$request_user)->first();
                   
                      // return $get_user_token[0]->firebase_token;
            
            
                                 ////////////////////////////////////////////////////////
                                 
                                   $msg = array
                                          (
                                    'body'  => $body,
                                    'title' => $title,
                                              
                                          );
                                  $fields = array
                                      (
                                        'to'    => $get_user_token['firebase_token'],
                                        'notification'  => $msg
                                      );
                                  
                                                   
            
                                  $headers = array
                                      (
                                        'Authorization: key=' . API_ACCESS_KEY12,
                                        'Content-Type: application/json'
                                      );
                                #Send Reponse To FireBase Server  
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    //echo $result;
                                    curl_close( $ch );
                             
                    
                    
                        $save=new Notification;
                        $save->title=$title;
                        $save->body=$body;
                        $save->title_ar = $title_ar;
                        $save->body_ar = $body_ar;
                        $save->sender=$check_token['id'];
                        $save->is_read=0;
                         $save->request_id=$id;
                        $save->user_id=$request_user;
                        $save->created_at=$dateTime;
                        $save->save();
                        
                        
                     }catch(Exception $ex){
                          $message['error']=4;
                           $message['message']='error in send notification';
                          
                     }
                
                
            }

              
              
              
            $driverWallet = \App\Wallet::where('user_id' , $check_token['id'])->value('credit');
            $get_user_token =User::select('firebase_token')->where('id', '=',$check_token['id'])->first();

                
            if($driverWallet == 0){
        
                       $msg = array
                              (
                        'body'  => "ممتاز يابطل 🤩 تم توثيق حسابك بنجاح.. شد حيلك.. 🔅 حياك ماراح يقصر معاك ",
                        'title' => "أشعارات حياك",
                                  
                              );
                      $fields = array
                          (
                            'to'    => $get_user_token['firebase_token'],
                            'notification'  => $msg
                          );
                      
                                       

                      $headers = array
                          (
                            'Authorization: key=' . API_ACCESS_KEY12,
                            'Content-Type: application/json'
                          );
                    #Send Reponse To FireBase Server  
                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        $result = curl_exec($ch );
                        //echo $result;
                        curl_close( $ch );
                 
                    
                    
                        $save=new Notification;
                        $save->title= "Hayyaak Notification";
                        $save->body= "Excellent, hero 🤩 Your account has been successfully verified .. Tighten your tricks .. Hayyak will not shorten with you";
                        $save->title_ar = "أشعارات حياك";
                        $save->body_ar = "ممتاز يابطل 🤩 تم توثيق حسابك بنجاح.. شد حيلك.. 🔅 حياك ماراح يقصر معاك ";
                        $save->sender= 1;
                        $save->is_read=0;
                         $save->request_id=$id;
                        $save->user_id=$check_token['id'];
                        $save->created_at=$dateTime;
                        $save->save();
                        
            }elseif($driverWallet == -50){
             
                
                $msg = array
                              (
                        'body'  => "يعطيك العافية 🙂 يابطل اتمنى السداد مستحقات في اقرب وقت ممكن .. وشكرا ",
                        'title' => "أشعارات حياك",
                                  
                              );
                      $fields = array
                          (
                            'to'    => $get_user_token['firebase_token'],
                            'notification'  => $msg
                          );
                      
                                       

                      $headers = array
                          (
                            'Authorization: key=' . API_ACCESS_KEY12,
                            'Content-Type: application/json'
                          );
                    #Send Reponse To FireBase Server  
                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        $result = curl_exec($ch );
                        //echo $result;
                        curl_close( $ch );
                 
                    
                    
                        $save=new Notification;
                        $save->title= "Hayyaak Notification";
                        $save->body= "It gives you wellness 🙂, I hope you will pay the dues as soon as possible .. Thank you                        ";
                        $save->title_ar = "أشعارات حياك";
                        $save->body_ar = "يعطيك العافية 🙂 يابطل اتمنى السداد مستحقات في اقرب وقت ممكن .. وشكرا ";
                        $save->sender= 1;
                        $save->is_read=0;
                         $save->request_id=$id;
                        $save->user_id=$check_token['id'];
                        $save->created_at=$dateTime;
                        $save->save();
            }elseif($driverWallet <-50){
        
                
                
                
                $msg = array
                              (
                        'body'  => "حبيت اذكرك 😁 بخصوص التسديد المستحقات ... وشكرا ",
                        'title' => "أشعارات حياك",
                                  
                              );
                      $fields = array
                          (
                            'to'    => $get_user_token['firebase_token'],
                            'notification'  => $msg
                          );
                      
                                       

                      $headers = array
                          (
                            'Authorization: key=' . API_ACCESS_KEY12,
                            'Content-Type: application/json'
                          );
                    #Send Reponse To FireBase Server  
                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        $result = curl_exec($ch );
                        //echo $result;
                        curl_close( $ch );
                 
                    
                    
                        $save=new Notification;
                        $save->title= "Hayyaak Notification";
                        $save->body= " I would like to remind you 😁 about the payment of dues ... and thank you";
                        $save->title_ar = "أشعارات حياك";
                        $save->body_ar = "حبيت اذكرك 😁 بخصوص التسديد المستحقات ... وشكرا ";
                        $save->sender= 1;
                        $save->is_read=0;
                         $save->request_id=$id;
                        $save->user_id=$check_token['id'];
                        $save->created_at=$dateTime;
                        $save->save();
            }
             
           
             
    
        
          if($action ==true){

        /*  $offer_data=Offers::where([['request_id',$id],['driver_id',$check_token['id']]])->update([
             'price'=>$delivery_price,
             'updated_at'=>$dateTime
             ]);*/


           $datax = [
                    "total_price" => $order_price,
                    "taxs" => $tax,
                    "price" => $offer_data['price'],
                    ];
            $message['data'] =   $datax;
            $message['error']=0;
            $message['message']='update bill of order';

          }else{
            
            $message['error']=1;
            $message['message']='error in update bill data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }








    public function user_active_requestes(Request $request)
    {
       try{
        
          $token=$request->input('user_token'); 
          $type  = $request->input('type');
            
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at))); 	 

          $check_token=User::where('user_token',$token)->first();
          if($request->has('user_token') && $check_token !=NULL){
              
                
              
              if($check_token['state']==2 && $type == "driver"){ //driver

                      $request_data=DB::select('select requestes.id,requestes.shop_id,shopes.name,shopes.image as shope_image,requestes.user_id,users.first_name,users.image,users.phone,requestes.description,requestes.delivery_time,state.name as state,offers.id as offer_id,offers.price as offe_price,requestes.total_price,requestes.created_at
                    FROM requestes
                    left join shopes ON requestes.shop_id=shopes.id
                    join users ON requestes.user_id=users.id
                    left join state ON requestes.state=state.id
                    join offers ON requestes.id=offers.request_id
                    where  offers.driver_id ='.$check_token['id'].' AND (offers.state ="accepted" OR offers.state ="received") order by requestes.created_at DESC'); 
                    
                    
                    if(count($request_data)>0){
                        
                          for($i=0;$i<count($request_data);$i++){
                              
                                  $user=$request_data[$i]->user_id;
                                
                                
                                $orderDate = date('Y-m-d', strtotime($request_data[$i]->created_at));
                                
                                $days = date('Y-m-d', strtotime($orderDate. ' + 1 days'));
                                if( $dateTime > $days && ($request_data[$i]->state == 'received_order' || $request_data[$i]->state == 'canceled')){
                                    $deleteRequest = \App\Requests::where('id' , $request_data[$i]->id)->delete();
                                    $deletedetails = \App\request_details::where('request_id' , $request_data[$i]->id)->delete();
                                    
                                }

                                
                                 $check=Rating::where([['rating.user_id',$user],['driver_id',$check_token['id']],['who_rate','driver'],['request_id',$request_data[$i]->id]])->get();
                                 
                                  if(count($check)>0 ){
                                       
                                       $request_data[$i]->rate_state='rated';
                                  }else{
                                      
                                       $request_data[$i]->rate_state=NULL;
                                  }
                          }
                    }
                  
                  
              }elseif($check_token['state']==2 && $type == "user"){
                  
                   $request_data=DB::select('select Distinct requestes.id,requestes.shop_id,shopes.name,shopes.image as shope_image,shopes.image,(select users.id from users join offers ON users.id=offers.driver_id  where offers.request_id= requestes.id AND offers.state="accepted")as user_id,(select users.first_name from users join offers ON users.id=offers.driver_id  where offers.request_id= requestes.id AND offers.state="accepted")as first_name,(select users.image from users join offers ON users.id=offers.driver_id  where offers.request_id= requestes.id AND offers.state="accepted")as image,(select users.phone from users join offers ON users.id=offers.driver_id  where offers.request_id= requestes.id AND offers.state="accepted")as phone,requestes.description,requestes.delivery_time,state.name as state,(select offers.id from offers where offers.request_id= requestes.id AND offers.state="accepted")as offer_id,(select offers.price from offers where offers.request_id= requestes.id AND offers.state="accepted")as offe_price,requestes.total_price,requestes.created_at
                     FROM requestes
                        left join shopes ON requestes.shop_id=shopes.id
                        left join state ON requestes.state=state.id
                        where  requestes.user_id ='.$check_token['id'].' order by requestes.created_at DESC'); 
                        
                        if(count($request_data)>0){
                             
                         for($i=0;$i<count($request_data);$i++){
                              
                                $user=$request_data[$i]->user_id;

                                
                                $orderDate = date('Y-m-d', strtotime($request_data[$i]->created_at));
                                
                                $days = date('Y-m-d', strtotime($orderDate. ' + 1 days'));

                                if( $dateTime > $days && ($request_data[$i]->state == 'received_order' || $request_data[$i]->state == 'canceled') ){
                                    $deleteRequest = \App\Requests::where('id' , $request_data[$i]->id)->delete();
                                    $deletedetails = \App\request_details::where('request_id' , $request_data[$i]->id)->delete();
                                    
                                }
                                 $check=Rating::where([['rating.user_id',$check_token['id']],['driver_id',$user],['who_rate','user'],['request_id',$request_data[$i]->id]])->get();
                                 
                                  if(count($check)>0 ){
                                       
                                       $request_data[$i]->rate_state='rated';
                                  }else{
                                      
                                       $request_data[$i]->rate_state=NULL;
                                  }
                          }
                    }
                    
              }else{

                    $request_data=DB::select('select Distinct requestes.id,requestes.shop_id,shopes.name,shopes.image as shope_image,shopes.image,(select users.id from users join offers ON users.id=offers.driver_id  where offers.request_id= requestes.id AND offers.state="accepted")as user_id,(select users.first_name from users join offers ON users.id=offers.driver_id  where offers.request_id= requestes.id AND offers.state="accepted")as first_name,(select users.image from users join offers ON users.id=offers.driver_id  where offers.request_id= requestes.id AND offers.state="accepted")as image,(select users.phone from users join offers ON users.id=offers.driver_id  where offers.request_id= requestes.id AND offers.state="accepted")as phone,requestes.description,requestes.delivery_time,state.name as state,(select offers.id from offers where offers.request_id= requestes.id AND offers.state="accepted")as offer_id,(select offers.price from offers where offers.request_id= requestes.id AND offers.state="accepted")as offe_price,requestes.total_price,requestes.created_at
                     FROM requestes
                        left join shopes ON requestes.shop_id=shopes.id
                        left join state ON requestes.state=state.id
                        where  requestes.user_id ='.$check_token['id'].' order by requestes.created_at DESC'); 
                        
                        if(count($request_data)>0){
                             
                         for($i=0;$i<count($request_data);$i++){
                              
                                $user=$request_data[$i]->user_id;
                                
                                $orderDate = date('Y-m-d', strtotime($request_data[$i]->created_at));
                                
                                $days = date('Y-m-d', strtotime($orderDate. ' + 1 days'));

                                if( $dateTime > $days && ($request_data[$i]->state == 'received_order' || $request_data[$i]->state == 'canceled')){
                                    $deleteRequest = \App\Requests::where('id' , $request_data[$i]->id)->delete();
                                    $deletedetails = \App\request_details::where('request_id' , $request_data[$i]->id)->delete();
                                    
                                }
                                 $check=Rating::where([['rating.user_id',$check_token['id']],['driver_id',$user],['who_rate','user'],['request_id',$request_data[$i]->id]])->get();
                                 
                                  if(count($check)>0 ){
                                       
                                       $request_data[$i]->rate_state='rated';
                                  }else{
                                      
                                       $request_data[$i]->rate_state=NULL;
                                  }
                          }
                    }
              }

        
          if( count($request_data) >0){


            $message['data']=$request_data;
            $message['error']=0;
            $message['message']='show  order data';

          }else{
            $message['data']=$request_data;
            $message['error']=1;
            $message['message']='no order data';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
   


    public function user_finished_requestes(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
         
          
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              
              if($check_token['state']==2){
                  
                  
               $request_data=DB::select('select requestes.id,requestes.shop_id,shopes.name,requestes.user_id,users.first_name,users.image,users.phone,requestes.description,requestes.delivery_time,requestes.state,requestes.created_at
                FROM requestes
                left join shopes ON requestes.shop_id=shopes.id
                join users ON requestes.user_id=users.id
                join offers ON requestes.id=offers.request_id
                where  offers.driver_id ='.$check_token['id'].' AND  requestes.state="received"  order by requestes.created_at DESC'); 
                  
                  
              }elseif($check_token['state']==3){
                $request_data=DB::select('select requestes.id,requestes.shop_id,shopes.name,shopes.image,requestes.user_id,users.first_name,users.image,users.phone,requestes.description,requestes.delivery_time,requestes.state,offers.price,requestes.created_at
                FROM requestes
                join shopes ON requestes.shop_id=shopes.id
                left join offers ON requestes.id=offers.request_id
                left join users ON offers.driver_id=users.id
                where  requestes.user_id ='.$check_token['id'].' 
                AND  requestes.state ="received" order by  requestes.created_at DESC'); 
              }else{
                  $request_data = [];
              }

        
          if( $request_data !=null){


            $message['data']=$request_data;
            $message['error']=0;
            $message['message']='show  order data';

          }else{
            $message['data']=$request_data;
            $message['error']=1;
            $message['message']='no order data';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    /////////////////////////////////////////////shope branches////////////
    
    
      public function add_branch(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $name=$request->input('name');
              $image=$request->file('image'); 
              $desc=$request->input('description');
              $address=$request->input('address');
              $long=$request->input('longitude');
              $lat=$request->input('latitude');
              $id=$request->input('shope_id');
              $dis=$request->input('discount');


             if(isset($image)) {
                  $new_name = $image->getClientOriginalName();
                  $savedFileName = rand(100000,999999).time()."_".$new_name; 
                    // give a unique name to file to    be saved
                        $destinationPath_id = 'uploads/shopes';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
                 }else{
                    $images =NULL;     
                 }


           $cat=Shopes::where('id',$id)->value('cat_id');
                  $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $insert=new Shopes;
             $insert->name=$name;
             $insert->image=$images;
             $insert->branch=$id;
             $insert->description=$desc;
             $insert->address=$address;
             $insert->longitude=$long;
             $insert->latitude=$lat;
             $insert->branch=$id;
             $insert->discount=$dis;
             $insert->cat_id=$cat;
             $insert->created_at=$dateTime;
             $insert->save();
    
        
          if($insert ==true){
        

         
            $message['error']=0;
            $message['message']='add branch to shope';

          }else{
           
            $message['error']=1;
            $message['message']='error in insert branch data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


  public function update_branch(Request $request)
  {
       try{
        
          $token=$request->input('user_token');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
             
              $id=$request->input('id');
              $name=$request->input('name');
              $image=$request->file('image'); 
              $desc=$request->input('description');
              $address=$request->input('address');
              $long=$request->input('longitude');
              $lat=$request->input('latitude');
                  $dis=$request->input('discount');
                  $cat=$request->input('category_id');
              


             if(isset($image)) {
                  $new_name = $image->getClientOriginalName();
                  $savedFileName = rand(100000,999999).time()."_".$new_name; 
                    // give a unique name to file to    be saved
                        $destinationPath_id = 'uploads/shopes';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
                 }else{
                    $images =Shopes::where('id',$id)->value('image');     
                 }
                  $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $update=Shopes::where('id',$id)->update([

              'name'=>$name,
              'image'=>$images,
              'address'=>$address,
              'longitude'=>$long,
              'latitude'=>$lat,
              'discount'=>$dis,
              'cat_id'=>$cat,
              'description'=>$desc,
              'updated_at'=>$dateTime


              ]);
               
          if($update ==true){
         
            $message['error']=0;
            $message['message']='update branch';

          }else{
           
            $message['error']=1;
            $message['message']='error in update branch data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
  }

  public function show_branches(Request $request)
  {
       try{
        
          $token=$request->input('user_token');
          $id=$request->input('shope_id');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $select=Shopes::where('branch',$id)->get();
          
  
          if(count($select) >0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show branches data';

          }else{
             $message['data']=$select;
            $message['error']=1;
            $message['message']='no branches data ';
          }
        
       }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
  }


    public function show_branchbyid(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
          $id=$request->input('id');
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $select=Shopes::select('id','name','description','image','address','longitude','latitude','discount','cat_id as category_id','created_at','updated_at')->where('id',$id)->first();
          
  
          if($select !=null){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show shope data';

          }else{
             $message['data']=$select;
            $message['error']=1;
            $message['message']='no shope data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
  
    public function delete_branche(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
          $id=$request->input('id');
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $delete=Shopes::where('id',$id)->delete();
          
  
          if($delete ==true){

          
            $message['error']=0;
            $message['message']='delete  data';

          }else{
           
            $message['error']=1;
            $message['message']='error in delete  data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

    

}
