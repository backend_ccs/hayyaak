<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Notification;
use carbon\carbon;
use App\Messages;
use App\Requests;
use App\Bill;
use App\setting;
use App\deriver_details;
use App\request_details;


class MessageController extends Controller
{
     public $message=array();
     
     
     
       public function click_order(Request $request)
      {
       try{
        
          $token=$request->input('user_token'); 
          $order=$request->input('request_id');  
          
          $check_token=User::where('user_token',$token)->first();
          
          if($request->has('user_token') && $check_token !=NULL){
              
              
              
                  
                  
          $request_data=Requests::join('offers','requestes.id','=','offers.request_id')->where([['requestes.id',$order],['offers.state','accepted']])->first();
          
          
          if( $request_data !=null){


       
            $message['error']=1;
            $message['message']='sorry some one make offer on it';

          }else{
            
            $message['error']=0;
            $message['message']='you can make offer';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
    
     public function remove_email(Request $request)
      {
       try{
        
          $email=$request->input('email'); 
         
              
                  
          $data=User::where('email',$email)->update([
              
               'email'=>NULL
              
              
              ]);
          
          
          if( $data ==true){


       
            $message['error']=0;
            $message['message']='remove success';

          }else{
            
            $message['error']=1;
            $message['message']='errorin remove data,no email';
          }
          

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
     
     
        public function new_requestes(Request $request)
      {
       try{
        
          $token=$request->input('user_token');     
          $check_token=User::where('user_token',$token)->first();
          if($request->has('user_token') && $check_token !=NULL){
              
              
              
                  
                  
          $request_data=DB::select('select requestes.id,requestes.shop_id,shopes.name as shope_name,
                     users.first_name,users.image as user_image, requestes.receiving_address, requestes.receiving_longitude,requestes.receiving_latitude,
                     requestes.delivery_longitude,requestes.delivery_latitude,requestes.delivery_address,requestes.description,requestes.delivery_time,state.name as state,requestes.created_at
                    FROM requestes
                    left join shopes ON requestes.shop_id=shopes.id
                    join users  ON requestes.user_id=users.id
                    join state ON requestes.state=state.id
                
                    where  requestes.state=1'); 
                  
                  
           

        
          if( count($request_data) >0){


            $message['data']=$request_data;
            $message['count']=count($request_data);
            $message['error']=0;
            $message['message']='show  order data';

          }else{
            $message['data']=$request_data;
              $message['count']=0;
            $message['error']=1;
            $message['message']='no order data';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
       public function wait_requestes(Request $request)
      {
       try{
        
          $token=$request->input('user_token');     
          $check_token=User::where('user_token',$token)->first();
          if($request->has('user_token') && $check_token !=NULL){
              
              
              
                  
                  
          $request_data=DB::select('select requestes.id,requestes.shop_id,shopes.name as shope_name,
                     users.first_name,users.image as user_image, requestes.receiving_address, requestes.receiving_longitude,requestes.receiving_latitude,
                     requestes.delivery_longitude,requestes.delivery_latitude,requestes.delivery_address,requestes.description,requestes.delivery_time,state.name as state,requestes.created_at
                    FROM requestes
                    left join shopes ON requestes.shop_id=shopes.id
                    join users  ON requestes.user_id=users.id
                    join state ON requestes.state=state.id
                
                    where  requestes.state=2'); 
                  
                  
           

        
          if( count($request_data) >0){


            $message['data']=$request_data;
            $message['count']=count($request_data);
            $message['error']=0;
            $message['message']='show  order data';

          }else{
            $message['data']=$request_data;
              $message['count']=0;
            $message['error']=1;
            $message['message']='no order data';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
     
      public function canceled_requestes(Request $request)
      {
       try{
        
          $token=$request->input('user_token');     
          $check_token=User::where('user_token',$token)->first();
          if($request->has('user_token') && $check_token !=NULL){
              
              
              
                  
                  
          $request_data=DB::select('select requestes.id,requestes.shop_id,shopes.name as shope_name,
                     users.first_name,users.image as user_image, requestes.receiving_address, requestes.receiving_longitude,requestes.receiving_latitude,
                     requestes.delivery_longitude,requestes.delivery_latitude,requestes.delivery_address,requestes.description,requestes.delivery_time,state.name as state,requestes.created_at
                    FROM requestes
                    left join shopes ON requestes.shop_id=shopes.id
                    join users  ON requestes.user_id=users.id
                    join state ON requestes.state=state.id
                
                    where  requestes.state=6'); 
                  
                  
           

        
          if( count($request_data) >0){


            $message['data']=$request_data;
              $message['count']=count($request_data);
            $message['error']=0;
            $message['message']='show  order data';

          }else{
            $message['data']=$request_data;
              $message['count']=0;
            $message['error']=1;
            $message['message']='no order data';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
    
     public function hang_requestes(Request $request)
      {
       try{
        
          $token=$request->input('user_token');     
          $check_token=User::where('user_token',$token)->first();
          if($request->has('user_token') && $check_token !=NULL){
              
              
              
                  
                  
          $request_data=DB::select('select requestes.id,requestes.shop_id,shopes.name as shope_name,
                     users.first_name,users.image as user_image, requestes.receiving_address, requestes.receiving_longitude,requestes.receiving_latitude,
                     requestes.delivery_longitude,requestes.delivery_latitude,requestes.delivery_address,requestes.description,requestes.delivery_time,state.name as state,requestes.created_at
                    FROM requestes
                    left join shopes ON requestes.shop_id=shopes.id
                    join users  ON requestes.user_id=users.id
                    join state ON requestes.state=state.id
                
                    where  requestes.state=8'); 
                  
                  
           

        
          if( count($request_data) >0){


            $message['data']=$request_data;
            $message['count']=count($request_data);
            $message['error']=0;
            $message['message']='show  order data';

          }else{
            $message['data']=$request_data;
              $message['count']=0;
            $message['error']=1;
            $message['message']='no order data';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
    
    //pending
     public function late_requestes(Request $request)
      {
       try{
        
          $token=$request->input('user_token');     
          $check_token=User::where('user_token',$token)->first();
          if($request->has('user_token') && $check_token !=NULL){
              
          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
              
                  
                  
          $request_data=DB::select('select requestes.id,requestes.shop_id,shopes.name as shope_name,
                     users.first_name,users.image as user_image, requestes.receiving_address, requestes.receiving_longitude,requestes.receiving_latitude,
                     requestes.delivery_longitude,requestes.delivery_latitude,requestes.delivery_address,requestes.description,requestes.delivery_time,state.name as state,requestes.created_at
                    FROM requestes
                    left join shopes ON requestes.shop_id=shopes.id
                    join users  ON requestes.user_id=users.id
                    join state ON requestes.state=state.id
                
                    where (requestes.state =2 OR requestes.state =3 OR requestes.state =5 ) AND requestes.accepted_time > "'.$dateTime.'"'); 
                  
                  
                  
           
           //$request_data['count']=
        
          if( count($request_data) >0){


            $message['data']=$request_data;
            $message['count']=count($request_data);
            $message['error']=0;
            $message['message']='show  order data';

          }else{
            $message['data']=$request_data;
            $message['count']=0;
            $message['error']=1;
            $message['message']='no order data';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
     public function finished_requestes(Request $request)
      {
       try{
        
          $token=$request->input('user_token');     
          $check_token=User::where('user_token',$token)->first();
          if($request->has('user_token') && $check_token !=NULL){
              
              
              
                  
                  
          $request_data=DB::select('select requestes.id,requestes.shop_id,shopes.name as shope_name,
                     user.first_name as client_name,user.image as client_image,agent.first_name as agent_name,agent.image as agent_image,requestes.receiving_address, requestes.receiving_longitude,requestes.receiving_latitude,
                     requestes.delivery_longitude,requestes.delivery_latitude,requestes.delivery_address,requestes.description,requestes.delivery_time,state.name as state,requestes.created_at
                    FROM requestes
                    left join shopes ON requestes.shop_id=shopes.id
                    join offers ON requestes.id=offers.request_id

                    join users as user ON requestes.user_id=user.id
                      join users as agent  ON offers.driver_id=agent.id
                    join state ON requestes.state=state.id
                
                    where  requestes.state=4 AND offers.state="accepted"'); 
                  
                  
           

        
          if( count($request_data) >0){


            $message['data']=$request_data;
              $message['count']=count($request_data);
            $message['error']=0;
            $message['message']='show  order data';

          }else{
            $message['data']=$request_data;
              $message['count']=0;
            $message['error']=1;
            $message['message']='no order data';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
    
     
    public function show_bill(Request $request)
      {
       try{
        
          $token=$request->input('user_token'); 
          $id=$request->input('request_id');    
          
          $check_token=User::where('user_token',$token)->first();
          if($request->has('user_token') && $check_token !=NULL){
              
              
              
                  
                  
          $bill_data=Bill::select('bill.id','user.first_name as user_name','agent.first_name as agent_name','bill.order_price','bill.delivery_price','bill.tax','bill.total_price')
                      ->join('users as user','bill.user_id','=','user.id')
                      ->join('users as agent','bill.driver_id','=','agent.id')->where('bill.request_id',$id)->get();
                  
                  
           

        
          if( count($bill_data) >0){


            $message['data']=$bill_data;
            $message['error']=0;
            $message['message']='show  bill data';

          }else{
            $message['data']=$bill_data;
            $message['error']=1;
            $message['message']='no bill data';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


       
    public function turn_ONNotifications(Request $request)
    {
       try{
           $token=$request->input('user_token');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
          
        

         $check=setting::where('user_id',$check_token['id'])->first();
         
              $created_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
          
         if($check !=null){
              $state=setting::where('user_id',$check_token['id'])->value('notification');
              
              if($state =='ON')
              {
               $update=setting::where('user_id',$check_token['id'])->update([
                    'notification'=>'off',
                    'updated_at'=>$dateTime
                   ]);
                   
              }else{
                      $update=setting::where('user_id',$check_token['id'])->update([
                    'notification'=>'ON',
                    'updated_at'=>$dateTime
                    ]);
              }
                    
                    $role=setting::where('user_id',$check_token['id'])->first();
                       if($update ==true){
        
                    $message['data']=$role;
                    $message['error']=0;
                    $message['message']='update successfully';
        
                  }else{
                    $message['data']=$role;
                    $message['error']=1;
                    $message['message']='error in update notification ';
                  }
                    
                  
                  
              
         }else{
              $insert=new setting;
              $insert->user_id=$check_token['id'];
              $insert->notification='ON';
              $insert->created_at=$dateTime;
              $insert->save();
              
              $role=setting::where('user_id',$check_token['id'])->first();
              
              
                  if($insert ==true){
        
                    $message['data']=$role;
                    $message['error']=0;
                    $message['message']='show successfully';
        
                  }else{
                    $message['data']=$role;
                    $message['error']=1;
                    $message['message']='no data ';
                  }
             
         }
       }else{
                $message['error']=3;
                    $message['message']='this token is not exist';
       }

              
         
       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
    



    public function show_Notifications(Request $request)
    {
       try{
           $token=$request->input('user_token');
           $type  = $request->input('type');
           $lang  = $request->input('lang');

          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
        
          $nowdate= date('Y-m-d H:i:s');


         if($check_token['state']==2  && $type == "driver"){

            if( $lang == 'ar'){
              $notify=DB::select('SELECT notification.id,notification.request_id,notification.sender as user_id,(select offers.id from offers where offers.driver_id='.$check_token['id'].' AND offers.request_id=notification.request_id limit 1) as offer_id,
                                    users.first_name as user_name,users.image as user_image,notification.title_ar as title,notification.body_ar as body,notification.click_action,notification.created_at,state.name as state from notification
                                    join users ON notification.sender=users.id
                                    left join requestes ON notification.request_id=requestes.id
                                    left join state On requestes.state=state.id

                                where notification.user_id='.$check_token['id'].' order by notification.id DESC');
            }else{
                $notify=DB::select('SELECT notification.id,notification.request_id,notification.sender as user_id,(select offers.id from offers where offers.driver_id='.$check_token['id'].' AND offers.request_id=notification.request_id limit 1) as offer_id,
                                            users.first_name as user_name,users.image as user_image,notification.title,notification.body,notification.click_action,notification.created_at,state.name as state from notification
                                            join users ON notification.sender=users.id
                                            left join requestes ON notification.request_id=requestes.id
                                            left join state On requestes.state=state.id
                  
                                         where notification.user_id='.$check_token['id'].' order by notification.id DESC');
            }
         }elseif($check_token['state']==2 && $type == "user"){
             
          if( $lang == 'ar'){
            $notify=DB::select('SELECT notification.id,notification.request_id,notification.sender as user_id,(select offers.id from offers where offers.driver_id=notification.sender AND offers.request_id=notification.request_id limit 1) as offer_id,
                                  users.first_name as user_name,users.image as user_image,notification.title_ar as title,notification.body_ar as body,notification.created_at,state.name as state from notification
                                  join users ON notification.sender=users.id
                                  left join requestes ON notification.request_id=requestes.id
                                  left join state On requestes.state=state.id
                              where notification.user_id='.$check_token['id'].' order by notification.id DESC');
          }else{
              $notify=DB::select('SELECT notification.id,notification.request_id,notification.sender as user_id,(select offers.id from offers where offers.driver_id=notification.sender AND offers.request_id=notification.request_id limit 1) as offer_id,
                                            users.first_name as user_name,users.image as user_image,notification.title,notification.body,notification.created_at,state.name as state from notification
                                            join users ON notification.sender=users.id
                                            left join requestes ON notification.request_id=requestes.id
                                            left join state On requestes.state=state.id
                                        where notification.user_id='.$check_token['id'].' order by notification.id DESC');
          }
         }else{
            if( $lang == 'ar'){
              $notify=DB::select('SELECT notification.id,notification.request_id,notification.sender as user_id,(select offers.id from offers where offers.driver_id=notification.sender AND offers.request_id=notification.request_id limit 1) as offer_id,
                                    users.first_name as user_name,users.image as user_image,notification.title_ar as title,notification.body_ar as body,notification.created_at,state.name as state from notification
                                    join users ON notification.sender=users.id
                                    left join requestes ON notification.request_id=requestes.id
                                    left join state On requestes.state=state.id
                                where notification.user_id='.$check_token['id'].' order by notification.id DESC');
            }else{
             
                $notify=DB::select('SELECT notification.id,notification.request_id,notification.sender as user_id,(select offers.id from offers where offers.driver_id=notification.sender AND offers.request_id=notification.request_id limit 1) as offer_id,
                                            users.first_name as user_name,users.image as user_image,notification.title,notification.body,notification.created_at,state.name as state from notification
                                            join users ON notification.sender=users.id
                                            left join requestes ON notification.request_id=requestes.id
                                            left join state On requestes.state=state.id
                                        where notification.user_id='.$check_token['id'].' order by notification.id DESC');
            }
             
         }

      
            if(count($notify) >0){
        
               $update=Notification::where('notification.user_id',$check_token['id'])->update([
                   'is_read'=>1
                   
                   ]);
                      
                    $message['data']=$notify;
                    $message['error']=0;
                    $message['message']='show successfully';
        
                  }else{
                    $message['data']=$notify;
                    $message['error']=1;
                    $message['message']='no data ';
                  }
          }else{
              $message['error']=3;
            $message['message']='this token is\'t exist';
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


public function  delete_Notification(Request $request)
    {
       try{
           $token=$request->input('user_token');
            $id=$request->input('id');

          $check_token=User::where('user_token',$token)->first();
          
          
         if($request->has('user_token') && $check_token !=NULL){
        
              $delete=Notification::where('id',$id)->delete();
              
                      if($delete ==true){
                          $message['error']=0;
                          $message['message']='delete success';
                          
                      }else{
                          
                           $message['error']=1;
                          $message['message']='error in delete';
                      }


          }else{
              $message['error']=3;
            $message['message']='this token is\'t exist';
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


     public function Notification_count(Request $request)
    {
       try{
           $token=$request->input('user_token');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
        
       

         $role=Notification::where([['notification.user_id',$check_token['id']],['is_read',0]])->count();
         
         
     
          

               
        
                      
                    $message['data']=$role;
                    $message['error']=0;
                    $message['message']='show successfully';
        
                 
          }else{
              $message['error']=3;
            $message['message']='this token is\'t exist';
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


    Public function showmessage(Request $request){
         try{

    	      $token=$request->input('user_token');
          
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
        


            $user_id=$request->input('user_id');
            $order=$request->input('order');

             $all_mess=Messages::select('messages.id','messages.request_id','messages.message','sender.id as sender_id','sender.first_name as sender_name','sender.phone as sender_phone','sender.image as sender_image','receiver.id as receiver_id','receiver.first_name as receiver_name','receiver.image as receiver_image','receiver.phone as receiver_phone','messages.image','messages.voice','messages.created_at','messages.updated_at')
                                ->join('users as sender','messages.sender_id','=','sender.id')
                                ->leftJoin('users as receiver','messages.receiver_id','=','receiver.id')
         
                          ->where([['sender_id',$user_id],['receiver_id' , $check_token['id']],['request_id',$order]])
                            ->orwhere([['sender_id',$check_token['id']],['receiver_id', $user_id],['request_id',$order]])
                            ->orwhere([['sender_id',$user_id],['request_id',$order]])
                            ->orwhere([['receiver_id',$user_id],['request_id',$order]])
                             ->orwhere([['sender_id',$check_token['id']],['request_id',$order]])
                            ->orwhere([['receiver_id',$check_token['id']],['request_id',$order]])
                             ->get();
            
             $update=Messages::where([['sender_id',$user_id],['receiver_id' , $check_token['id']],['request_id',$order]])
            ->orwhere([['sender_id',$check_token['id']],['receiver_id', $user_id],['request_id',$order]])
             
             ->update([
                 'is_read'=>1
                 ]);
            
            

    	      if(count($all_mess) >0)
    	      {          
    	          $all_mess[0]['message'] = Requests::where('requestes.id',$order)->value('description');

    	          $state=Requests::select('state.id','state.name' , 'requestes.accepted_time')->where('requestes.id',$order)
    	                        ->join('state','requestes.state','=','state.id')
    	                        ->first();
    	        
    	        $get_acceptTime = \App\Offers::where([['state' , "accepted"], ['request_id' , $order]])->value('accepted_time');
    	        
    	        if($state->id == 1 || $state->id == 8){
    	            $acceptTime = NULL;
    	        }else{
    	            $acceptTime = $get_acceptTime;
    	        }
    	        
    	        $get_deliveryTime = \App\Requests::where('requestes.id',$order)->value('updated_at');
    	        
    	        $message['data']=$all_mess;
    	        $message['state']=$state['name'];
                $message['acceptTime'] = $acceptTime;
                $message['deliveryTime'] = $get_deliveryTime;
    	        $message['error']=0;
    	        $message['message']='show message body';
    	      }else{
    	          $all_mess[] = [
    	              "id" => 0,
    	              "request_id" =>(string)$order,
    	              "message"   => Requests::where('requestes.id',$order)->value('description'),
    	              "sender_id" => (string)$user_id,
    	              "sender_name" => \App\User::where('id' , $user_id)->value('first_name'),
    	              "sender_phone" => \App\User::where('id' , $user_id)->value('phone'),
      	              "sender_image" => \App\User::where('id' , $user_id)->value('image'),
    	              "receiver_id" => NULL,
    	              "receiver_name" => NULL,
    	              "receiver_image" => NULL,
    	              "receiver_phone" => NULL,
    	              "image" => NULL,
    	              "voice" => NULL,
                      "created_at" =>   NULL,
                      "updated_at" => NULL

                        ];
    	        $message['data']=$all_mess;
    	        $message['state']=NULL;

    	      $message['error']=1;
    	      $message['message']='no message';
    	      }
    	     }else{
    	           $message['error']=3;
    	           $message['message']='this token is\'t exist';
    	      }

           }catch(Exception $ex){
             
                $message['error']=2;
                $message['message']='error'.$ex->getMessage();

           }

      return response()->json($message);
    }
       
    Public function sentmessage(Request $request)
    {  

      try{

    	  $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
        


        $user_id=$request->input('user_id');
		    $body=$request->input('message');
		    $image=$request->file('image');
		    $voice=$request->file('voice');
            $id=$request->input('request_id');
		      

          $created_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
          
          if(isset($image)) {
                        $new_name = $image->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/messages';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
               }else{
                  $images = NULL;       
              }

               if(isset($voice)) {
                        $new_name = $voice->getClientOriginalName();
                        $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads/messages';
                        $voice->move($destinationPath_id, $savedFileName);
            
                        $voices = $savedFileName;
                      
               }else{
                  $voices = NULL;       
              }
           
           $who='';
            if($check_token['state']==2){
              $who='agent_message';
            }else{
                $who='user_message';
                
            }
              
               $mess=new Messages;

              $mess->sender_id=$check_token['id'];
              $mess->receiver_id=$user_id;
              $mess->request_id=$id;
              $mess->image=$images;
              $mess->voice=$voices;
              $mess->message=$body;
              $mess->who_sent=$who;
           
              $mess->created_at=$dateTime;
              $mess->save();
   
        

            if($mess == true){
                  $message['data']=$mess;
      		      $message['error']=0;
      		      $message['message']='sent message success';
      		      
      		      
      		         
                   
                          try{       
                   //  $cc = 0;
                     
                
       $title ='the agent'.$check_token['first_name'].' sent  you a message';
         $body =$check_token['first_name'].'sent you a message  on  order '.$id;
         
                     
                 define( 'API_ACCESS_KEY12','AAAAnSDEgLc:APA91bFisJ6mf6QmnpUvaC48ND3u4u_ULaQNnR4fxCRur392hErem3qi3fjRQ05wXg45D8NrhehK4Bp8h1f_uUGW0YbSQBmxDRWg3pi9PBkHvYnE8GHqjYs3JRTmCab08jUJQph9luxS');
            
            
                   $get_user_token =User::select('firebase_token')->where('id', '=',$user_id)->first();
                   
                      // return $get_user_token[0]->firebase_token;
            
            
                                 ////////////////////////////////////////////////////////
                                 
                                   $msg = array
                                          (
                                    'body'  => $body,
                                    'title' => $title,
                                              
                                          );
                                  $fields = array
                                      (
                                        'to'    => $get_user_token['firebase_token'],
                                        'notification'  => $msg
                                      );
                                  
                                                   
            
                                  $headers = array
                                      (
                                        'Authorization: key=' . API_ACCESS_KEY12,
                                        'Content-Type: application/json'
                                      );
                                #Send Reponse To FireBase Server  
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    //echo $result;
                                    curl_close( $ch );
                             
                    
                    
                        $save=new Notification;
                        $save->title=$title;
                        $save->body=$body;
                        $save->sender=$check_token['id'];
                        $save->is_read=0;
                         $save->request_id=$id;
                        $save->user_id=$user_id;
                        $save->created_at=$dateTime;
                        $save->save();
                        
                        
                     }catch(Exception $ex){
                          $message['error']=4;
                           $message['message']='error in send notification';
                          
                     }
      		      }else{

      		      $message['data']=$mess;  
      		      $message['error']=1;
      		      $message['message']='error in send';
      		      }

          }else{
	           $message['error']=3;
	           $message['message']='this token is\'t exist';
	      }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
      return response()->json($message);
    }

   

    Public function show_allMessageOfUser(Request $request)
    {
     	 try{

    	  $token=$request->input('user_token');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

          

          $all_mess=Messages::select('messages.id','messages.sender_id','messages.receiver_id','messages.message','sender.first_name as sender_name','sender.image as sender_image','receiver.first_name as receiver_name','receiver.image as receiver_image','messages.created_at','messages.updated_at')
            ->join('users as sender','messages.sender_id','=','sender.id')
            ->join('users as receiver','messages.receiver_id','=','receiver.id')
           
            ->where('sender_id',$check_token['id'])
            ->orWhere('receiver_id',$check_token['id'])
            ->get();


   


	        if(count($all_mess) >0)
	        {
	        $message['data']=$all_mess;

	        $message['error']=0;
	        $message['message']='show message body';
	        }else{

	        $message['data']=$all_mess;

	        $message['error']=1;
	        $message['message']='no message';
	        }
	       }else{
		           $message['error']=3;
		           $message['message']='this token is\'t exist';
		      }

	       }catch(Exception $ex){
	         
	            $message['error']=2;
	            $message['message']='error'.$ex->getMessage();

	       }

      return response()->json($message);
    }

   Public function show_UsersHasMessage(Request $request)
     {


         try{

    	    $token=$request->input('user_token');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
            
            
		          $all_mess=DB::select('SELECT t1.id,t1.sender_id,t1.receiver_id,t1.message,t1.is_read,sender.image as sender_image,sender.phone as sender_phone,receiver.image as receiver_image,receiver.phone as receiver_phone,t1.created_at
		        FROM messages AS t1
		        INNER JOIN
		        (
		            SELECT
		                LEAST(sender_id, receiver_id) AS sender_id,
		                GREATEST(sender_id, receiver_id) AS receiver_id,
		                MAX(id) AS max_id
		            FROM messages
		            GROUP BY
		                LEAST(sender_id, receiver_id),
		                GREATEST(sender_id, receiver_id)
		        ) AS t2
		            ON LEAST(t1.sender_id, t1.receiver_id) = t2.sender_id AND
		               GREATEST(t1.sender_id, t1.receiver_id) = t2.receiver_id AND
		               t1.id = t2.max_id


		               INNER JOIN users as sender ON t1.sender_id=sender.id
		                INNER JOIN users as receiver ON t1.receiver_id=receiver.id
		            WHERE (t1.sender_id ='.$check_token['id'].' OR t1.receiver_id ='.$check_token['id'].')AND t1.sender_id !=t1.receiver_id');
		        
      


	        if(count($all_mess) >0)
	        {
	        $message['data']=$all_mess;

	        $message['error']=0;
	        $message['message']='show message body';
	        }else{

	        $message['data']=$all_mess;

	        $message['error']=1;
	        $message['message']='no message';
	        }
	       }else{
		           $message['error']=3;
		           $message['message']='this token is\'t exist';
		      }

	       }catch(Exception $ex){
	         
	            $message['error']=2;
	            $message['message']='error'.$ex->getMessage();

	       }
	      return response()->json($message);
    } 

}
