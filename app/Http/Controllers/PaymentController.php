<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\User;
use App\User_copon;
use App\setting;
use carbon\carbon;
use App\Vehicels;
use App\Copon;

class PaymentController extends Controller
{
     public $message=array();


   public function show_payment(Request $request)
   {
     try{
       
       $lang=$request->input('lang');

   

       //1 =>arabic     2=>english
            if($lang =='en' || $lang=='EN')
            {
            $pay=Payment::select('id','E_name as name')->get();
            }else{
            $pay=Payment::select('id','name')->get();
            }
      
       if(count($pay) > 0){
            
             $message['data']=$pay;
            $message['error']=0;
            $message['message']='show data success';
       }else{

            $message['data']=$pay;
            $message['error']=1;
            $message['message']='no data';
       }
    

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

       return response()->json($message);
   }

  


    public function add_mycopon(Request $request){
        try{

            $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){


            $code=$request->input('copon');
         
            
            $created_at = carbon::now()->toDateTimeString();
           $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
           
            
            $insertCopon = new User_copon;
            
            $insertCopon->copon = $code;
            $insertCopon->user_id = $check_token['id'];
            
            $insertCopon->created_at = $dateTime;
            
            $insertCopon->save();
            
            if($insertCopon == true){
                
                $update=Copon::where('code',$code)->increment('use_count',1);
                $message['error'] = 0;
                $message['message'] = "a new copon is inserted successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
              }else{
                 $message['error']=3;
             $message['message']='this token is not exist';
            }

            
        }catch(Exception $ex){
          $message['error'] = 2;
            $message['message'] = "error('DataBase Error :{$ex->getMessage()}')";
         }
    
          return response()->json($message);
    }
    
    
    
    public function show_myCopons(Request $request){
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){

            $get_allCopons = User_copon::select('id','copon','created_at')
           ->where('user_id',$check_token['id'])
            ->orderBy('id','ASC')->get();
            
            if( count($get_allCopons)>0 ){

                $message['data'] = $get_allCopons;
                $message['error'] = 0;
                $message['message'] = "there is all the copon data";
            }else{

                $message['data'] = $get_allCopons;
                $message['error'] = 1;
                $message['message'] = "there is an errorn please try again";
            }
          }else{
          $message['error']=3;
        $message['message']='this token is not exist';
        }

            
        }catch(Exception $ex){
          $message['error'] = 2;
            $message['message'] = "error('DataBase Error :{$ex->getMessage()}')";
         }
    
          return response()->json($message);
    }
    
    
    public function delete_mycopon(Request $request){
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){

            $copon_id = $request->input('copon_id');
            
            $deleteCopon =User_copon::where('id',$copon_id)->delete();
            
            if($deleteCopon == true){

                $message['error'] = 0;
                $message['message'] = "this copon is deleted succesfully";
            }else{

                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
          }else{
          $message['error']=3;
        $message['message']='this token is not exist';
        }
            
        }catch(Exception $ex){
          $message['error'] = 2;
            $message['message'] = "error('DataBase Error :{$ex->getMessage()}')";
         }
    
          return response()->json($message);
    } 
    
    
    
    
    
    
    /*************************************************************** payment online ************************************/
    
    
    public function payment_request(Request $request){
        
        $token=$request->input('user_token');
          
        $check_token=User::select('id')->where('user_token',$token)->first();
          
        if($request->has('user_token') && $check_token !=NULL){

            
            $merchantTransactionId = $request->input('merchantTransactionId');
            $amount = $request->input('amount');
            $email = $request->input('email');
            
            $url = "https://test.oppwa.com/v1/checkouts";
        	$data = "entityId=8a8294174b7ecb28014b9699220015ca" .
                        "&amount=$amount" .
                        "&currency=EUR" .
                        "&paymentType=DB" .
                        "&merchantTransactionId=$merchantTransactionId".
                        "&customer.email=$email".
                        "&notificationUrl=http://www.example.com/notify";
        
        	$ch = curl_init();
        	curl_setopt($ch, CURLOPT_URL, $url);
        	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                           'Authorization:Bearer OGE4Mjk0MTc0YjdlY2IyODAxNGI5Njk5MjIwMDE1Y2N8c3k2S0pzVDg='));
        	curl_setopt($ch, CURLOPT_POST, 1);
        	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        	$responseData = curl_exec($ch);
        	if(curl_errno($ch)) {
        		return curl_error($ch);
        	}
        	curl_close($ch);
        	
        	return $responseData;
            
        }else{
            $message['error']=3;
            $message['message']='this token is not exist';
        }
          return response()->json($message);

    }
    
    
    public function payment_status(Request $request){
        $token=$request->input('user_token');
          
        $check_token=User::select('id')->where('user_token',$token)->first();
          
        if($request->has('user_token') && $check_token !=NULL){

        
            $id = $request->input('id');
            
            $url = "https://test.oppwa.com/v1/checkouts/{$id}/payment";
        	$url .= "?entityId=8a8294174b7ecb28014b9699220015ca";
        
        	$ch = curl_init();
        	curl_setopt($ch, CURLOPT_URL, $url);
        	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                           'Authorization:Bearer OGE4Mjk0MTc0YjdlY2IyODAxNGI5Njk5MjIwMDE1Y2N8c3k2S0pzVDg='));
        	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        	$responseData = curl_exec($ch);
        	if(curl_errno($ch)) {
        		return curl_error($ch);
        	}
        	curl_close($ch);
        	
        	return $responseData;
        	
        }else{
            $message['error']=3;
            $message['message']='this token is not exist';
        }
        return response()->json($message);

    }
    
    
}
