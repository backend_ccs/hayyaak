<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Compliance;
use App\User;
use App\Requests;
use App\Language;
use carbon\carbon;
use App\compliance_image;
use App\complain_answer;
use App\complain_question;
use App\setting;

class NotificationController extends Controller
{
    public $message=array();

   public function show_compliance(Request $request)
   {
     try{
       $token=$request->input('user_token');
          
          
       $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

        //2 => driver  ,3 =>users
          if($check_token['state']==2){

               $show=Compliance::select('compliance.id','shopes.name as shope_name','shopes.image as shope_image','compliance.order_id','compliance.state','compliance.content','compliance.created_at')
                    ->join('requestes','compliance.order_id','=','requestes.id')
                    ->join('shopes','requestes.shop_id','=','shopes.id')
                    ->where([['compliance.driver_id',$check_token['id']],['compliance.who_complain','driver']])->get();

          }else{
              $show=Compliance::select('compliance.id','shopes.name as shope_name','shopes.image as shope_image','compliance.order_id','compliance.state','compliance.content','compliance.created_at')
                     ->join('requestes','compliance.order_id','=','requestes.id')
                    ->join('shopes','requestes.shop_id','=','shopes.id')
                   ->where([['compliance.user_id',$check_token['id']],['compliance.who_complain','user']])->get();

          }
      
       if(count($show) > 0){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }

  public function show_compliancebyid(Request $request)
   {
     try{
       $token=$request->input('user_token');
       $id=$request->input('id');   
          
       $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

    
                    $show=Compliance::select('compliance.id','shopes.name as shope_name','shopes.image as shope_image','compliance.order_id','compliance.state','compliance.content','compliance.details','compliance.answer','compliance.created_at')
                    ->join('users','compliance.user_id','=','users.id')
                    ->join('requestes','compliance.order_id','=','requestes.id')
                    ->join('shopes','requestes.shop_id','=','shopes.id')
                  //  ->join('complain_answer','compliance.id','=','complain_answer.complain_id')

                   // ->join('complain_questions','complain_answer.question_id','=','complain_questions.id')
                     ->where('compliance.id',$id)->first();
        
           if($show !=null){
               $images=compliance_image::where('complain_id',$id)->get();
                    $question_answer=complain_question::select('complain_questions.name','complain_answer.answer')->join('complain_answer','complain_questions.id','=','complain_answer.question_id')->where('complain_answer.complain_id',$id)->get();
                $message['data']=$show;
                 $message['answers']=$question_answer;
                $message['images']=$images;
                $message['error']=0;
                $message['message']='show data';
           }else{

                $message['data']=$show;
                $message['error']=1;
                $message['message']='no data';
           }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }

  public function show_languages(Request $request)
   {
     try{
       $token=$request->input('user_token');
        //$lang=$request->input('lang');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


       $check_lang=setting::where('user_id',$check_token['id'])->value('language');

       //1 =>arabic     2=>english
            if($check_lang ==1)
            {
            $show=Language::select('id','name')->get();
            }else{
            $show=Language::select('id','E_name as name')->get();
            }

    

       if(count($show) > 0){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
   
   
   public function show_complain_questions(Request $request)
   {
     try{
       $token=$request->input('user_token');
        //$lang=$request->input('lang');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


       $check_lang=setting::where('user_id',$check_token['id'])->value('language');

       //1 =>arabic     2=>english
            if($check_lang ==1)
            {
            $show=complain_question::select('id','name')->get();
            }else{
            $show=complain_question::select('id','E_name as name')->get();
            }

    

       if(count($show) > 0){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }

   //choose language

    public function choose_language(Request $request)
   {
     try{
       $token=$request->input('user_token');
        $lang=$request->input('language_id');
          
              $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

       $update=setting::where('user_id',$check_token['id'])->update([
        'language'=>$lang,
        'updated_at'=>$dateTime
         
       ]);
       $select=setting::select('language.name')->join('language','setting.language','=','language.id')
       ->where('user_id',$check_token['id'])->get();

       if($update==true){

            $message['data']=$select;  
            $message['error']=0;
            $message['message']='you choose language successfully';
       }else{

        
            $message['error']=1;
            $message['message']='error in choose language';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }

     public function make_compliance(Request $request)
    {
      try{
           $token=$request->input('user_token');
           $id=$request->input('user_id');
           $order=$request->input('order_id');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
         
         $comment=$request->input('notes');
          $reason=$request->input('reason');
          $image=$request->file('image');
          $details=$request->input('details');
        $list=json_decode($request->input('answers'),true);//[{"id":1,"answer":"yes"},{"id":2,"answer":"no"},{"id":3,"answer":"yes"},{"id":4,"answer":"no"}]
          
          
           if(isset($image)) {
               
                 foreach($image as $myimg){
            
                    $new_name = $myimg->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                    $destinationPath_id = 'uploads/compliance';
                    $myimg->move($destinationPath_id, $savedFileName);
        
                    $images[]= $savedFileName;
       
                    }
                      
                 }else{
                    $images =NULL;     
                 }

          $created_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
           

           if($check_token['state']==2){
        
            $insert=new Compliance;
            $insert->user_id=$id;
            $insert->driver_id=$check_token['id'];
            $insert->order_id=$order;
            $insert->content=$comment;
            $insert->reason=$reason;
           
            $insert->details=$details;
            $insert->who_complain='driver';
            $insert->state='wait';
            $insert->created_at=$dateTime;
            $insert->save();
          }else{

            $insert=new Compliance;
            $insert->user_id=$check_token['id'];
            $insert->driver_id=$id;
            $insert->order_id=$order;
            $insert->content=$comment;
             $insert->reason=$reason;
            $insert->state='wait';
            $insert->details=$details;
            $insert->who_complain='user';
            $insert->created_at=$dateTime;
            $insert->save();

          }
          $my_arr=array();
         if(isset($image)){
              foreach ($images as $key ) {
          
               $my_arr[]=array('complain_id'=>$insert->id,'image'=>$key,'created_at'=>$dateTime);
              }
         }
              
              $ans_arr=array();
              
              for($i=0;$i<count($list);$i++){
                  
                  $ans_arr[]=array('complain_id'=>$insert->id,'question_id'=>$list[$i]['id'],'answer'=>$list[$i]['answer'],'user_id'=>$check_token['id'],'created_at'=>$dateTime);
              }

        
          if($insert==true){
                     $insertimg=compliance_image::insert($my_arr);
                     $insert_answers=complain_answer::insert($ans_arr);
                     
                     $update_order=Requests::where('id',$order)->update([
                         
                          'state'=>8,
                          'updated_at'=>$dateTime
                         ]);

                  $message['data']=$insert;
                   $message['error']=0;
                   $message['message']='make rate';

          }else{
                   $message['data']=$insert;
                   $message['error']=1;
                   $message['message']='can not make rate';
          }
        
      }else{
        $message['error']=3;
        $message['message']='this token is not exist';
      }
      }catch(Exception  $ex){
         $message['error']=2;
          $message['message']='error'.$ex->getMessage();

      }
      return response()->json($message);
    } 


    
}
