<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Requests;
use App\Notification;
use carbon\carbon;
use App\User;
use App\Messages;
use App\Wallet;
use App\Rating;
use App\Offers;
use App\About;
use App\Bill;

define( 'API_ACCESS_KEY12','AAAAnSDEgLc:APA91bFisJ6mf6QmnpUvaC48ND3u4u_ULaQNnR4fxCRur392hErem3qi3fjRQ05wXg45D8NrhehK4Bp8h1f_uUGW0YbSQBmxDRWg3pi9PBkHvYnE8GHqjYs3JRTmCab08jUJQph9luxS');

class OfferController extends Controller
{
     public $message=array();

//for driver
     public function order_deliverd(Request $request)
    {
       try{
        
            $token=$request->input('user_token');
         
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

            $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

            $id=$request->input('request_id');


            $check_request=Requests::where('id',$id)->first();
        
            $update=Requests::where('id',$id)->update([
                                                     'state'=>4,
                                                     'updated_at'=>$dateTime
                                                     ]);
           
           
            $state_data=Requests::select('state.name')->join('state','requestes.id','=','state.id')->where('requestes.id',$id)->first();
        
          if( $update ==true){
              
                          $mess=new Messages;
                          $mess->receiver_id=$check_request['user_id'];
                          $mess->sender_id=$check_token['id'];
                          $mess->request_id=$id;
                          $mess->message=' تم التسليم';
                          $mess->created_at=$dateTime;
                          $mess->save();
                          
                          
            try{       
       //  $cc = 0;
         
            $title ='the agent  '.$check_token['first_name'].' deliverd the order';
            $body =$check_token['first_name'].'  deliverd   your order '.$id;
            $title_ar = "سليم الطلب ". $check_token['first_name'] ." المندوب ";
            $body_ar = $id." سلم طلبك".$check_token['first_name'];
         


       $get_user_token =User::select('id','firebase_token')->where('id', '=',$check_request['user_id'])->first();
       
          // return $get_user_token[0]->firebase_token;


                     ////////////////////////////////////////////////////////
                     
                       $msg = array
                              (
                        'body'  => $body,
                        'title' => $title,
                        'click_action' => '1',
                        'driver_id'  => "$check_token->id",
                        'request_id' =>"$id",
                              );
                      $fields = array
                          (
                            'to'    => $get_user_token['firebase_token'],
                            'data' => $mg = array(
                                                    'click_action' => '1',
                                                    'driver_id'  => "$check_token->id",
                                                    'request_id' =>"$id",
                                                ),
                            'notification'  => $msg
                          );
                      
                                       

                      $headers = array
                          (
                            'Authorization: key=' . API_ACCESS_KEY12,
                            'Content-Type: application/json'
                          );
                    #Send Reponse To FireBase Server  
                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        $result = curl_exec($ch );
                        //echo $result;
                        curl_close( $ch );
                 
        
        
            $save=new Notification;
            $save->title=$title;
            $save->body=$body;
            $save->title_ar = $title_ar;
            $save->body_ar = $body_ar;
            $save->sender=$check_token['id'];
            $save->is_read=0;
             $save->request_id=$id;
            $save->user_id=$check_request['user_id'];
            $save->created_at=$dateTime;
            $save->save();
            
         }catch(Exception $ex){
              $message['error']=4;
               $message['message']='error in send notification';
         }
         
            $message['data'] = $fields;
            $message['error']=0;
            $message['message']='order  is delivered';

          }else{
       
            $message['error']=1;
            $message['message']='error in receive order';
          }
        }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
        }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
    
       public function order_received(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
         
           
             $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

           $id=$request->input('request_id');


        $check_request=Requests::where('id',$id)->first();


           $update=Requests::where('id',$id)->update([
             'state'=>3,
             'updated_at'=>$dateTime

           ]);
           
                $state_data=Requests::select('state.name')->join('state','requestes.id','=','state.id')->where('requestes.id',$id)->first();
        
          if( $update ==true){
              
                          $mess=new Messages;
                         $mess->receiver_id=$check_request['user_id'];
                          $mess->sender_id=$check_token['id'];
                          $mess->request_id=$id;
                          $mess->message='تم الاستلام';
                           
                       
                          $mess->created_at=$dateTime;
                          $mess->save();
                          
                          
                                    try{       
       //  $cc = 0;
         
        $title ='the agent  '.$check_token['first_name'].' received the order';
        $body =$check_token['first_name'].'  received   your order '.$id;
        $title_ar =" استلم الطلب ".$check_token['first_name'] ." المندوب " ;
        $body_ar =         $id.  " استلم طلبك ".$check_token['first_name']   ;                

       $get_user_token =User::select('id','firebase_token')->where('id', '=',$check_request['user_id'])->first();
       
          // return $get_user_token[0]->firebase_token;


                     ////////////////////////////////////////////////////////
                     
                       $msg = array
                              (
                        'body'  => $body,
                        'title' => $title,
                        'state'=>$state_data,
                                  
                              );
                      $fields = array
                          (
                            'to'    => $get_user_token['firebase_token'],
                            'notification'  => $msg
                          );
                      
                                       

                      $headers = array
                          (
                            'Authorization: key=' . API_ACCESS_KEY12,
                            'Content-Type: application/json'
                          );
                    #Send Reponse To FireBase Server  
                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        $result = curl_exec($ch );
                        //echo $result;
                        curl_close( $ch );
                 
        
        
            $save=new Notification;
            $save->title=$title;
            $save->body=$body;
            $save->title_ar = $title_ar;
            $save->body_ar = $body_ar;
            $save->sender=$check_token['id'];
            $save->is_read=0;
             $save->request_id=$id;
            $save->user_id=$check_request['user_id'];
            $save->created_at=$dateTime;
            $save->save();
            
         }catch(Exception $ex){
              $message['error']=4;
               $message['message']='error in send notification';
         }
         
            $message['error']=0;
            $message['message']='order  is received';

          }else{
       
            $message['error']=1;
            $message['message']='error in receive order';
          }
        }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
        }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
    
    
     public function arrived_site(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
         
           
             $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

           $id=$request->input('request_id');
           
           
           
               $update=Requests::where('id',$id)->update([
             'state'=>7,
             'updated_at'=>$dateTime

           ]);
        
          if( $update ==true){
              
       $state_data=Requests::select('state.name')->join('state','requestes.id','=','state.id')->where('requestes.id',$id)->first();


        $user=Requests::where('id',$id)->value('user_id');
        
        



                  try{       
       //  $cc = 0;
         
        $title ='the agent '.$check_token['first_name'].' arrived to the  site';
        $body =$check_token['first_name'].' arrived to the  site to deliver  your order '.$id.' and you should pay off th money of your order';
        $title_ar =  "وصل إلى الموقع ".$check_token['first_name']. " المندوب";
        $body_ar =   " وعليك سداد أموال طلبك ". $id." وصل إلى الموقع لتسليم طلبك " . $check_token['first_name'];

       $get_user_token =User::select('firebase_token')->where('id', '=',$user)->first();
       
          // return $get_user_token[0]->firebase_token;


                     ////////////////////////////////////////////////////////
                     
                       $msg = array
                              (
                        'body'  => $body,
                        'title' => $title,
                        'state'=>$state_data,
                                  
                              );
                      $fields = array
                          (
                            'to'    => $get_user_token['firebase_token'],
                            'notification'  => $msg
                          );
                      
                                       

                      $headers = array
                          (
                            'Authorization: key=' . API_ACCESS_KEY12,
                            'Content-Type: application/json'
                          );
                    #Send Reponse To FireBase Server  
                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        $result = curl_exec($ch );
                        //echo $result;
                        curl_close( $ch );
                 
        
        
            $save=new Notification;
            $save->title=$title;
            $save->body=$body;
            $save->title_ar = $title_ar;
            $save->body_ar = $body_ar;
            $save->sender=$check_token['id'];
            $save->is_read=0;
             $save->request_id=$id;
            $save->user_id=$user;
            $save->created_at=$dateTime;
            $save->save();
            
            
                          $mess=new Messages;
                          $mess->receiver_id=$user;
                          $mess->sender_id=$check_token['id'];
                          $mess->request_id=$id;
                          $mess->message='تم الوصول الي الموقع ';
                           ;
                       
                          $mess->created_at=$dateTime;
                          $mess->save();
            
            
            
         }catch(Exception $ex){
              $message['error']=4;
               $message['message']='error in send notification';
              
         }
         
         
            $message['error']=0;
            $message['message']='notififcation sent';
                 
          }else{
              
              $message['error']=1;
            $message['message']='error in update state';
                 
          }

        }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
        }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
    
    
    
    
    
    
    
        //driver
     public function Withdraw_Fromorder (Request $request)
    {
       try{
        
          $token=$request->input('user_token');
          $cancel_reason=$request->input('cancel_reason');
           
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

             $id=$request->input('request_id');
             
             
             $updated_at = carbon::now()->toDateTimeString();
                $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
          
          
          
            $offer=Offers::where([['request_id',$id],['state','accepted'],['driver_id',$check_token['id']]])->first();
            
            
          
          $accpted_time=$offer['accepted_time'];
          $change_time=date('Y-m-d H:i:s',strtotime('+10 minutes',strtotime($accpted_time)));
        
    
          
          if($dateTime < $change_time){



          $cancel_offer=Offers::where([['request_id',$id],['driver_id',$check_token['id']]])->update([

                  'state'=>'canceled',
                  'cancel_reason'=>$cancel_reason,
                  'updated_at'=>$dateTime
            
                ]);

        
        
          if($cancel_offer ==true){

                    $update_user=User::where('id',$check_token['id'])
                 ->increment('cancel_times',1);
        
                   $update_request=Requests::where('id',$id)
                 ->update([
                  'state'=>1,
                  'updated_at'=>$dateTime
        
                   ]);
           
           
         
            
        $user=Requests::where('id',$id)->value('user_id');
        
        
        
        
        
          $delete_chat=Messages::where([['sender_id',$check_token['id']],['receiver_id',$user],['request_id',$id] ])
                                ->orwhere([['sender_id',$user],['receiver_id',$check_token['id']],['request_id',$id]])
                                
                                ->delete();

                 
                     
                  $delete_bill=Bill::where('request_id',$id)->delete();
                   
                  $state_data=Requests::select('state.name')->join('state','requestes.id','=','state.id')->where('requestes.id',$id)->first();

                   
           
             try{       
       //  $cc = 0;
         
        $title ='sorry the agent '.$check_token['first_name'].' cancel his offer';
        $body =$check_token['first_name'].' has canceled his offer on your order '.$id;
        $title_ar =   " إلغاء عرضه ".$check_token['first_name']." آسف المندوب ";
        $body_ar = $cancel_reason." سبب الإلغاء ".$id." ألغى عرضه على طلبك " .$check_token['first_name'];

       $get_user_token =User::select('firebase_token')->where('id', '=',$user)->first();
       
          // return $get_user_token[0]->firebase_token;


                     ////////////////////////////////////////////////////////
                     
                       $msg = array
                              (
                        'body'  => $body,
                        'title' => $title,
                        'state'=>$state_data,
                                  
                              );
                      $fields = array
                          (
                            'to'    => $get_user_token['firebase_token'],
                            'notification'  => $msg
                          );
                      
                                       

                      $headers = array
                          (
                            'Authorization: key=' . API_ACCESS_KEY12,
                            'Content-Type: application/json'
                          );
                    #Send Reponse To FireBase Server  
                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        $result = curl_exec($ch );
                        //echo $result;
                        curl_close( $ch );
                 
        
        
            $save=new Notification;
            $save->title=$title;
            $save->body=$body.'  reason of cancel is '.$cancel_reason;
            $save->title_ar = $title_ar;
            $save->body_ar = $body_ar;
            $save->sender=$check_token['id'];
            $save->is_read=0;
             $save->request_id=$id;
            $save->user_id=$user;
            $save->created_at=$dateTime;
            $save->save();
            
            
         }catch(Exception $ex){
              $message['error']=4;
               $message['message']='error in send notification';
              
         }


            $message['error']=0;
            $message['message']='offer is canceled sucess';

          }else{
            
            $message['error']=1;
            $message['message']='error in cancel offer';
          }
          }else{
             $message['error']=5;
            $message['message']='sorry you can\'t withdrowal from  in cancel offer';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }



    public function change_agent (Request $request)
    {
       try{
        
          $token=$request->input('user_token');
           
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

             $id=$request->input('request_id');
             
             $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
          
          
          $offer=Offers::where([['request_id',$id],['state','accepted']])->first();
          
          
          	  $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at))); 
          
          $accpted_time=$offer['accepted_time'];
          $change_time=date('Y-m-d H:i:s',strtotime('+10 minutes',strtotime($accpted_time)));
        
   
          
          if($dateTime < $change_time){

    $offer_data=Offers::where([['request_id',$id],['offers.state','accepted']])->first();

    $driver=Offers::where([['request_id',$id],['offers.state','accepted']])->value('driver_id');
    
            $delete_offer=Offers::where('id',$offer_data['id'])->delete();
            
    
          $cancel_offer=Offers::where('request_id',$id)->update([

                  'state'=>'wait',
                  'updated_at'=>$dateTime
            
                ]);

        
                  if($cancel_offer ==true){
        
        
                
        
                           $update_request=Requests::where('id',$id)
                         ->update([
                          'state'=>1,
                          'updated_at'=>$dateTime
                
                           ]);
                                       
                    $delete_chat=Messages::where([['sender_id',$check_token['id']],['receiver_id',$driver],['request_id',$id] ])
                                ->orwhere([['sender_id',$driver],['receiver_id',$check_token['id']],['request_id',$id]])
                                
                                ->delete();

                 
                     
                   $delete_bill=Bill::where('request_id',$id)->delete();
        
                    $message['error']=0;
                    $message['message']='agent is changed sucess';
        
                  }else{
                    
                    $message['error']=1;
                    $message['message']='error in change agent';
                  }
          }else{
               $message['error']=4;
               $message['message']='sorry you can\'t change this agent'; 
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


      //for driver
    public function driver_active_Orders(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
         
          $check_token=User::where('user_token',$token)->first();
      
          if($request->has('user_token') && $check_token !=NULL){
   
            $request_data=DB::select('select requestes.id,requestes.shop_id,shopes.name,requestes.user_id,users.first_name,users.image,users.phone,requestes.description,requestes.delivery_time,requestes.state,requestes.created_at
         FROM requestes
                left join shopes ON requestes.shop_id=shopes.id
                join users ON requestes.user_id=users.id
                join offers ON requestes.id=offers.request_id
                where  offers.driver_id ='.$check_token['id'].' AND offers.state ="accepted" AND requestes.state="processed"'); 

  
          /*  $request_data=Requests::select('requestes.id','requestes.shop_id','shopes.name','requestes.user_id','users.first_name','users.image','users.phone','requestes.description','requestes.delivery_time','requestes.state','requestes.created_at')
                ->join('shopes','requestes.shop_id','=','shopes.id')
                ->join('users','requestes.user_id','=','users.id')
                ->where('requestes.id',$id)
                ->first();*/

  


        
          if( $request_data !=null){


            $message['data']=$request_data;
            $message['error']=0;
            $message['message']='show  order data';

          }else{
            $message['data']=$request_data;
            $message['error']=1;
            $message['message']='no order data';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
   


     public function driver_finished_Orders(Request $request)
    {
       try{
        
          $token=$request->input('user_token');   
          
          $check_token=User::where('user_token',$token)->first();
           
          if($request->has('user_token') && $check_token !=NULL){

   
            $request_data=DB::select('select requestes.id,requestes.shop_id,shopes.name,requestes.user_id,users.first_name,users.image,users.phone,requestes.description,requestes.delivery_time,requestes.state,requestes.created_at
         FROM requestes
                left join shopes ON requestes.shop_id=shopes.id
                join users ON requestes.user_id=users.id
                join offers ON requestes.id=offers.request_id
                where  offers.driver_id ='.$check_token['id'].' AND offers.state ="accepted" AND requestes.state="received"'); 

  
          /*  $request_data=Requests::select('requestes.id','requestes.shop_id','shopes.name','requestes.user_id','users.first_name','users.image','users.phone','requestes.description','requestes.delivery_time','requestes.state','requestes.created_at')
                ->join('shopes','requestes.shop_id','=','shopes.id')
                ->join('users','requestes.user_id','=','users.id')
                ->where('requestes.id',$id)
                ->first();*/

  


        
          if(count($request_data)>0){


            $message['data']=$request_data;
            $message['error']=0;
            $message['message']='show  order data';

          }else{
            $message['data']=$request_data;
            $message['error']=1;
            $message['message']='no order data';
          }
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }


   public function driver_Datacounts(Request $request){
      try{
        
          $token=$request->input('user_token');
         
         
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){



            $credit=Wallet::where('user_id',$check_token['id'])->value('credit');

            $requests=Requests::join('offers','requestes.id','=','offers.request_id')->
            where([['offers.driver_id',$check_token['id']],['requestes.type',1],['requestes.state','received'],['offers.state','accepted']])->count();

          //  $journy=Requests::where([['user_id',$check_token['id']],['type',2]])->count();

            $rate=Rating::where([['driver_id',$check_token['id']],['who_rate','user']])->count();



          
             $message['credit']=$credit;
             $message['requests']=$requests;
             // $message['journy']=$journy;
              $message['rate']=$rate;
          
           
         
            

          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);

  }
  
  
   public function user_received_order (Request $request)
    {
       try{
        
          $token=$request->input('user_token');
         
           
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));

          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

            $id=$request->input('request_id');
            $driver_id = $request->input('driver_id');

            $check_request=Requests::where('id',$id)->first();
        
            $driver_data = \App\User::where('id' , $driver_id)->first();
            

            $update=Requests::where('id',$id)->update([
                                                     'state'=>9,
                                                     'updated_at'=>$dateTime
                                        
                                                   ]);
           
           
                $state_data=Requests::select('state.name')->join('state','requestes.id','=','state.id')->where('requestes.id',$id)->first();
        
          if( $update ==true){
              
                          $mess=new Messages;
                          $mess->receiver_id = $driver_id;
                          $mess->sender_id=$check_token['id'];
                          $mess->request_id=$id;
                          $mess->message= 'تم استلام الطلب من المندوب';
                       
                          $mess->created_at=$dateTime;
                          $mess->save();
                          
                          
                                    try{       
       //  $cc = 0;
         
        $title ='the user  '.$check_token['first_name'].' received the order';
        $body =$check_token['first_name'].'  received  your order '.$id;
        $title_ar = " استلم الطلب ".$check_token['first_name']." العميل ";
        $body_ar = $id. " استلم الطلب ".$check_token['first_name']     ;         
         
         


       $get_user_token =User::select('id','firebase_token')->where('id', '=',$check_request['user_id'])->first();
       
          // return $get_user_token[0]->firebase_token;


                     ////////////////////////////////////////////////////////
                     
                       $msg = array
                              (
                        'body'  => $body,
                        'title' => $title,
                        'click_action' => '9'
                              );
                      $fields = array
                          (
                            'to'    => $get_user_token['firebase_token'],
                            'data' => $msg,
                            'notification'  => $msg
                          );
                      
                                       

                      $headers = array
                          (
                            'Authorization: key=' . API_ACCESS_KEY12,
                            'Content-Type: application/json'
                          );
                    #Send Reponse To FireBase Server  
                        $ch = curl_init();
                        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                        curl_setopt( $ch,CURLOPT_POST, true );
                        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                        $result = curl_exec($ch );
                        //echo $result;
                        curl_close( $ch );
                 
        
        
            $save=new Notification;
            $save->title=$title;
            $save->body=$body;
            $save->title_ar = $title_ar;
            $save->body_ar = $body_ar;
            $save->sender=$check_token['id'];
            $save->is_read=0;
             $save->request_id=$id;
            $save->user_id=$driver_id;
            $save->created_at=$dateTime;
            $save->save();
            
         }catch(Exception $ex){
              $message['error']=4;
              $message['message']='error in send notification';
         }
         
            $message['error']=0;
            $message['message']='order  is delivered';

          }else{
       
            $message['error']=1;
            $message['message']='error in receive order';
          }
        }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
        }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
  
  
  
  
  
  
  
  
  
  
  
  
  
}
