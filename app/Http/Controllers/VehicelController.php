<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use carbon\carbon;
use App\setting;
use App\Payment;



class VehicelController extends Controller
{
    public $message=array();

   //payments

  public function show_payments(Request $request)
  {
      try{
          $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


          	  $check_lang=setting::where('user_id',$check_token['id'])->value('language');

       //1 =>arabic     2=>english
            if($check_lang ==1)
            {
            $about=Payment::select('id','name','created_at','updated_at')->get();
            }else{
            $about=Payment::select('id','E_name as name','created_at','updated_at')->get();
            }

         

                  if( count($about)>0){
                        $message['data']= $about;
                         $message['error']=0;
                        $message['message']='show payment data';
                    }else{
                         $message['data']= $about;
                         $message['error']=1;
                         $message['message']='no data ';
                    }

               }else{
         	  $message['error']=3;
               $message['message']='this token is not exist';
         }

            }catch(Exception $ex){
                 $message['error']=2;
                 $message['message']='error'.$ex->getMessage();
            }  
         

       
         return response()->json($message);
  }

  public function show_paymentByid(Request $request)
  {
       try{
       
         $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
           $id=$request->input('id');


             $check_lang=setting::where('user_id',$check_token['id'])->value('language');

       //1 =>arabic     2=>english
            if($check_lang ==1)
            {
            $select=Payment::select('id','name','created_at','updated_at')
            ->where('id',$id)->first();
            }else{
            $select=Payment::select('id','E_name as name','created_at','updated_at')
             ->where('id',$id)->first();
            }


          if( $select !=null){
              $message['data']=$select;
              $message['error']=0;
              $message['message']='show payment';
          }else{
              $message['data']=$select;
              $message['error']=1;
              $message['message']='no data';
          }
            }else{
         	  $message['error']=3;
               $message['message']='this token is not exist';
         }
        }catch(Exception $ex){
             
              $message['error']=2;
              $message['message']='error'.$ex->getMessage();

        }
       return response()->json($message);
    }
    
    
    
    //show payment by id dashboard ::
    
    public function show_paymentByid_dash(Request $request){
        try{
       
            $token=$request->input('user_token');
          
            $check_token=User::where('user_token',$token)->first();
          
          
            if($request->has('user_token') && $check_token !=NULL){
                
                $id=$request->input('id');



       
               $select=Payment::select('id','name','E_name','created_at','updated_at')->where('id',$id)->first();
           
                if( $select !=null){
                    $message['data']=$select;
                    $message['error']=0;
                    $message['message']='show payment';
                }else{
                    $message['data']=$select;
                    $message['error']=1;
                    $message['message']='no data';
                }
            }else{
         	   $message['error']=3;
               $message['message']='this token is not exist';
            }
        }catch(Exception $ex){
             
              $message['error']=2;
              $message['message']='error'.$ex->getMessage();

        }
       return response()->json($message);
    }
    
  public function delete_payment(Request $request)
  {
       try{
       	 $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
       
           $id=$request->input('id');

            $select=Payment::where('id',$id)->delete();

          if( $select ==true){
             
              $message['error']=0;
              $message['message']='delete success';
          }else{
              
              $message['error']=1;
              $message['message']='error in delete';
          }
            }else{
         	  $message['error']=3;
               $message['message']='this token is not exist';
         }
        }catch(Exception $ex){
             
              $message['error']=2;
              $message['message']='error'.$ex->getMessage();

        }
       return response()->json($message);
    }
    
  public function insert_payment(Request $request)
  {
       try{
          $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

           $name=$request->input('name');
            $e_name=$request->input('E_name');
               

           $created_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

          $check=Payment::where('name',$name)->orwhere('E_name',$e_name)->get();
          if(count($check)>0){
              $message['error']=4;
              $message['message']='this payment is exist';
          }else{
                $insert=new Payment;
            $insert->name=$name;
             $insert->E_name=$e_name;
            $insert->created_at=$dateTime;
            $insert->save();

          if( $insert ==true){
              $message['data']=$insert;
              $message['error']=0;
              $message['message']='insert payment';
          }else{
              $message['data']=$insert;
              $message['error']=1;
              $message['message']='error in insert data';
          }

          }

           
          
           }else{
         	  $message['error']=3;
               $message['message']='this token is not exist';
         }
        }catch(Exception $ex){
             
              $message['error']=2;
              $message['message']='error'.$ex->getMessage();

        }
       return response()->json($message);
    }
    
    
  public function update_payment(Request $request)
  {
       try{

       	 $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
           $id=$request->input('id');
           $name=$request->input('name');
            $e_name=$request->input('E_name');

                $created_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

           
            $update=Payment::where('id',$id)->update([
                'name'=>$name,
                'E_name'=>$e_name,
                'updated_at'=>$dateTime
                
                ]);
            
             $select=Payment::where('id',$id)->first();
         
          if( $update ==true){
              $message['data']=$select;
              $message['error']=0;
              $message['message']='insert payment';
          }else{
              $message['data']=$select;
              $message['error']=1;
              $message['message']='error in insert data';
          }
           }else{
         	  $message['error']=3;
               $message['message']='this token is not exist';
         }
        }catch(Exception $ex){
             
              $message['error']=2;
              $message['message']='error'.$ex->getMessage();

        }
       return response()->json($message);
    }


}
