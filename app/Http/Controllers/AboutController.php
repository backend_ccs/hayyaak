<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Privacy_policy;
use App\User;
use App\setting;
use App\About;
use App\App_rating;
use App\complain_reason;
use App\Rating;
use carbon\carbon;
use App\Notification;

class AboutController extends Controller
{
    public $message=array();
    
    
   public function show_complain_reasons(Request $request)
   {
     try{
           $token=$request->input('user_token');
        //$lang=$request->input('lang');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


       $check_lang=setting::where('user_id',$check_token['id'])->value('language');
       

       //1 =>arabic     2=>english
                       if($check_lang ==1)
                        {
                            if($check_token['state']==2){
                                 $show=complain_reason::select('id','text','created_at','updated_at')->where('for_who','for_driver')->get(); 
                            }else{
                                
                                 $show=complain_reason::select('id','text','created_at','updated_at')->where('for_who','for_user')->get(); 
                            }
                       
                        }else{
                               if($check_token['state']==2){
                                 $show=complain_reason::select('id','E_text as text','created_at','updated_at')->where('for_who','for_driver')->get(); 
                            }else{
                                
                                 $show=complain_reason::select('id','E_text as text','created_at','updated_at')->where('for_who','for_user')->get(); 
                            }
                      
                        }

    

               if($show !=null){
                    $message['data']=$show;
                    $message['error']=0;
                    $message['message']='show data';
               }else{
        
                    $message['data']=$show;
                    $message['error']=1;
                    $message['message']='no data';
               }
          }else{
              
              $message['error']=3;
              $message['message']='this token is not exist'; 
          } 
                
     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }



    public function show_policy(Request $request)
   {
     try{
       $token=$request->input('user_token');
        //$lang=$request->input('lang');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


       $check_lang=setting::where('user_id',$check_token['id'])->value('language');

       //1 =>arabic     2=>english
            if($check_lang ==1)
            {
            $show=Privacy_policy::select('id','text','created_at','updated_at')->get();
            }else{
            $show=Privacy_policy::select('id','E_text as text','created_at','updated_at')->get();
            }

    

       if($show !=null){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
         }else{
             $message['error']=3;
             $message['message']='this token is not exit';
         }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }


/// api show privacy for dashboard:: 

    public function show_policy_dash(Request $request){
        try{
            $token=$request->input('user_token');
              
            $check_token=User::select('id')->where('user_token',$token)->first();
              
            if($request->has('user_token') && $check_token !=NULL){
    
    
                $show = Privacy_policy::select('id','text as A_text','E_text','created_at','updated_at')->get();
     
               if($show !=null){
                    $message['data']=$show;
                    $message['error']=0;
                    $message['message']='show data';
               }else{
        
                    $message['data']=$show;
                    $message['error']=1;
                    $message['message']='no data';
               }
            }else{
                $message['error']=3;
                $message['message']='this token is not exit';
            }

        }catch(Exception $ex){
             
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
        }
        
        return response()->json($message);
   }


    public function update_policy(Request $request)
   {
     try{
       $token=$request->input('user_token');
        //$lang=$request->input('lang');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
            $text=$request->input('A_text');
            $e_text=$request->input('E_text');
               

               $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
         
          $update=Privacy_policy::where('id',1)->update([
           'text'=>$text,
           'E_text'=>$e_text,
           'updated_at'=>$dateTime

          ]);

     
    

       if($update ==true){


            $message['error']=0;
            $message['message']='update data successfully';
       }else{

          
            $message['error']=1;
            $message['message']='error in update';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
   
   

 public function show_use_conditions(Request $request)
   {
     try{
          $token=$request->input('user_token');

          

          
    



            $show=About::select('id','use_conditions as A_text','E_use_conditions as E_text','created_at','updated_at')->get();
            

    

       if(count($show)>0 ){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
  

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }



 public function show_oneconditions(Request $request)
   {
     try{
       $token=$request->input('user_token');
        //$lang=$request->input('lang');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){



       //1 =>arabic     2=>english
          
            $show=About::select('id','use_conditions as text','E_use_conditions as E_text','created_at','updated_at')->first();
           

    

       if($show !=null){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
   public function update_use_conditions(Request $request)
   {
     try{
       $token=$request->input('user_token');
        $text=$request->input('A_text');
        $e_text=$request->input('E_text');
          $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
         
  
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


       
            $update=About::where('id',1)->update([
                'use_conditions'=>$text,
                'E_use_conditions'=>$e_text,
                'updated_at'=>$dateTime

            ]);

        
    

       if($update ==true ){
     
            $message['error']=0;
            $message['message']='update data successfully';
       }else{

           
            $message['error']=1;
            $message['message']='error in update';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }

    

 public function about_us(Request $request)
   {
     try{
       $token=$request->input('user_token');
        //$lang=$request->input('lang');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


   
            $show=About::select('id','name','E_name','address','details','E_details')->get();
            

    

       if($show !=null ){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }


//show about us for dddashboard :


 public function about_us_dash(Request $request){
    try{
          $token=$request->input('user_token');
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

            $show = About::select('id','name','E_name','address','details','E_details')->first();
          
           if($show !=null ){
                $message['data']=$show;
                $message['error']=0;
                $message['message']='show data';
           }else{
    
                $message['data']=$show;
                $message['error']=1;
                $message['message']='no data';
           }
        }else{
            $message['error']=3;
            $message['message']='this token is not exit';
        }

    }catch(Exception $ex){
         
        $message['error']=2;
        $message['message']='error'.$ex->getMessage();
    }

    return response()->json($message);
   }


//update about
    public function update_about_us(Request $request)
   {
     try{
       $token=$request->input('user_token');
        $name=$request->input('name');
         $e_name=$request->input('E_name');
          $address=$request->input('address');
            $details=$request->input('details');
            $e_details=$request->input('E_details');

          $updated_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
         
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){


            $update=About::where('id',1)->update([
              'name'=>$name,
              'E_name'=>$e_name,
              
              'address'=>$address,
              'details'=>$details,
             
              'E_details'=>$e_details,
              'updated_at'=>$dateTime]);
         
    

       if($update==true){
            
            $message['error']=0;
            $message['message']='update data successfully';

       }else{

         
            $message['error']=1;
            $message['message']='error in update';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }

 
    public function driver_rateUser(Request $request)
    {
      try{
        $token=$request->input('user_token');
           $id=$request->input('user_id');
           $order=$request->input('request_id');
          
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

         $rate_num=$request->input('rate_number');
         $comment=$request->input('comment');


          $created_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
           
     $state=User::where('id',$id)->value('state');

      if($state ==2){

          $who='user';

           $check=Rating::where([['user_id',$check_token['id']],['driver_id',$id],['who_rate',$who],['request_id',$order]])->get();
     //   return count($check);
         if(count($check) > 0){

             $message['error']=4;
             $message['message']='you rated befor';

           }else{
            $insert=new Rating;
            $insert->user_id=$check_token['id'];
            $insert->driver_id=$id;
            $insert->rate=$rate_num;
            $insert->who_rate=$who;
            $insert->request_id=$order;
            $insert->comment=$comment;

            $insert->created_at=$dateTime;
            $insert->save();

           
        
          if($insert==true){
            
                  $message['data']=$insert;
                   $message['error']=0;
                   $message['message']='make rate';
                   
                    try{       
                   //  $cc = 0;
                     
                
            $title ='the user'.$check_token['first_name'].' rate  you';
            $body =$check_token['first_name'].'rate  you on  order '.$order;
            $title_ar = " قيمك ".$check_token['first_name'];
            $body_ar =$order. " قيمك على طلب رقم "   . $check_token['first_name'];
            
                 define( 'API_ACCESS_KEY12','AAAAnSDEgLc:APA91bFisJ6mf6QmnpUvaC48ND3u4u_ULaQNnR4fxCRur392hErem3qi3fjRQ05wXg45D8NrhehK4Bp8h1f_uUGW0YbSQBmxDRWg3pi9PBkHvYnE8GHqjYs3JRTmCab08jUJQph9luxS');
            
            
                   $get_user_token =User::select('firebase_token')->where('id', '=',$id)->first();
                   
                      // return $get_user_token[0]->firebase_token;
            
            
                                 ////////////////////////////////////////////////////////
                                 
                                   $msg = array
                                          (
                                    'body'  => $body,
                                    'title' => $title,
                                              
                                          );
                                  $fields = array
                                      (
                                        'to'    => $get_user_token['firebase_token'],
                                        'notification'  => $msg
                                      );
                                  
                                                   
            
                                  $headers = array
                                      (
                                        'Authorization: key=' . API_ACCESS_KEY12,
                                        'Content-Type: application/json'
                                      );
                                #Send Reponse To FireBase Server  
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    //echo $result;
                                    curl_close( $ch );
                             
                    
                    
                        $save=new Notification;
                        $save->title=$title;
                        $save->body=$body;
                        $save->title_ar = $title_ar;
                        $save->body_ar = $body_ar;
                        $save->sender=$check_token['id'];
                        $save->is_read=0;
                         $save->request_id=$order;
                        $save->user_id=$id;
                        $save->created_at=$dateTime;
                        $save->save();
                        
                        
                     }catch(Exception $ex){
                          $message['error']=4;
                           $message['message']='error in send notification';
                          
                     }
                   
                   
                   
                   
                   
                   
                   
                   
                   

          }else{
                   $message['data']=$insert;
                   $message['error']=1;
                   $message['message']='can not make rate';
          }
        }

      }else{
        $who='driver';

           $check=Rating::where([['user_id',$id],['driver_id',$check_token['id']],['who_rate',$who],['request_id',$order]])->get();
     //   return count($check);
             if(count($check) > 0){

                 $message['error']=4;
                 $message['message']='you rated befor';


           }else{

            $insert=new Rating;
            $insert->user_id=$id;
            $insert->driver_id=$check_token['id'];
            $insert->rate=$rate_num;
            $insert->who_rate=$who;
            $insert->comment=$comment;
            $insert->request_id=$order;
            $insert->created_at=$dateTime;
            $insert->save();

           
        
          if($insert==true){
            
                  $message['data']=$insert;
                   $message['error']=0;
                   $message['message']='make rate';
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                          try{       
                   //  $cc = 0;
                     
                
            $title ='the agent'.$check_token['first_name'].' rate  you';
            $body =$check_token['first_name'].'rate  you on  order '.$order;
            $title_ar =  " قيمك ".$check_token['first_name']. " المندوب ";
            $body_ar = $order." قيمك على طلب رقم ".$check_token['first_name'];
                     
                 define( 'API_ACCESS_KEY12','AAAAnSDEgLc:APA91bFisJ6mf6QmnpUvaC48ND3u4u_ULaQNnR4fxCRur392hErem3qi3fjRQ05wXg45D8NrhehK4Bp8h1f_uUGW0YbSQBmxDRWg3pi9PBkHvYnE8GHqjYs3JRTmCab08jUJQph9luxS');
            
            
                   $get_user_token =User::select('firebase_token')->where('id', '=',$id)->first();
                   
                      // return $get_user_token[0]->firebase_token;
            
            
                                 ////////////////////////////////////////////////////////
                                 
                                   $msg = array
                                          (
                                    'body'  => $body,
                                    'title' => $title,
                                              
                                          );
                                  $fields = array
                                      (
                                        'to'    => $get_user_token['firebase_token'],
                                        'notification'  => $msg
                                      );
                                  
                                                   
            
                                  $headers = array
                                      (
                                        'Authorization: key=' . API_ACCESS_KEY12,
                                        'Content-Type: application/json'
                                      );
                                #Send Reponse To FireBase Server  
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    $result = curl_exec($ch );
                                    //echo $result;
                                    curl_close( $ch );
                             
                    
                    
                        $save=new Notification;
                        $save->title=$title;
                        $save->body=$body;
                        $save->title_ar = $title_ar;
                        $save->body_ar = $body_ar;
                        $save->sender=$check_token['id'];
                        $save->is_read=0;
                         $save->request_id=$order;
                        $save->user_id=$id;
                        $save->created_at=$dateTime;
                        $save->save();
                        
                        
                     }catch(Exception $ex){
                          $message['error']=4;
                           $message['message']='error in send notification';
                          
                     }

          }else{
                   $message['data']=$insert;
                   $message['error']=1;
                   $message['message']='can not make rate';
          }
               
     
      
        }
      }
      }else{
        $message['error']=3;
        $message['message']='this token is not exist';
      }
      }catch(Exception  $ex){
         $message['error']=2;
          $message['message']='error'.$ex->getMessage();

      }
      return response()->json($message);
    } 

 

       
    public function show_rating(Request $request)
   {
     try{
       $token=$request->input('user_token');
    
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
        

          if($check_token['state'] == 2){

          $show=Rating::select('users.first_name','users.image','rating.rate','rating.comment','rating.created_at')
            ->join('users','rating.user_id','=','users.id')
            ->where([['rating.driver_id',$check_token['id']],['who_rate','user']])->get();

           }else{

            $show=Rating::select('users.first_name','users.image','rating.rate','rating.comment','rating.created_at')
              ->join('users','rating.driver_id','=','users.id')
              ->where([['rating.user_id',$check_token['id']],['who_rate','driver']])->get();
           }
                      
     
          
    

       if(count($show) > 0){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }
     
     public function show_ratingby_userid(Request $request)
   {
     try{
       $token=$request->input('user_token');
        $id=$request->input('user_id');
          
          
          $check_token=User::select('id')->where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

                       
                $user_state=User::where('id',$id)->value('state');
                
                if($user_state ==2){

                   $show=Rating::select('users.first_name','users.image','rating.rate','rating.comment','rating.created_at')
                   ->join('users','rating.user_id','=','users.id')
                   ->where([['rating.driver_id',$id],['who_rate','user']])->get();

                } else{
                  $show=Rating::select('users.first_name','users.image','rating.rate','rating.comment','rating.created_at')
                ->join('users','rating.driver_id','=','users.id')
                ->where([['rating.user_id',$id],['who_rate','driver']])->get();

                }      
     
          
    

       if(count($show) > 0){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
         $message['error']=3;
         $message['message']='this token is not exit';
     }

     }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();
     }

    return response()->json($message);
   }

 

}
