<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\User;

use carbon\carbon;

class CountryController extends Controller
{
    

     public function show_allcountry(Request $request)
  {
      try{
              $lang=$request->input('lang');

              if($lang=='en' || $lang=='EN'){
                $show=Country::select('id','country_key','E_name as name')->get();
              }else{
               $show=Country::select('id','country_key','name')->get();
              }


               if(count($show) > 0){
                    $message['data']=$show;
                    $message['error']=0;
                    $message['message']='show data';
               }else{

                    $message['data']=$show;
                    $message['error']=1;
                    $message['message']='no data';
               }
           

          }catch(Exception $ex){
              $message['error']=2;
              $message['message']='error'.$ex->getMessage();
          }  
         

       
         return response()->json($message);
   } 
        //country
  public function show_country(Request $request)
    {
      try{
        
             $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

       $show=Country::all();

       if(count($show) > 0){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
      }else{
         $message['error']=3;
         $message['message']='this token is not exit';
      }

            }catch(Exception $ex){
                 $message['error']=2;
                 $message['message']='error'.$ex->getMessage();
            }  
         

       
         return response()->json($message);
    }





    public function show_countryByid(Request $request)
    {
       try{
       
             $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
            $id=$request->input('country_id');
       
        $show=Country::where('id',$id)->first();

       if($show !=null){
            $message['data']=$show;
            $message['error']=0;
            $message['message']='show data';
       }else{

            $message['data']=$show;
            $message['error']=1;
            $message['message']='no data';
       }
     }else{
       $message['error']=3;
       $message['message']='this token is not exist';
     }

        }catch(Exception $ex){
             
              $message['error']=2;
              $message['message']='error'.$ex->getMessage();

        }
       return response()->json($message);
    }
    
     public function delete_country(Request $request)
    {
       try{
       
         $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
            $id=$request->input('country_id');
    
        $delete=Country::where('id',$id)->delete();

       if($delete ==true){
        
            $message['error']=0;
            $message['message']='delete success';
       }else{

           
            $message['error']=1;
            $message['message']='error in delete';
       }
          }else{
       $message['error']=3;
       $message['message']='this token is not exist';
     }
        }catch(Exception $ex){
             
              $message['error']=2;
              $message['message']='error'.$ex->getMessage();

        }
       return response()->json($message);
    }
    
     public function insert_country(Request $request)
    {
       try{
       
           $token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $name=$request->input('name');
           $e_name=$request->input('E_name');
                $created_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

           
            $select=new Country;
            $select->name=$name;
            $select->E_name=$e_name;
            $select->created_at=$dateTime;
            $select->save();

	          if( $select ==true){
	              $message['data']=$select;
	              $message['error']=0;
	              $message['message']='insert success';
	          }else{
	              $message['data']=$select;
	              $message['error']=1;
	              $message['message']='error in insert data';
	          }
         }else{
       $message['error']=3;
       $message['message']='this token is not exist';
     }
        }catch(Exception $ex){
             
              $message['error']=2;
              $message['message']='error'.$ex->getMessage();

        }
       return response()->json($message);
    }
    
    
     public function update_country(Request $request)
    {
       try{
       	$token=$request->input('user_token');
          
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
           $id=$request->input('id');
           $name=$request->input('name');
           $e_name=$request->input('E_name');

                $created_at = carbon::now()->toDateTimeString();
          $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));

           
            $update=Country::where('id',$id)->update([
                'name'=>$name,
                'E_name'=>$e_name,
                'updated_at'=>$dateTime
                
                ]);
            
             $select=Country::where('id',$id)->first();
         
          if( $update ==true){
              $message['data']=$select;
              $message['error']=0;
              $message['message']='update success';
          }else{
              $message['data']=$select;
              $message['error']=1;
              $message['message']='error in update data';
          }
     }else{
       $message['error']=3;
       $message['message']='this token is not exist';
     }
        }catch(Exception $ex){
             
              $message['error']=2;
              $message['message']='error'.$ex->getMessage();

        }
       return response()->json($message);
    }



}
