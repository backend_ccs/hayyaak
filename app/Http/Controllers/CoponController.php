<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use carbon\carbon;
use App\Copon;

use App\Invitations;

class CoponController extends Controller
{
     public $message = array();
    
    public function add_copon(Request $request){
        try{

            $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){


            $code=$request->input('code');
            $value=$request->input('value');
            $percentage=$request->input('percentage');
            $start_date=$request->input('start_date');
            $end_date= $request->input('end_date');
            
            $created_at = carbon::now()->toDateTimeString();
           $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
           
            
            $insertCopon = new Copon;
            
            $insertCopon->code = $code;
            $insertCopon->value = $value;
            $insertCopon->percentage = $percentage;
            $insertCopon->start_date = $start_date;
            $insertCopon->end_date = $end_date;
            $insertCopon->created_at = $dateTime;
            
            $insertCopon->save();
            
            if($insertCopon == true){
                $message['error'] = 0;
                $message['message'] = "a new copon is inserted successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
              }else{
	               $message['error']=3;
			       $message['message']='this token is not exist';
	          }

            
        }catch(Exception $ex){
    	    $message['error'] = 2;
            $message['message'] = "error('DataBase Error :{$ex->getMessage()}')";
         }
    
          return response()->json($message);
    }
    
     public function show_notactiveCopons(Request $request){
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
             
           $created_at = carbon::now()->toDateTimeString();
           $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
          
          
             if($request->has('user_token') && $check_token !=NULL){
                 
            $get_allCopons = Copon::select('id','code','value','percentage','start_date','end_date','use_count','created_at')
            ->where('end_date','<',$dateTime)
            ->orderBy('id','ASC')->get();
            
            if( count($get_allCopons)>0 ){
                $message['data'] = $get_allCopons;
                $message['error'] = 0;
                $message['message'] = "there is all the copon data";
            }else{
                $message['data'] = $get_allCopons;
                $message['error'] = 1;
                $message['message'] = "there is an errorn please try again";
            }
          }else{
	        $message['error']=3;
		    $message['message']='this token is not exist';
	      }

            
        }catch(Exception $ex){
    	    $message['error'] = 2;
            $message['message'] = "error('DataBase Error :{$ex->getMessage()}')";
         }
    
          return response()->json($message);
    }
    
    public function show_allCopons(Request $request){
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::where('user_token',$token)->first();
           $created_at = carbon::now()->toDateTimeString();
           $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($created_at)));
          
             if($request->has('user_token') && $check_token !=NULL){
                 
            $get_allCopons = Copon::select('id','code','value','percentage','start_date','end_date','use_count','created_at')
              ->where('end_date','>',$dateTime)
            
            ->orderBy('id','ASC')->get();
            
            if( count($get_allCopons)>0 ){
                $message['data'] = $get_allCopons;
                $message['error'] = 0;
                $message['message'] = "there is all the copon data";
            }else{
                $message['data'] = $get_allCopons;
                $message['error'] = 1;
                $message['message'] = "there is an errorn please try again";
            }
          }else{
	        $message['error']=3;
		    $message['message']='this token is not exist';
	      }

            
        }catch(Exception $ex){
    	    $message['error'] = 2;
            $message['message'] = "error('DataBase Error :{$ex->getMessage()}')";
         }
    
          return response()->json($message);
    }
    
    
    public function delete_copon(Request $request){
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
            $copon_id = $request->input('copon_id');
            
            $deleteCopon =Copon::where('id',$copon_id)->delete();
            
            if($deleteCopon == true){
                $message['error'] = 0;
                $message['message'] = "this copon is deleted succesfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
          }else{
	        $message['error']=3;
		    $message['message']='this token is not exist';
	      }
            
        }catch(Exception $ex){
    	    $message['error'] = 2;
            $message['message'] = "error('DataBase Error :{$ex->getMessage()}')";
         }
    
          return response()->json($message);
    }
    
    public function show_copon_ByID(Request $request){
        try{

        	 $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
            
            $copon_id = $request->input('copon_id');
            
            $show_copon = Copon::select('id','code','value','percentage','start_date','end_date','created_at')
                                    ->where('id',$copon_id)->first();
                                    
            if($show_copon != NULL){
                $message['data'] = $show_copon;
                $message['error'] = 0;
                $message['message'] = "this is the data of that copon";
            }else{
                $message['data'] = $show_copon;
                $message['error'] = 1;
                $message['message'] = "there is no copon, please try again";
            }
          }else{
	        $message['error']=3;
		    $message['message']='this token is not exist';
	      }
        }catch(Exception $ex){
    	    $message['error'] = 2;
            $message['message'] = "error('DataBase Error :{$ex->getMessage()}')";
         }
    
          return response()->json($message);
    }
    
    public function updated_copon(Request $request){
        try{
            
             $token=$request->input('user_token');
          
             $check_token=User::select('id')->where('user_token',$token)->first();
          
          
             if($request->has('user_token') && $check_token !=NULL){
            $copon_id = $request->input('copon_id');
            $code=$request->input('code');
            $value=$request->input('value');
            $percentage=$request->input('percentage');
            $start_date=$request->input('start_date');
            $end_date= $request->input('end_date');
            
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));
           
            
            $update_copon = Copon::where('id',$copon_id)
                                        ->update([
                 
                                        'code' => $code,
                                        'value' => $value,
                                        'percentage'=>$percentage,
                                        'start_date' => $start_date,
                                        'end_date' =>$end_date,
                                        'updated_at' => $dateTime,
                                          ]);
                                          
                                          
           if($update_copon == true){
               $message['error'] = 0;
               $message['message'] = "this copon data is updated successfully";
           }else{
               $message['error'] = 1;
               $message['message'] = "there is an error, please try again";
           }
         }else{
	        $message['error']=3;
		    $message['message']='this token is not exist';
	      }  
           

        }catch(Exception $ex){
    	    $message['error'] = 2;
            $message['message'] = "error('DataBase Error :{$ex->getMessage()}')";
         }
    
          return response()->json($message);
    }


 
}
