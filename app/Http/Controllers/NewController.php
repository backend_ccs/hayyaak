<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Menu;
use App\Product_type;
use App\cancel_reason;
use App\Offers;
use carbon\carbon;
use App\setting;
use App\About;

define( 'API_ACCESS_KEY12','AAAAnSDEgLc:APA91bFisJ6mf6QmnpUvaC48ND3u4u_ULaQNnR4fxCRur392hErem3qi3fjRQ05wXg45D8NrhehK4Bp8h1f_uUGW0YbSQBmxDRWg3pi9PBkHvYnE8GHqjYs3JRTmCab08jUJQph9luxS');

class NewController extends Controller
{
     public $message=array();

 public function cancel_reasons(Request $request){
       try{

          $token=$request->input('user_token');


          $check_token=User::where('user_token',$token)->first();

          if($request->has('user_token') && $check_token !=null){
              
             $set=setting::where('user_id',$check_token)->value('language');
              
              if($check_token['state']==2){//driver
                
                if($set==1){
                    $select=cancel_reason::select('id','text')->where('type','for_agent')->orWhere('type','all')->orderBy('id','DESC')->get();
                }else{
                      $select=cancel_reason::select('id','E_text as text')->where('type','for_agent')->orWhere('type','all')->orderBy('id','DESC')->get();
  
                }
                  
                  

                  
              }else{
  

                   if($set==1){
                       
                    $select=cancel_reason::select('id','text')->where('type','for_user')->orwhere('type','all')->orderBy('id','DESC')->get();
                }else{
                      $select=cancel_reason::select('id','E_text as text')->where('type','for_user')->orWhere('type','all')->orderBy('id','DESC')->get();
  
                }
                
              }

                 if(count($select )>0){
                 	$message['data']=$select;
                    $message['error']=0;
                    $message['message']='show data success';

                 }else{
                    $message['data']=$select;
                    $message['error']=1;
                    $message['message']='no data';

                 }


          }else{
             $message['error']=3;
             $message['message']='this token is not exist';
          }


       }catch(Exception $ex){

       	$message['error']=2;
       	$message['message']='error '.$getMessage();
       }
     	return response()->json($message);
     }
     
     
     
      public function show_tax(Request $request){
       try{

          $token=$request->input('user_token');


          $check_token=User::where('user_token',$token)->first();

          if($request->has('user_token') && $check_token !=null){
              




         $select=About::select('id','user_tax','driver_tax')->where('id',1)->first();
  
               
                  
                  

                  
             
                
              

                 if($select !=null){
                 	$message['data']=$select;
                    $message['error']=0;
                    $message['message']='show data success';

                 }else{
                    $message['data']=$select;
                    $message['error']=1;
                    $message['message']='no data';

                 }


          }else{
             $message['error']=3;
             $message['message']='this token is not exist';
          }


       }catch(Exception $ex){

       	$message['error']=2;
       	$message['message']='error '.$getMessage();
       }
     	return response()->json($message);
     }
     
     
      public function update_tax(Request $request){
       try{

          $token=$request->input('user_token');
           $user_tax=$request->input('user_tax');
            $driver_tax=$request->input('driver_tax');

          $check_token=User::where('user_token',$token)->first();

          if($request->has('user_token') && $check_token !=null){
              
          $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at))); 



         $update=About::where('id',1)->update([
             
              'user_tax'=>$user_tax,
              'driver_tax'=>$driver_tax,
              'updated_at'=>$dateTime
             
             ]);
  
               
                  
                  

                  
             
                
              

                 if($update ==true){
                // 	$message['data']=$select;
                    $message['error']=0;
                    $message['message']='update data success';

                 }else{
                  //  $message['data']=$select;
                    $message['error']=1;
                    $message['message']='error in update data';

                 }


          }else{
             $message['error']=3;
             $message['message']='this token is not exist';
          }


       }catch(Exception $ex){

       	$message['error']=2;
       	$message['message']='error '.$getMessage();
       }
     	return response()->json($message);
     }
     
     
      public function show_usertax(Request $request){
       try{

          $token=$request->input('user_token');


          $check_token=User::where('user_token',$token)->first();

          if($request->has('user_token') && $check_token !=null){
              




         $select=About::select('id')->where('id',1)->first();
         
          $user_tax=About::where('id',1)->value('user_tax');
           $tax=str_replace('%', '', $user_tax);
                 
            $tax=round(($tax/100)*100,2);
  
 
              

                 if($user_tax !=null){
                 	$message['data']=$tax;
                    $message['error']=0;
                    $message['message']='show data success';

                 }else{
                    $message['data']=$tax;
                    $message['error']=1;
                    $message['message']='no data';

                 }


          }else{
             $message['error']=3;
             $message['message']='this token is not exist';
          }


       }catch(Exception $ex){

       	$message['error']=2;
       	$message['message']='error '.$getMessage();
       }
     	return response()->json($message);
     }


     
     
     public function show_offers(Request $request){
       try{

           $token = $request->input('user_token');
           $type  = $request->input('type');
           

           $check_token=User::where('user_token',$token)->first();

           $updated_at = carbon::now()->toDateTimeString();
           $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at))); 	 

          if($request->has('user_token') && $check_token !=null){
              
              $long=$check_token['longitude'];
              $lat=$check_token['latitude'];
              $offers_count = 0;
       //  (select IF((select offers.id from offers where driver_id='.$check_token['id'].' AND offers.request_id=requestes.id)>0,"1","0"))as offered
              
              if($check_token['state']==2 && $type == "driver"){//driver
                  
                $offer_request = \App\Offers::where('driver_id' , $check_token->id)->orderBy('id' , 'DESC')->first();


                if($offer_request != NULL){

                    $allrequests = \App\Requests::where('id' , $offer_request->request_id)->value('shop_id');
    
                    $offers_count =  \App\Offers::join('requestes' , 'offers.request_id', '=' ,'requestes.id')
                                        ->WHERE([ ['offers.driver_id' , $check_token->id ],['requestes.shop_id' , $allrequests], ['requestes.state' , 2] ] )->count();

                }
                
                
                
                if( $offer_request != NULL && $offers_count < 3){
                    
                     $select=DB::select('select requestes.id,requestes.user_id as delivery_id,requestes.receiving_longitude,shopes.name,shopes.image,requestes.receiving_latitude,
                           (select IF((select offers.id from offers where driver_id='.$check_token['id'].' AND offers.request_id=requestes.id)>0,1,0))as offered
                          ,requestes.delivery_longitude as user_longitude,requestes.delivery_latitude as user_latitude,requestes.description,
                          
                         requestes.delivery_time,state.name as state,requestes.created_at,requestes.payment_id as payment,
                              ( 6367 * acos( cos( radians(round('.$lat.',2) ) ) * cos( radians(round( requestes.receiving_latitude,2) ) ) * cos( radians( round( requestes.receiving_longitude,2) )
                              - radians(round('.$long.',2) ) ) + sin( radians( round('.$lat.',2) ) ) * sin( radians(round(requestes.receiving_latitude,2) ) ) ) )AS agent_Recivingdistance,
             
                                 ( 6371 * acos( cos( radians(requestes.receiving_latitude) ) * cos( radians(requestes.delivery_latitude) ) * cos( radians(requestes.delivery_longitude) - radians(requestes.receiving_longitude) ) 
                                 + sin( radians(requestes.receiving_latitude) ) * sin( radians(requestes.delivery_latitude)))) AS client_Recivingdistance , requestes.receiving_address , requestes.delivery_address
                                    FROM requestes
                                      left join shopes ON requestes.shop_id=shopes.id
                                       left join state ON requestes.state=state.id
                                      
                                    where requestes.state=1 
                                    
                                 Having agent_Recivingdistance <=100  order by requestes.created_at DESC');
                                 
                                 
                      foreach($select as $data){
                        $request_order = \app\Offers::where('offers.request_id' , $data->id)->first();
                        
                        if($request_order == NULL){
                        
                            $newtime = strtotime($data->created_at);
                            $time = date('H:i',$newtime);
                            
                            $newtime2 = strtotime($dateTime);
                            $time2 = date('H:i',$newtime2);
                            
                            
                            // $interval = date_diff($data->created_at, $dateTime);
       
                            $mints15 = ( strtotime($time2) - strtotime($time) ) / 60;
                            
                            if( $mints15 >= 30){
                                $update_request = \App\Requests::where('id' ,  $data->id)->update(['state' => 6]);
                                
        
        
                                 $get_user_token =User::where('id', $data->delivery_id)->value('firebase_token');
                           
                              // return $get_user_token[0]->firebase_token;
                    
                    
                                         ////////////////////////////////////////////////////////
                                         
                                           $msg = array
                                                  (
                                            'body'  => "Hayyaak requests",
                                            'title' => "No one make offer in your order, so it canceled",
                                            'state' => $data->state,
                                                      
                                                  );
                                          $fields = array
                                              (
                                                'to'    => $get_user_token,
                                                'notification'  => $msg
                                              );
                                          
                                                           
                    
                                          $headers = array
                                              (
                                                'Authorization: key=' . API_ACCESS_KEY12,
                                                'Content-Type: application/json'
                                              );
                                        #Send Reponse To FireBase Server  
                                            $ch = curl_init();
                                            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                            curl_setopt( $ch,CURLOPT_POST, true );
                                            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                            $result = curl_exec($ch );
                                            //echo $result;
                                            curl_close( $ch );
                                     
                            
                            
                                $save=new \App\Notification;
                                $save->title="Hayyaak requests";
                                $save->body="No one make offer in your order, so it canceled";
                                $save->title_ar = "طلبات حياك";
                                $save->body_ar = "لا أحد قدم عرضًا على طلبك ، لذلك تم إلغاؤه";
                                $save->sender= 1;
                                $save->is_read=0;
                                $save->request_id=$data->id;
                                $save->user_id=$data->delivery_id;
                                $save->created_at=$dateTime;
                                $save->save();
                                
                            }
                        }
                        
                        
                    }
                             
                }elseif( $offers_count >= 3){
                    $select = [];
                }else{
                
                     
                      $select=DB::select('select requestes.id,requestes.user_id as delivery_id,requestes.receiving_longitude,shopes.name,shopes.image,requestes.receiving_latitude,
                                   (select IF((select offers.id from offers where driver_id='.$check_token['id'].' AND offers.request_id=requestes.id)>0,1,0))as offered
                                  ,requestes.delivery_longitude as user_longitude,requestes.delivery_latitude as user_latitude,requestes.description,
                                  
                                 requestes.delivery_time,state.name as state,requestes.created_at,requestes.payment_id as payment,
                                      ( 6367 * acos( cos( radians(round('.$lat.',2) ) ) * cos( radians(round( requestes.receiving_latitude,2) ) ) * cos( radians( round( requestes.receiving_longitude,2) )
                                      - radians(round('.$long.',2) ) ) + sin( radians( round('.$lat.',2) ) ) * sin( radians(round(requestes.receiving_latitude,2) ) ) ) )AS agent_Recivingdistance,
                     
                                         ( 6371 * acos( cos( radians(requestes.receiving_latitude) ) * cos( radians(requestes.delivery_latitude) ) * cos( radians(requestes.delivery_longitude) - radians(requestes.receiving_longitude) ) 
                                         + sin( radians(requestes.receiving_latitude) ) * sin( radians(requestes.delivery_latitude)))) AS client_Recivingdistance , requestes.receiving_address , requestes.delivery_address
                                            FROM requestes
                                              left join shopes ON requestes.shop_id=shopes.id
                                               left join state ON requestes.state=state.id
                                              
                                            where requestes.state=1 
                                            
                                         Having agent_Recivingdistance <=100  order by requestes.created_at DESC');
                                         
                                         
                         
    
                              foreach($select as $data){
                        $request_order = \app\Offers::where('offers.request_id' , $data->id)->first();
                        
                        if($request_order == NULL){
                        
                            $newtime = strtotime($data->created_at);
                            $time = date('H:i',$newtime);
                            
                            $newtime2 = strtotime($dateTime);
                            $time2 = date('H:i',$newtime2);
                            
                            
                            // $interval = date_diff($data->created_at, $dateTime);
       
                            $mints15 = ( strtotime($time2) - strtotime($time) ) / 60;
                            
                            if( $mints15 >= 30){
                                $update_request = \App\Requests::where('id' ,  $data->id)->update(['state' => 6]);
                                
        
        
                                 $get_user_token =User::where('id', $data->delivery_id)->value('firebase_token');
                           
                              // return $get_user_token[0]->firebase_token;
                    
                    
                                         ////////////////////////////////////////////////////////
                                         
                                           $msg = array
                                                  (
                                            'body'  => "Hayyaak requests",
                                            'title' => "No one make offer in your order, so it canceled",
                                            'state' => $data->state,
                                                      
                                                  );
                                          $fields = array
                                              (
                                                'to'    => $get_user_token,
                                                'notification'  => $msg
                                              );
                                          
                                                           
                    
                                          $headers = array
                                              (
                                                'Authorization: key=' . API_ACCESS_KEY12,
                                                'Content-Type: application/json'
                                              );
                                        #Send Reponse To FireBase Server  
                                            $ch = curl_init();
                                            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                            curl_setopt( $ch,CURLOPT_POST, true );
                                            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                            $result = curl_exec($ch );
                                            //echo $result;
                                            curl_close( $ch );
                                     
                            
                            
                                $save=new Notification;
                                $save->title="Hayyaak requests";
                                $save->body="No one make offer in your order, so it canceled";
                                $save->title_ar = "طلبات حياك";
                                $save->body_ar = "لا أحد قدم عرضًا على طلبك ، لذلك تم إلغاؤه";
                                $save->sender= 1;
                                $save->is_read=0;
                                $save->request_id=$data->id;
                                $save->user_id=$data->delivery_id;
                                $save->created_at=$dateTime;
                                $save->save();
                                
                            }
                        }
                        
                        
                    } 
                    
                }
                  
              }elseif($check_token['state']==2 && $type == "user"){
                  
                  
                    $select=DB::select('select offers.id as offer_id,offers.request_id,users.id as delivery_id,users.first_name,users.last_name,users.image,users.rate,requestes.delivery_time,offers.price,(select user_accounts.payment from user_accounts   where user_accounts.user_id=users.id and main=1 limit 1 )as payment,
                                 (select requestes.receiving_address  from requestes where requestes.id = offers.request_id) as receiving_address ,(select requestes.delivery_address  from requestes where requestes.id = offers.request_id) as delivery_address ,
                                 ( 6371 * acos( cos( radians(requestes.receiving_latitude) ) * cos( radians(users.latitude) ) * cos( radians(users.longitude) - radians(requestes.receiving_longitude) ) 
                                 + sin( radians(requestes.receiving_latitude) ) * sin( radians(users.latitude)))) AS agent_Recivingdistance,
                                 ( 6371 * acos( cos( radians(requestes.receiving_latitude) ) * cos( radians(requestes.delivery_latitude) ) * cos( radians(requestes.delivery_longitude) - radians(requestes.receiving_longitude) ) 
                                 + sin( radians(requestes.receiving_latitude) ) * sin( radians(requestes.delivery_latitude)))) AS client_Recivingdistance,
                                  ( 6371 * acos( cos( radians('.$check_token['latitude'].') ) * cos( radians(users.latitude) ) * cos( radians(users.longitude) - radians('.$check_token['longitude'].') ) 
                                 + sin( radians('.$check_token['latitude'].') ) * sin( radians(users.latitude)))) AS away_distance 
                    
                    		                from offers 
                    		            	join users ON offers.driver_id=users.id
                    		             join (
                                                select requestes.id,requestes.delivery_time,requestes.delivery_longitude,requestes.delivery_latitude,requestes.receiving_longitude,requestes.receiving_latitude  
                                                from requestes 
                                                 join offers ON requestes.id=offers.request_id
                                                where requestes.user_id='.$check_token['id'].' AND requestes.state=1 limit 1)
                                                as requestes ON offers.request_id=requestes.id
                                         
                                    where  offers.state="wait" order by offers.price');
              }else{



            $select=DB::select('select offers.id as offer_id,offers.request_id,users.id as delivery_id,users.first_name,users.last_name,users.image,users.rate,requestes.delivery_time,offers.price,(select user_accounts.payment from user_accounts   where user_accounts.user_id=users.id and main=1 limit 1 )as payment,
             (select requestes.receiving_address  from requestes where requestes.id = offers.request_id) as receiving_address ,(select requestes.delivery_address  from requestes where requestes.id = offers.request_id) as delivery_address ,
              ( 6371 * acos( cos( radians(requestes.receiving_latitude) ) * cos( radians(users.latitude) ) * cos( radians(users.longitude) - radians(requestes.receiving_longitude) ) 
                     + sin( radians(requestes.receiving_latitude) ) * sin( radians(users.latitude)))) AS agent_Recivingdistance,
                     ( 6371 * acos( cos( radians(requestes.receiving_latitude) ) * cos( radians(requestes.delivery_latitude) ) * cos( radians(requestes.delivery_longitude) - radians(requestes.receiving_longitude) ) 
                     + sin( radians(requestes.receiving_latitude) ) * sin( radians(requestes.delivery_latitude)))) AS client_Recivingdistance,
                      ( 6371 * acos( cos( radians('.$check_token['latitude'].') ) * cos( radians(users.latitude) ) * cos( radians(users.longitude) - radians('.$check_token['longitude'].') ) 
                     + sin( radians('.$check_token['latitude'].') ) * sin( radians(users.latitude)))) AS away_distance 

		                from offers 
		            	join users ON offers.driver_id=users.id
		             join (
                            select requestes.id,requestes.delivery_time,requestes.delivery_longitude,requestes.delivery_latitude,requestes.receiving_longitude,requestes.receiving_latitude  
                            from requestes 
                             join offers ON requestes.id=offers.request_id
                            where requestes.user_id='.$check_token['id'].' AND requestes.state=1 limit 1)
                            as requestes ON offers.request_id=requestes.id
                     
                where  offers.state="wait" order by offers.price');
                
                
              }
              

                 if(count($select )>0){
                 	$message['data']=$select;
                    $message['error']=0;
                    $message['message']='show data success';

                 }else{
                    $message['data']=$select;
                    $message['error']=1;
                    $message['message']='no data';


                 }


          }else{
             $message['error']=3;
             $message['message']='this token is not exist';
          }


       }catch(Exception $ex){

       	$message['error']=2;
       	$message['message']='error '.$getMessage();
       }
     	return response()->json($message);
     }
     
     
     
     
     
     
     
     
       public function delivery_total(Request $request){
       try{

          $token=$request->input('user_token');
          

          $check_token=User::where('user_token',$token)->first();

          if($request->has('user_token') && $check_token !=null){
              
            
              
        
 //	join users ON offers.driver_id=users.id
		            
            $select=DB::select('select requestes.id,shopes.name,shopes.image,requestes.description,offers.price,requestes.created_at,
              ( 6371 * acos( cos( radians(requestes.receiving_latitude) ) * cos( radians('.$check_token['latitude'].') ) * cos( radians('.$check_token['longitude'].') - radians(requestes.receiving_longitude) ) 
                     + sin( radians(requestes.receiving_latitude) ) * sin( radians('.$check_token['latitude'].')))) AS agent_Recivingdistance,
                     ( 6371 * acos( cos( radians(requestes.receiving_latitude) ) * cos( radians(requestes.delivery_latitude) ) * cos( radians(requestes.delivery_longitude) - radians(requestes.receiving_longitude) ) 
                     + sin( radians(requestes.receiving_latitude) ) * sin( radians(requestes.delivery_latitude)))) AS client_Recivingdistance

		                from requestes 
		           
		            	join offers ON requestes.id=offers.request_id
                        join shopes ON requestes.shop_id=shopes.id
                        where requestes.state="received" AND offers.state="accepted" AND offers.driver_id='.$check_token['id']);
                
                
              
                $total = \App\Wallet::where('user_id' , $check_token->id)->value('credit');
              

                 if(count($select )>0){
                    $message['total'] = $total;
                 	$message['data'] = $select;
                    $message['error']=0;
                    $message['message']='show data success';

                 }else{
                    $message['total'] = $total;
                    $message['data']=$select;
                    $message['error']=1;
                    $message['message']='no data';

                 }


          }else{
             $message['error']=3;
             $message['message']='this token is not exist';
          }


       }catch(Exception $ex){

       	$message['error']=2;
       	$message['message']='error '.$getMessage();
       }
     	return response()->json($message);
     }
     
     

//add ptoduct to menu
     public function add_product(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $name=$request->input('name');
              $image=$request->file('image'); 
              $price=$request->input('price');
              $id=$request->input('shope_id');


             if(isset($image)) {
                  $new_name = $image->getClientOriginalName();
                  $savedFileName = rand(100000,999999).time()."_".$new_name; 
                    // give a unique name to file to    be saved
                        $destinationPath_id = 'uploads/products';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
                 }else{
                    $images =NULL;     
                 }



                  $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $insert=new Menu;
             $insert->product=$name;
             $insert->image=$images;
             $insert->shop_id=$id;
             $insert->price=$price;
             $insert->created_at=$dateTime;
             $insert->save();
    
        
          if($insert ==true){
        
         
            $message['error']=0;
            $message['message']='add product to menu';

          }else{
           
            $message['error']=1;
            $message['message']='error in insert product data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

   public function show_types(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
          $id=$request->input('product_id');
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             

            $type=Product_type::select('id','name','price','created_at','updated_at')->where('product_id',$id)->get();
          
  
          if(count($type ) > 0){
              
            $message['data']=$type;
            $message['error']=0;
            $message['message']='show type data';

          }else{
             $message['data']=$type;
            $message['error']=1;
            $message['message']='no type data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }

    public function show_typebyid(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
          $id=$request->input('id');
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             

            $type=Product_type::select('id','name','price','created_at','updated_at')->where('id',$id)->first();
          
  
          if($type !=null){
              
            $message['data']=$type;
            $message['error']=0;
            $message['message']='show type data';

          }else{
             $message['data']=$type;
            $message['error']=1;
            $message['message']='no type data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
    
    public function delete_type(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
          $id=$request->input('id');
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             

            $type=Product_type::where('id',$id)->delete();
          
  
          if($type ==true){
              
     
            $message['error']=0;
            $message['message']='delete type data';

          }else{
           
            $message['error']=1;
            $message['message']='error in delete type data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
     public function add_product_type(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              $id=$request->input('product_id');
              $name=$request->input('name');
             
              $price=$request->input('price');
             

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $insert=new Product_type;
             $insert->product_id=$id;
             $insert->name=$name;
             $insert->price=$price;
             $insert->created_at=$dateTime;
             $insert->save();
    
        
          if($insert ==true){
      
            $message['error']=0;
            $message['message']='add product type to menu';

          }else{
           
            $message['error']=1;
            $message['message']='error in insert product type ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
       public function update_product_type(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
              $id=$request->input('id');
              $name=$request->input('name');
             
              $price=$request->input('price');
             

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $updat=Product_type::where('id',$id)->update([
                                                            'name'=>$name,
                                                            'price'=>$price,
                                                            'updated_at'=>$dateTime  
                 ]);
            
    
        
          if($updat ==true){
      
            $message['error']=0;
            $message['message']='update product type to menu';

          }else{
           
            $message['error']=1;
            $message['message']='error in update product type ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
    
    

  public function update_product(Request $request)
  {
       try{
        
          $token=$request->input('user_token');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
             
              $id=$request->input('product_id');
              $name=$request->input('name');
              $image=$request->file('image'); 
              $price=$request->input('price');


             if(isset($image)) {
                  $new_name = $image->getClientOriginalName();
                  $savedFileName = rand(100000,999999).time()."_".$new_name; 
                    // give a unique name to file to    be saved
                        $destinationPath_id = 'uploads/products';
                        $image->move($destinationPath_id, $savedFileName);
            
                        $images = $savedFileName;
                      
                 }else{
                    $images =Menu::where('id',$id)->value('image');     
                 }
                  $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $update=Menu::where('id',$id)->update([

              'product'=>$name,
              'image'=>$images,
              'price'=>$price,
              'updated_at'=>$dateTime


              ]);
               
          if($update ==true){
         
            $message['error']=0;
            $message['message']='add product to menu';

          }else{
           
            $message['error']=1;
            $message['message']='error in insert product data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
  }

  public function show_menu(Request $request)
  {
       try{
        
          $token=$request->input('user_token');
          $id=$request->input('shope_id');
                
           
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $select=Menu::where('shop_id',$id)->get();
          
  
          if(count($select) >0){
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show shope data';

          }else{
             $message['data']=$select;
            $message['error']=1;
            $message['message']='no shope data ';
          }
        
       }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
  }


    public function show_productbyid(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
          $id=$request->input('product_id');
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $select=Menu::where('id',$id)->first();
             
          //   $type=Product_type::select('id','name')->where('product_id',$id)->get();
          
  
          if($select !=null){
              
            $message['data']=$select;
            $message['error']=0;
            $message['message']='show shope data';

          }else{
             $message['data']=$select;
            $message['error']=1;
            $message['message']='no shope data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
  
    public function delete_product(Request $request)
    {
       try{
        
          $token=$request->input('user_token');
                
          $id=$request->input('product_id');
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){

              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+2 hours',strtotime($updated_at)));    

  
             
             $select=Menu::where('id',$id)->delete();
          
  
          if($select ==true){

          
            $message['error']=0;
            $message['message']='delete  data';

          }else{
           
            $message['error']=1;
            $message['message']='error in delete  data ';
          }
        
          }else{
              $message['error']=3;
            $message['message']='this token is not exist'; 
          }

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }





   public function near_orders(Request $request){
     try{


            
          $token=$request->input('user_token');
          $long=$request->input('longitude');
          $lat=$request->input('latitude');    
          
          $check_token=User::where('user_token',$token)->first();
          
          
          if($request->has('user_token') && $check_token !=NULL){
                    
          if(!$request->has('longitude') && !$request->has('latitude')){

            $long=$check_token['longitude'];
            $lat=$check_token['latitude'];
          }

              $select=DB::select('select requestes.id,requestes.receiving_longitude,requestes.receiving_latitude,requestes.delivery_longitude as user_longitude,requestes.delivery_latitude as user_latitude,requestes.description,requestes.delivery_time,requestes.state,requestes.created_at,
                  ( 6371 * acos( cos( radians(requestes.receiving_latitude) ) * cos( radians('.$lat.') ) * cos( radians('.$long.') - radians(requestes.receiving_longitude) ) 
                     + sin( radians(requestes.receiving_latitude) ) * sin( radians('.$lat.')))) AS agent_Recivingdistance,
                     ( 6371 * acos( cos( radians(requestes.receiving_latitude) ) * cos( radians(requestes.delivery_latitude) ) * cos( radians(requestes.delivery_longitude) - radians(requestes.receiving_longitude) ) 
                     + sin( radians(requestes.receiving_latitude) ) * sin( radians(requestes.delivery_latitude)))) AS client_Recivingdistance
                        FROM requestes
                        
                       Having agent_Recivingdistance <=20');



              if(count($select)>0){
                $message['data']=$select;
                $message['error']=0;
                $message['message']='show data success';
             
              }else{
                $message['data']=$select;
                $message['error']=1;
                $message['message']='no data';


              }
        }else{
          $message['error']=3;
          $message['message']='this token is not exist';
        }

     }catch(Exception $ex){
       $message['error']=2;
       $message['message']='error '.$ex->getMessage();
     }
 
     return response()->json($message);
   }




     
}
